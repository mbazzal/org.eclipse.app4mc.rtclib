/* CurveSubSegmentIteratorInv implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Red (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch)

Created: 02.11.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.Messages;

/**
 * <code>CurveSubSegmentIteratorInv</code> provides an iterator that iterates 
 * over the single vertical curve sub-segments of the of two curves.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: CurveSubSegmentIteratorInv.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Red (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class CurveSubSegmentIteratorInv {

    /**
     * Creates an CurveSubSegmentIteratorInv that iterates over all vertical 
     * Subsegments of the Curves a and b, from x=0 up to and including x=xMax.
     * <br><br>
     * Vertical Subsegments are defined as follows:<br> 
     * Y_A is the ordered list of the y-values of the start-points of all 
     * Segments of Curve a.<br>
     * Y_B is the ordered list of the y-values of the start-points of all 
     * Segments of Curve b.<br>
     * Y_AB is the ordered union of Y_A and Y_B without any duplicates.<br>
     * Y_AB contains the y-values of the start and end points of all Subsegments
     * of the Curve a and b. A Subsegment therefore starts at Y_AB[i] and ends 
     * at Y_AB[i+1].<br><br>
     * Vertical Subsegments are of interest, because the inverse of both Curves 
     * a and b are continuous and do not change their slope during a 
     * vertical subsegment.
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @param yMax the upper value up to which this CurveSubSegmentIterator 
     * should iterate.
     */
    public CurveSubSegmentIteratorInv(Curve a, Curve b, double yMax) {
        if (a != null && b != null) {
            this.a = a;
            this.b = b;
            this.yMax = yMax;
            this.iterA = a.segmentIterator();
            this.iterB = b.segmentIterator();
            this.init = false;
            this.pointer = -1;
        }
    }
    
    /**
     * Creates an CurveSubSegmentIteratorInv that iterates over all vertical 
     * Subsegments of the Curves a and b.<br><br>
     * Vertical Subsegments are defined as follows:<br> 
     * Y_A is the ordered list of the y-values of the start-points of all 
     * Segments of Curve a.<br>
     * Y_B is the ordered list of the y-values of the start-points of all 
     * Segments of Curve b.<br>
     * Y_AB is the ordered union of Y_A and Y_B without any duplicates.<br>
     * Y_AB contains the y-values of the start and end points of all Subsegments
     * of the Curve a and b. A Subsegment therefore starts at Y_AB[i] and ends 
     * at Y_AB[i+1].<br><br>
     * Vertical Subsegments are of interest, because the inverse of both Curves 
     * a and b are continuous and do not change their slope during a 
     * vertical subsegment.
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     */
    public CurveSubSegmentIteratorInv(Curve a, Curve b) {
        this(a, b, Double.POSITIVE_INFINITY);
    }
    
	/**
	 * Advances this CurveSubSegmentIteratorInv to the next vertical Subsegment 
     * and returns true, if this CurveSubSegmentIteratorInv did not reach its 
     * end yet. 
     * 
	 * @return true, if this CurveSubSegmentIteratorInv did not reach its end yet.
	 */
	public boolean next() {
        if (a == null || b == null) {
            return false;
        }
        if (!init) {
            iterA.next();
            if (DoubleMath.lt(iterA.s(), 0.0)) {
                throw new IllegalOperationException(Messages.SEGMENT_LIST_A_NOT_INVERTABLE);
            }
            iterB.next();
            if (DoubleMath.lt(iterB.s(), 0.0)) {
                throw new IllegalOperationException(Messages.SEGMENT_LIST_B_NOT_INVERTABLE);
            }
            init = true;
            aIsNew = true;
            bIsNew = true;
            aIsRenewed = false;
            bIsRenewed = false;
            yEnd = 0.0;
            yStart = -1.0;
        } else {
            if (DoubleMath.eq(yEnd, yMax) && 
                    DoubleMath.gt(segmentA.s(), 0.0) &&
                    DoubleMath.gt(segmentB.s(), 0.0)) {
                return false;
            }
            if (DoubleMath.eq(iterA.yEnd(), yEnd)) {
                aIsNew = iterA.next();
                if (DoubleMath.lt(iterA.s(), 0.0)) {
                    throw new IllegalOperationException(Messages.SEGMENT_LIST_A_NOT_INVERTABLE);
                }
                aIsRenewed = false;
            } else if (DoubleMath.gt(iterA.yEnd(), yEnd)) {
                aIsNew = false;
                aIsRenewed = true;
            } else {
                aIsNew = false;
                aIsRenewed = false;
            }
            if (DoubleMath.eq(iterB.yEnd(), yEnd)) {
                bIsNew = iterB.next();
                if (DoubleMath.lt(iterB.s(), 0.0)) {
                    throw new IllegalOperationException(Messages.SEGMENT_LIST_B_NOT_INVERTABLE);
                }
                bIsRenewed = false;
            } else if (DoubleMath.gt(iterB.yEnd(), yEnd)) {
                bIsNew = false;
                bIsRenewed = true;
            } else {
                bIsNew = false;
                bIsRenewed = false;
            }
            if (aIsNew || bIsNew || (aIsRenewed && bIsRenewed)) {
                // nop
            } else {
                //System.out.println("end2: " + aIsNew + bIsNew + aIsRenewed + bIsRenewed);
                return false;
            }
            
        }	
        yStart = yEnd;
        if (DoubleMath.gt(iterA.yStart(), yStart)) {
            segmentA = new Segment(iterA.xStart(), yStart, Double.POSITIVE_INFINITY);
            xStartA = segmentA.x();
            xEndA = segmentA.x();
            yEndA = iterA.yStart();
        } else {
            segmentA = iterA.segment();
            xStartA = segmentA.x();
            yEndA = iterA.yEnd();
        }
        if (DoubleMath.gt(iterB.yStart(), yStart)) {
            segmentB = new Segment(iterB.xStart(), yStart, Double.POSITIVE_INFINITY);
            xStartB = segmentB.x();
            xEndB = segmentB.x();
            yEndB = iterB.yStart();
        } else {
            segmentB = iterB.segment();
            xStartB = segmentB.x();
            yEndB = iterB.yEnd();
        }        
        yEnd = Math.min(yEndA, yEndB);
        yEnd = Math.min(yEnd, yMax);
        yEnd = Math.min(Math.min(yEndA, yEndB), yEnd);
        if (DoubleMath.leq(iterA.yStart(), yStart)) {
            xStartA = segmentA.xAt(yStart);
            xEndA = segmentA.xAt(yEnd);
        }
        if (DoubleMath.leq(iterB.yStart(), yStart)) {
            xStartB = segmentB.xAt(yStart);
            xEndB = segmentB.xAt(yEnd);
        }        
//        System.out.println(subSegmentA() + "  :  " + subSegmentB()
//                + "   yStart,yEnd,yMax=(" + yStart + "," + yEnd + "," + yMax + ")");       
        pointer = pointer + 1;
        return true;
	}
    
    /**
     * Resets this CurveSubSegmentIteratorInv.
     */
    public void reset() {
        if (a != null && b != null) {
            iterA.reset();
            iterB.reset();
            this.init = false;
            this.pointer = -1;
        }        
    }
	
	/**
	 * Returns the y-value of the start of the current vertical Subsegment.
     * 
	 * @return the y-value of the start of the current vertical Subsegment.
	 */
	public double yStart() {
        if (!init) throw new IllegalStateException();
	    return yStart;
	}
	
	/**
	 * Returns the x-value of the end of the current vertical Subsegment.
     * 
	 * @return the x-value of the end of the current vertical Subsegment.
	 */
	public double yEnd() {
        if (!init) throw new IllegalStateException();
	    return yEnd;
	}

	/**
	 * Returns the current vertical Segment of Curve a.
     * 
	 * @return the current vertical Segment of Curve a.
	 */
	public Segment segmentA() {
        if (!init) throw new IllegalStateException();
	    return segmentA;
	}
	
	/**
	 * Returns the current vertical Segment of Curve b.
     * 
	 * @return the current vertical Segment of Curve b.
	 */
	public Segment segmentB() {
        if (!init) throw new IllegalStateException();
	    return segmentB;
	}
    
//	/**
//	 * Returns the current vertical subsegment of Curve a.
//     * 
//	 * @return the current vertical subsegment of Curve a.
//	 */
//	public Segment subSegmentA() {
//        if (!init) throw new IllegalStateException();
//	    return new Segment(xStartA, yStart, segmentA.s());
//	}
//	
//	/**
//	 * Returns the current vertical subsegment of Curve b.
//     * 
//	 * @return the current vertical subsegment of Curve b.
//	 */
//	public Segment subSegmentB() {
//        if (!init) throw new IllegalStateException();
//	    return new Segment(xStartB, yStart, segmentB.s());
//	}
	
	/**
	 * Returns the x-value of Curve a at the start of the current vertical Subsegment.
     * 
	 * @return the x-value of Curve a at the start of the current vertical Subsegment.
	 */
	public double xStartA() {
        if (!init) throw new IllegalStateException();
	    return xStartA;
	}
	
	/**
	 * Returns the x-value of Curve a at the end of the current vertical Subsegment.
     * 
	 * @return the x-value of Curve a at the end of the current vertical Subsegment.
	 */
	public double xEndA() {
        if (!init) throw new IllegalStateException();
	    return xEndA;
	}
	
	/**
	 * Returns the slope of the current vertical Segment of Curve a.
     * 
	 * @return the slope of the current vertical Segment of Curve a.
	 */
	public double sA() {
        if (!init) throw new IllegalStateException();
	    return segmentA.s();
	}
	
	/**
	 * Returns the x-value of Curve b at the start of the current vertical Subsegment.
     *
	 * @return the x-value of Curve b at the start of the current vertical Subsegment.
	 */
	public double xStartB() {
        if (!init) throw new IllegalStateException();
	    return xStartB;
	}
	
	/**
	 * Returns the x-value of Curve b at the end of the current vertical Subsegment.
     * 
	 * @return the x-value of Curve b at the end of the current vertical Subsegment.
	 */
	public double xEndB() {
        if (!init) throw new IllegalStateException();
	    return xEndB;
	}
	
	/**
	 * Returns the slope of the current vertical Segment of Curve b.
     * 
	 * @return the slope of the current vertical Segment of Curve b.
	 */
	public double sB() {
        if (!init) throw new IllegalStateException();
	    return segmentB.s();
	}
	
    /**
     * Returns the index of the current vertical SubSegment.
     * 
     * @return the index of the current vertical SubSegment.
     */
    public int index() {
        if (!init) throw new IllegalStateException();
        return pointer;
    }
    
    private Curve a, b;
	private CurveSegmentIterator iterA, iterB;
	private int pointer;
    private double yMax;
    private double yStart, yEnd;
	private Segment segmentA, segmentB;
	private double xStartA, xEndA; 
	private double xStartB, xEndB;
    private double yEndA, yEndB;
	private boolean aIsNew, bIsNew, aIsRenewed, bIsRenewed, init;
 }