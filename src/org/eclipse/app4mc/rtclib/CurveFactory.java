/* CurveFactory implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch)

Created: 02.11.2005 by Ernesto Wandeler
*/

package org.eclipse.app4mc.rtclib;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.MathUtil;
import org.eclipse.app4mc.rtclib.util.Messages;
import org.eclipse.app4mc.rtclib.util.Point;

/**
 * <code>CurveFactory</code> provides methods to create instances of Curve.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: CurveFactory.java 968 2015-09-11 11:37:36Z thiele $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating 
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class CurveFactory {

    /**
	 * Creates a Curve that represents the upper arrival curve of an event 
	 * stream with the given <code>period</code>, <code>jitter</code> and 
     * <code>minimum event inter-arrival distance</code>.
     * <p>
     * 
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>p >= 1</li>
     * <li>j >= 0</li>
     * <li>d >= 0</li>
     * </ul>
     * 
	 * @param p the period. 
	 * @param j the jitter. 
	 * @param d the minimum event inter-arrival distance. 
     * @param name the name of the created Curve.
	 * @return the upper arrival curve of an event stream with the given
	 * <code>period</code>, <code>jitter</code> and 
     * <code>minimum event inter-arrival distance</code>.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
	 */
	public static Curve createUpperPJDCurve(long p, double j, double d, 
            String name) {
        if (p < 1 || DoubleMath.lt(j, 0) || DoubleMath.lt(d, 0) || DoubleMath.geq(d, (double)p)) {
            throw new IllegalArgumentException();
        } 

        double px0, py0;
        double pdy = 1;
		double pds = 1/(double)p;
        SegmentList aper = new SegmentList();
		SegmentList per = new SegmentList(1);
		per.add(new Segment(0,0,0));		
		if (DoubleMath.gt(d, 0.0)) {
			Segment lowerPeriodicBound = new Segment(-j, 0, pds);
			Segment lowerBurstBound = new Segment(0, 0, 1/d);
			Point crossPoint = SegmentMath
                    .crossPoint(lowerPeriodicBound, lowerBurstBound);
			py0 = Math.ceil(crossPoint.y());
			px0 = (p * py0) - j;			
			for (int i = 0; i < py0; i++) {
				aper.add(new Segment(i * d, i + 1, 0));
			}
		} else if (DoubleMath.gt(j, 0.0)) {
		    double burstHeight = Math.ceil(j/p);
            py0 = burstHeight;
		    px0 = (burstHeight * p) - j;
            if (!MathUtil.isFullNumber(j / (double)p)) {
                aper.add(new Segment(0, burstHeight, 0));
            }
		} else {
            py0 = 0;
		    px0 = 0;
		}
        py0 = py0 + 1;
        return new Curve(aper, per, px0, py0, p, pdy, name);
	}
	
    /**
     * Creates a Curve that represents the lower arrival curve of an event 
     * stream with the given <code>period</code>, <code>jitter</code> and 
     * <code>minimum event inter-arrival distance</code>.
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>p >= 1</li>
     * <li>j >= 0</li>
     * <li>d >= 0</li>
     * </ul>
     * 
     * @param p the period. 
     * @param j the jitter. 
     * @param d the minimum event inter-arrival distance. 
     * @param name the name of the created Curve.
     * @return the lower arrival curve of an event stream with the given
     * <code>period</code>, <code>jitter</code> and 
     * <code>minimum event inter-arrival distance</code>.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
     */
	public static Curve createLowerPJDCurve(long p, double j, double d, 
            String name) {
        if (p < 1 || DoubleMath.lt(j, 0) || DoubleMath.lt(d, 0) || DoubleMath.geq(d, (double)p)) {
            throw new IllegalArgumentException();
        } 
        
        double px0 = j + p;
		double py0 = 1;		
        double pdy = 1;
        SegmentList aper = new SegmentList();
        aper.add(new Segment(0, 0, 0));
        SegmentList per = new SegmentList();
        per.add(new Segment(0,0,0));
        return new Curve(aper, per, px0, py0, p, pdy, name);
	}
    
    /**
     * Creates a Curve that represents the upper service
     * curve of a TDMA resource with a given <code>cycle</code>, 
     * <code>slot</code> and <code>bandwith</code>.
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>s >= 0 ^ s <= c</li>
     * <li>c >= 1</li>
     * <li>b >= 0</li>
     * </ul>
     * 
	 * @param s the slot length allocated to this service.
	 * @param c the cycle length of the TDMA resource.
	 * @param b the bandwidth of the TDMA resource.
	 * @param name the name of the created Curve.
	 * @return the upper service curve of a TDMA resource with the given 
     * <code>cycle</code>, <code>slot</code> and <code>bandwidth</code>.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
	 */
	public static Curve createUpperTDMACurve(double s, long c, double b, 
            String name) {        
        if (DoubleMath.lt(s, 0.0) || DoubleMath.gt(s, c) || c < 1 
                || DoubleMath.lt(b, 0)) {
            throw new IllegalArgumentException();
        }  
        
        if (DoubleMath.eq(s, 0.0)) {
            SegmentList aper = new SegmentList(1);
            aper.add(new Segment(0,0,0));
            return new Curve(aper, name);
        } else if (DoubleMath.lt(s, (double)c)) {
            SegmentList per = new SegmentList(2);
            per.add(new Segment(0, 0, b));
            per.add(new Segment(s, s * b, 0));
			return new Curve(per, 0.0, c, s * b, name);	    
	    } else if (DoubleMath.eq(s, (double)c)){
            SegmentList aper = new SegmentList(1);
            aper.add(new Segment(0,0,b));
	        return new Curve(aper, name);
	    } else {
	        return null;
	    }
	}
	
    /**
     * Creates a Curve that represents the lower service
     * curve of a TDMA resource with a given <code>cycle</code>, 
     * <code>slot</code> and <code>bandwith</code>.
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>s >= 0 ^ s <= c</li>
     * <li>c >= 1</li>
     * <li>b >= 0</li>
     * </ul>
     * 
     * @param s the slot length allocated to this service.
     * @param c the cycle length of the TDMA resource.
     * @param b the bandwidth of the TDMA resource.
     * @param name the name of the created Curve.
     * @return the lower service curve of a TDMA resource with the given 
     * <code>cycle</code>, <code>slot</code> and <code>bandwidth</code>.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
     */
    public static Curve createLowerTDMACurve(double s, long c, double b, 
            String name) {
        if (DoubleMath.lt(s, 0.0) || DoubleMath.gt(s, c) || c < 1 
                || DoubleMath.lt(b, 0)) {
            throw new IllegalArgumentException();
        } 
        
        if (DoubleMath.eq(s, 0.0)) {
            SegmentList aper = new SegmentList(1);
            aper.add(new Segment(0,0,0));
            return new Curve(aper, name);
        } else if (DoubleMath.lt(s, (double)c)) {
            SegmentList per = new SegmentList(2);
            per.add(new Segment(0,0,0));
            per.add(new Segment(c - s,0,b));
			return new Curve(per, 0.0, c, s * b, name);
	    } else if (DoubleMath.eq(s, (double)c)){
            SegmentList aper = new SegmentList(1);
            aper.add(new Segment(0,0,b));
            return new Curve(aper, name);
	    } else {
	        return null;
	    }
	}

    /**
     * Creates a Curve that represents the upper service curve of a bounded
     * delay resource with a given <code>delay</code> and 
     * <code>bandwidth</code>.
     * <p>
     * Alternatively, the created Curve can also be interpreted as an ordinary
     * two-segment curve.
     * <p>
     * In research literature, the following name 
     * substitutions are sometimes used: 
     * <ul>
     * <li>d = Delta/2</li>
     * <li>b = alpha</li>
     * </ul>
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>d >= 0</li>
     * <li>b >= 0</li>
     * </ul>
     * 
     * @param d the delay.
     * @param b the bandwidth of the resource.
     * @param name the name of the created Curve.
     * @return the upper service curve of a bounded delay resource with the 
     * given <code>delay</code> and <code>bandwidth</code>.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
     */
    public static Curve createUpperBoundedDelayCurve(double d, double b, 
            String name) {
        if (DoubleMath.lt(d, 0.0) || DoubleMath.lt(b, 0.0)) {
            throw new IllegalArgumentException();
        } 
        
        SegmentList aper = new SegmentList(1);
        aper.add(new Segment(0,d*b,b));
        return new Curve(aper, name);
    }
    
    /**
     * Creates the two-segment bounded-delay curve that is an upper bound to
     * the passed curve c.
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>c must be wide-sense increasing</li>
     * </ul>
     * 
     * @param c the curve.
     * @return the two-segment bounded-delay curve that is an upper bound to
     * the passed curve c.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
     */
    public static Curve createUpperBoundedDelayCurve(Curve c) {
        if (!CurveChecker.isWideSenseIncreasing(c)) {
            throw new IllegalArgumentException(Messages.NON_WSI_CURVE);
        }
        
        double dy = Double.NEGATIVE_INFINITY;
        double s;
        CurveSegmentIterator iter;
        SegmentList list = new SegmentList(1);
        if (c.hasPeriodicPart()) {
            s = Math.max(0, c.pds());
            iter = c.segmentIterator(c.px0() + c.pdx());
        } else {            
            Segment bound = c.upperBound();
            s = Math.max(0, bound.s());
            iter = c.segmentIterator(bound.x());
            dy = Math.max(dy, bound.y() - (bound.x() * s));       
        }  
        while(iter.next()) {
            dy = Math.max(dy, iter.yStart() - (iter.xStart() * s));
            dy = Math.max(dy, iter.yEnd() - (iter.xEnd() * s));
        }
        list.add(new Segment(0, Math.max(0, dy), s));
        return new Curve(list);
    }

    /**
     * Creates a Curve that represents the lower service curve of a bounded
     * delay resource with a given <code>delay</code> and 
     * <code>bandwidth</code>.
     * <p>
     * Alternatively, the created Curve can also be interpreted as an ordinary
     * two-segment curve.
     * <p>
     * In research literature, the following name 
     * substitutions are sometimes used: 
     * <ul>
     * <li>d = Delta/2</li>
     * <li>b = alpha</li>
     * </ul>
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>d >= 0</li>
     * <li>b >= 0</li>
     * </ul>
     * 
     * @param d the delay.
     * @param b the bandwidth of the resource.
     * @param name the name of the created Curve.
     * @return the lower service curve of a bounded delay resource with the 
     * given <code>delay</code> and <code>bandwidth</code>.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
     */
    public static Curve createLowerBoundedDelayCurve(double d, double b, String name) {
        if (DoubleMath.lt(d, 0.0) || DoubleMath.lt(b, 0.0)) {
            throw new IllegalArgumentException();
        } 
        
        SegmentList aper = new SegmentList(2);
        if (DoubleMath.neq(d, 0.0)) {
            aper.add(new Segment(0,0,0));
        }
        aper.add(new Segment(d,0,b));
        return new Curve(aper, name);
    }
    
    /**
     * Creates the two-segment bounded-delay curve that is a lower bound to
     * the passed curve c.
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>c must be wide-sense increasing</li>
     * </ul>
     * 
     * @param c the curve.
     * @return the two-segment bounded-delay curve that is a lower bound to
     * the passed curve c.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
     */
    public static Curve createLowerBoundedDelayCurve(Curve c) {
        if (!CurveChecker.isWideSenseIncreasing(c)) {
            throw new IllegalArgumentException(Messages.NON_WSI_CURVE);
        }
        double dy = Double.POSITIVE_INFINITY;
        double s;
        CurveSegmentIterator iter;
        SegmentList list = new SegmentList(2);
        if (c.hasPeriodicPart()) {
            s = Math.max(0, c.pds());
            iter = c.segmentIterator(c.px0() + c.pdx());
        } else {            
            Segment bound = c.upperBound();
            s = Math.max(0, bound.s());
            iter = c.segmentIterator(bound.x());
            dy = Math.min(dy, bound.y() - (bound.x() * s));       
        }  
        while(iter.next()) {
            dy = Math.min(dy, iter.yStart() - (iter.xStart() * s));
            dy = Math.min(dy, iter.yEnd() - (iter.xEnd() * s));
        }
        dy = Math.min(0, dy);
        if (DoubleMath.eq(dy, 0.0)) {
            list.add(new Segment(0, 0, s));
        } else {
            list.add(new Segment(0, 0, 0));
            if (DoubleMath.gt(s, 0.0)) {
                list.add(new Segment( - dy / s, 0, s));
            }
        }
        return new Curve(list);
    }

    /**
     * Creates a Curve that represents the upper service curve of a periodic
     * resource model with the given <code>share</code>, <code>period</code> and 
     * <code>bandwidth</code>.
     * <p>
     * Alternatively, the created Curve can also be interpreted as an ordinary
     * two-segment curve.
     * <p>
     * In research literature, the following name 
     * substitutions are sometimes used: 
     * <ul>
     * <li>s = Theta</li>
     * <li>p = Pi</li>
     * <li>b = 1</li>
     * </ul>
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>s >= 0 ^ s <= p</li>
     * <li>p >= 1</li>
     * <li>b >= 0</li>
     * </ul>
     * 
     * @param s the periodic service share slot of the resource.
     * @param p the period of the resource.
     * @param b the bandwidth of the resource.
     * @param name the name of the created Curve.
     * @return the upper service curve of a periodec service resource with the 
     * <code>share</code>, <code>period</code> and <code>bandwidth</code>.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
     */
    public static Curve createUpperPeriodicServiceCurve(double s, long p, 
            double b, String name) {
        if (DoubleMath.lt(s, 0.0) || DoubleMath.gt(s, p) || p < 1 
                || DoubleMath.lt(b, 0)) {
            throw new IllegalArgumentException();
        }  
        
        if (DoubleMath.eq(s, 0.0)) {
            SegmentList aper = new SegmentList(1);
            aper.add(new Segment(0,0,0));
            return new Curve(aper, name);
        } else if (DoubleMath.lt(s, (double)p)) {
            SegmentList aper = new SegmentList(1);
            aper.add(new Segment(0,0,b));
            SegmentList per = new SegmentList(2);
            per.add(new Segment(0, 0, b));
            per.add(new Segment(s, s * b, 0));
            return new Curve(aper, per, s,  s * b, p, s * b, name);     
        } else if (DoubleMath.eq(s, (double)p)){
            SegmentList aper = new SegmentList(1);
            aper.add(new Segment(0,0,b));
            return new Curve(aper, name);
        } else {
            return null;
        }
    }

    /**
     * Creates a Curve that represents the lower service curve of a periodic
     * resource model with the given <code>share</code>, <code>period</code> and 
     * <code>bandwidth</code>.
     * <p>
     * Alternatively, the created Curve can also be interpreted as an ordinary
     * two-segment curve.
     * <p>
     * In research literature, the following name 
     * substitutions are sometimes used: 
     * <ul>
     * <li>s = Theta</li>
     * <li>p = Pi</li>
     * <li>b = 1</li>
     * </ul>
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>s >= 0 ^ s <= p</li>
     * <li>p >= 1</li>
     * <li>b >= 0</li>
     * </ul>
     * 
     * @param s the periodic service share slot of the resource.
     * @param p the period of the resource.
     * @param b the bandwidth of the resource.
     * @param name the name of the created Curve.
     * @return the lower service curve of a periodec service resource with the 
     * <code>share</code>, <code>period</code> and <code>bandwidth</code>.
     * @exception IllegalArgumentException if the parameters do not fulfill
     * the specified conditions.
     */
    public static Curve createLowerPeriodicServiceCurve(double s, long p, double b, String name) {
        if (DoubleMath.lt(s, 0.0) || DoubleMath.gt(s, p) || p < 1 
                || DoubleMath.lt(b, 0)) {
            throw new IllegalArgumentException();
        }  
        
        Curve result = createLowerTDMACurve(s, p, b, name);
        double dx = ((double)p - s);
        result.move(dx, 0.0);
        return result;
    }
    
    
    
    /**
     * (Simon Perathoner)
     * This constructor is intended for use in SymTA/S (MPA Plugin).
     * The array samples contains the arrival times of events in
     * an increasing order.
     * For instance, samples = [d1, d2, ..., dn] specifies an 
     * upper arrival curve alphau with minimum distance d1, d2, ..., dn between 
     * 1, 2, ..., n events, respectively. In other words, we have 
     * alphau^-1(1)=d1, alphau^-1(2)=d2, etc.    
     * The finite specification is extended to an infinite curve 
     * by (conservative) periodic repetition of the sampled curve part.  
     */
    public static Curve createConservativeUpperCurveFromFiniteSampleArray(double[] samples) {
    	Curve result;
    	Curve convr, minr;
    	long eventCount;
    	long period;
    	Double periodD; 
    	double lastEventTime;
    	SegmentList segList = new SegmentList();
  	    	    	    	
    	eventCount = 0;
    	lastEventTime = 0.0;
    	if (samples != null && samples.length > 1) {
	    	for (double eventTime : samples){
	    		if (DoubleMath.gt(eventTime, lastEventTime)){
	    			segList.add(new Segment(lastEventTime, eventCount, 0.0));
	    			lastEventTime = eventTime;
	    		}
	    		else if (DoubleMath.lt(eventTime, lastEventTime)) {
	    			throw new IllegalArgumentException(Messages.NON_WSI_CURVE); 
	    		}
	    		eventCount++;
	    	}	    	
    	}
    	else {
    		throw new IllegalArgumentException(Messages.ILLEGAL_ARGUMENT_SAMPLE_ARRAY_TOO_SMALL);
    	}
    	periodD = new Double(lastEventTime);    	  	
    	period = periodD.longValue();
    	if (period < 1) {
    	    throw new IllegalArgumentException(Messages.PERIOD_ZERO);
    	}
    	else {
        	segList.trimLT(period);
        	result = new Curve(segList, 0.0, period, segList.lastSegment().yAt(period));
        	result.simplify();
        	       	
        	// Period was converted to long, i.e. could have become smaller. 
        	// Thus, the resulting curve could potentially not be sub-additive anymore.
        	
        	 //  Improve sub-additivity of curve.        	
            convr = result.clone();
            minr = result.clone();
        	for (int i = 0; i < 5; i++) {        // Hack: This should be a configurable parameter.
        	  convr = CurveMath.minPlusConv(convr, result); 	 
        	  convr.simplify();
        	  minr = CurveMath.min(minr, convr);
        	  minr.simplify();
            }        	               	
    	}    	      
    	return minr;
	    //return result;
    }    
    
    
    /**
     * (Simon Perathoner)
     * This constructor is intended for use in SymTA/S (MPA Plugin).
     * The array samples contains the arrival times of events in
     * an increasing order.
     * For instance, samples = [d1, d2, ..., dn] specifies a 
     * lower arrival curve alphal with maximum distance d1, d2, ..., dn between 
     * 1, 2, ..., n events, respectively. In other words, we have 
     * alphal^-1(1)=d1, alphal^-1(2)=d2, etc.    
     * The finite specification is extended to an infinite curve 
     * by (conservative) periodic repetition of the sampled curve part.  
     */
    public static Curve createConservativeLowerCurveFromFiniteSampleArray(double[] samples) {
    	Curve result;
    	Curve convr, maxr;
    	long eventCount;
    	long period;
    	Double periodD; 
    	Long periodL;
    	double lastEventTime;
    	SegmentList segList = new SegmentList();
  	    	
    	eventCount = -1;
    	lastEventTime = 0.0;
    	if (samples != null && samples.length > 1) {
	    	for (double eventTime : samples){
	    		if (DoubleMath.gt(eventTime, lastEventTime)){
	    			segList.add(new Segment(lastEventTime, eventCount, 0.0));
	    			lastEventTime = eventTime;
	    		}
	    		else if (DoubleMath.lt(eventTime, lastEventTime)) {
	    			throw new IllegalArgumentException(Messages.NON_WSI_CURVE); 
	    		}
	    		eventCount++;
	    	}	    	
    	}
    	else {
    		throw new IllegalArgumentException(Messages.ILLEGAL_ARGUMENT_SAMPLE_ARRAY_TOO_SMALL);
    	}
    	periodD = new Double(lastEventTime);    	    	
    	period = periodD.longValue();
        periodL = new Long(period);
  	    if (DoubleMath.lt(periodL.floatValue(), lastEventTime)) {   
  	    	// Period was reduced to next smaller integer
  	       period = period + 1;
  	    }  	    	  	    	
        result = new Curve(segList, 0.0, period, segList.lastSegment().yAt(period) + 1);
        result.simplify();
 
        // The resulting curve could potentially be not super-additive anymore.
        
        //  Improve super-additivity of curve.        	
        convr = result.clone();
        maxr = result.clone();
        for (int i = 0; i < 5; i++) {        // Hack: This should be a configurable parameter.
           convr = CurveMath.maxPlusConv(convr, result); 	 
           convr.simplify();
           maxr = CurveMath.max(maxr, convr);
           maxr.simplify();
        }        	               	    	    	      
    	return maxr;
        // return result;
    }        
    

    
   
    
    
    /**
     * Creates a Curve from a textual representation that corresponds to the
     * command that is used in Matlab to create a Curve. 
     * <p>
     * Any of the following syntax is recognized by this method:
     * <pre>
     * name = curve(aper, per, px0, py0, period, pdy, 'name') 
     * name = curve(aper, per, px0, py0, period, pdy) 
     * name = curve(aper, per, px0, py0, period, 'name') 
     * name = curve(aper, per, px0, py0, period) 
     * name = curve(per, py0, period, pdy, 'name') 
     * name = curve(per, py0, period, pdy) 
     * name = curve(per, py0, period, 'name') 
     * name = curve(per, py0, period) 
     * name = curve(per, period, 'name') 
     * name = curve(per, period)
     * name = curve(aper, 'name') 
     * name = curve(aper)
     * </pre>
     * With the following parameters:
     * <ul>
     * <li>name - the name of the Curve. E.g. "A"</li>
     * <li>aper - the aperiodic part of the Curve, encoded as a Double[][], 
     * where every segment is represented by the triple [x0 y0 slope]. 
     * E.g. "[[0 0 0];[1 0 0.5]]"</li>
     * <li>per - the periodic part of the Curve, encoded as a Double[][], 
     * where every segment is represented by the triple [x0 y0 slope]. 
     * E.g. "[[0 0 0];[1 0 0.5]]"</li>
     * <li>period - the period of the Curve. The value of this parameters is
     * interpreted as a Long!</li>
     * <li>pdy - the offset between two periodic repetidions.</li>
     * <li>px0 - the x-value of the start-coordinate of the periodic part</li>
     * <li>py0 - the y-value of the start-coordinate of the periodic part</li>
     * </ul>
     * <p>
     * Additionally, to the above specified syntax, any of the strings without 
     * "name = " is also recognized, e.g.:<br><br>
     * <pre>
     * curve(aper, per, px0, py0, period, pdy, 'name') 
     * </pre>
     * If name of the curve is defined at the left side of the equal sign, 
     * as well as on the right side, then the value on the right side is taken.
     * 
     * @param str the textural representation of a Curve.
     * @return the Curve that is represented by str.
     * @exception IllegalArgumentException if the string does not conform to
     * the specified syntax.
     */
    public static Curve createFromString(String str) {
        SegmentList aper, per;
        double px0, py0, pdy;
        long period;
        String aperStr = "", perStr = "", 
                px0Str = "", py0Str = "", 
                periodStr = "", pdyStr = "", 
                name = "";        
        String[] temp;
        
        // Check for a name by assignment.
        temp = str.split("=");
        if (temp.length > 1) {
            name = temp[0].trim();
            str = temp[1].trim();
        }
    
        try {
            str = (str.substring(str.indexOf("(") + 1, 
                    str.lastIndexOf(")"))).trim();
            temp = str.split(",");

            // Check for a name in the method call
            int tempLen = temp.length;
            if (tempLen > 1) {
                try {
                    Double.valueOf(temp[tempLen - 1]);
                } catch (NumberFormatException e) {
                    name = temp[tempLen - 1].trim();
                    name = (name.substring(name.indexOf("'") + 1, 
                            name.lastIndexOf("'"))).trim();
                    tempLen = tempLen - 1;
                }
            }
        
            // Match correct method call footprint
            switch(tempLen) {
            	case 6: 
                    aperStr = temp[0].trim(); 
                    perStr = temp[1].trim(); 
                    px0Str = temp[2].trim(); 
                    py0Str = temp[3].trim(); 
            	    periodStr = temp[4].trim();
                    pdyStr = temp[5].trim();
            	    break;
            	case 5: 
                    aperStr = temp[0].trim(); 
                    perStr = temp[1].trim(); 
                    px0Str = temp[2].trim(); 
                    py0Str = temp[3].trim(); 
            	    periodStr = temp[4].trim();
            	    break;
            	case 4:
                    perStr = temp[0].trim(); 
                    py0Str = temp[1].trim(); 
            	    periodStr = temp[2].trim();
                    pdyStr = temp[3].trim();
            	    break;
            	case 3:
                    perStr = temp[0].trim(); 
                    py0Str = temp[1].trim();
            	    periodStr = temp[2].trim();
            	    break;
            	case 2:
                    perStr = temp[0].trim(); 
            	    periodStr = temp[1].trim();
            	    break;
            	case 1:
                    aperStr = temp[0].trim();
            	    break;
            	default: 
            	    throw new IllegalArgumentException();
            }
            if (aperStr.trim() != "") {
                aper = new SegmentList(aperStr);          
            } else {
                aper = null;
            }
            if (perStr.trim() != "") {
                per = new SegmentList(perStr);          
            } else {
                per = null;	            
            }
            if (px0Str.trim() != "") {
                px0 = Double.valueOf(px0Str).doubleValue();
            } else {
                px0 = Double.NaN;
            }
            if (py0Str.trim() != "") {
                py0 = Double.valueOf(py0Str).doubleValue();
            } else {
                py0 = Double.NaN;
            }
            if (periodStr.trim() != "") {
                period = Long.valueOf(periodStr).longValue();
            } else {
                period = 0;
            }  
            if (pdyStr.trim() != "") {
                pdy = Double.valueOf(pdyStr).doubleValue();
            } else {
                pdy = Double.NaN;
            }
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
        return new Curve(aper, per, px0, py0, period, pdy, name);
    }

    /**
     * Imports a set of Curves from a text file.
     * <p>
     * Every line in the text file must either be a comment, a blank line, or
     * must contain a string representation of a curve that is accepted by
     * createFromString():
     * <pre>
     * %Comment
     * 
     * curveA = Curve([[0 2 2];[1 4 1];[3 6 0.5]],[[0 0 0 ];[5 0 2]], 23, 19, 7)
	 * curveB = Curve([[0 2 2];[1 4 1];[3 6 0.5]],[[0 0 0 ];[5 0 2]], 23, 7)
	 * curveC = Curve([[0 0 0 ];[5 0 2]], 7)
	 * curveD = Curve([[0 2 2];[1 4 1];[3 6 0.5]])
     * </pre>
     *  
	 * @param filename the name of the text file
     * @return a HashMap of Curves, with keys equalling  the curve names in the 
     * text file.
     * @exception IllegalArgumentException - if the input of the text file does
     * not correspond to the specification.
     * @exception IOException if I/O problems occur.
     * @exception FileNotFoundException if the text file cannot be found.
     */
    public static HashMap<String, Curve> importCurvesFromFile(String filename) 
    		throws IOException, FileNotFoundException {
        HashMap<String, Curve> curveMap = new HashMap<String, Curve>();
        String line;
        String[] elements;
        BufferedReader rdr = new BufferedReader(new FileReader(filename));
        while ((line = rdr.readLine()) != null) {
            if (line.startsWith("%")) { // Ignore comments
                continue;
            }
            if (line.trim() == "") { // Ignore empty lines
                continue;
            }
            elements = line.split("=");
            if (elements.length != 2) { // Ignore lines without assigments
                continue;
            }
            curveMap.put(elements[0].trim(), 
                    CurveFactory.createFromString(elements[0].trim() + "=" + elements[1].trim()));
        }
        rdr.close();
        return curveMap;
    }
        
    /**
     * Imports a Curve from a text file.
     * <p>
     * Every line in the text file must either be a comment, a blank line, or
     * must contain a string representation of a curve that is accepted by
     * createFromString():
     * <pre>
     * %Comment
     * 
     * curveA = Curve([[0 2 2];[1 4 1];[3 6 0.5]],[[0 0 0 ];[5 0 2]], 23, 19, 7)
     * </pre>
     *  
     * @param filename the name of the text file
     * @return the imported Curve.
     * @exception IllegalArgumentException - if the input of the text file does
     * not correspond to the specification.
     * @exception IOException if I/O problems occur.
     * @exception FileNotFoundException if the text file cannot be found.
     */
    public static Curve importCurveFromFile(String filename) 
    		throws IOException, FileNotFoundException {       
        String line;
        String[] elements;
        BufferedReader rdr = new BufferedReader(new FileReader(filename));
        while ((line = rdr.readLine()) != null) {
            if (line.startsWith("%")) { // Ignore comments
                continue;
            }
            if (line.trim() == "") { // Ignore empty lines
                continue;
            }
            elements = line.split("=");
            if (elements.length == 2) elements[0] = elements[1];
            rdr.close();
            return CurveFactory.createFromString(elements[0].trim());
        }
        rdr.close();
        throw new IllegalArgumentException();
    }    
    
    /**
     * Exports a HashMap of Curves, such that it can be imported by 
     * importCurvesFromFile(String)
     * 
     * @param map the HashMap of Curves
     * @param filename the name of the file to which the Curves are exported
     * @exception IOException if I/O problems occur.
     * @exception FileNotFoundException if the text file cannot be found.
     */
    public static void exportCurvesToFile(HashMap<String,Curve> map, String filename) 
    		throws IOException, FileNotFoundException {       
        PrintWriter out = new PrintWriter(
                new BufferedWriter(new FileWriter(filename)));
        Set<String> keySet = map.keySet();
        String[] setType = new String[0];
        String[] keyArray = (String[])keySet.toArray(setType);
        Arrays.sort(keyArray);
        for (int i = 0; i < keyArray.length; i++) {
            out.println(keyArray[i] + " = " 
                    + ((Curve)map.get(keyArray[i])).toMatlabString()
                    + ";");
        }
        out.close();
    }
     
    /**
     * Exports a Curve, such that it can be imported by 
     * importCurveFromFile(String)
     * 
     * @param curve the Curve
     * @param filename the name of the file to which the Curve is exported
     * @exception IOException if I/O problems occur.
     * @exception FileNotFoundException if the text file cannot be found.
     */
    public static void exportCurveToFile(Curve curve, String filename) 
			throws IOException, FileNotFoundException {       
        PrintWriter out = new PrintWriter(
                new BufferedWriter(new FileWriter(filename)));
        out.println(curve.toMatlabString());
        out.close();
    }
}
