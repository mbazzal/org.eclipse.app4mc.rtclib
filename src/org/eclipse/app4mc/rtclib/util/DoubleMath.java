/* DoubleMath implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit          none (wandeler@tik.ee.ethz.ch)

Created: 18.02.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib.util;

import org.eclipse.app4mc.rtclib.Parameters;

/**
 * <code>SegmentMath</code> provides methods to compare doubles.
 * <br>
 * Two doubles <code>a</code> and <code>b</code> are considered equal (a==b) if
 * <br><br>
 * <code>b - Parameters.PRECISION < a < b + Parameters.PRECISION</code>
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: DoubleMath.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    none (wandeler@tik.ee.thz.ch)
 */
public class DoubleMath {

    /**
     * Returns true if a == b within a precision specified by 
     * Parameters.PRECISION
     * 
     * @param a the first double
     * @param b the second double
     * @return true if a == b
     */
    public static boolean eq(double a, double b) {
        return (Math.abs(a - b) < Parameters.PRECISION);
    }

    /**
     * Returns true if a != b within a precision specified by 
     * Parameters.PRECISION
     * 
     * @param a the first double
     * @param b the second double
     * @return true if a != b
     */
    public static boolean neq(double a, double b) {
        return (Math.abs(a - b) >= Parameters.PRECISION);
    }
    
    /**
     * Returns true if a < b within a precision specified by 
     * Parameters.PRECISION
     * 
     * @param a the first double
     * @param b the second double
     * @return true if a < b
     */
    public static boolean lt(double a, double b) {
        return (a <= b - Parameters.PRECISION);
    }
    
    /**
     * Returns true if a <= b within a precision specified by 
     * Parameters.PRECISION
     * 
     * @param a the first double
     * @param b the second double
     * @return true if a <= b
     */
    public static boolean leq(double a, double b) {
        return (a < b + Parameters.PRECISION);
    }
    
    /**
     * Returns true if a > b within a precision specified by 
     * Parameters.PRECISION
     * 
     * @param a the first double
     * @param b the second double
     * @return true if a > b
     */
   public static boolean gt(double a, double b) {
        return (a >= b + Parameters.PRECISION);
    }
    
   /**
    * Returns true if a >= b within a precision specified by 
    * Parameters.PRECISION
    * 
    * @param a the first double
    * @param b the second double
    * @return true if a >= b
    */
    public static boolean geq(double a, double b) {
        return (a > b - Parameters.PRECISION);
    }

}
