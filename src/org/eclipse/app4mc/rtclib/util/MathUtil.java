/* MathUtil implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch)			

Created: 28.10.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib.util;

/**
 * <code>MathUtil</code> provides some mathematical methods.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: MathUtil.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class MathUtil {

    /**
     * Computes the hyperperiod of two periods. If either of the periods is 0,
     * then the hyperperiod equals the other period. If both periods are >0, 
     * then the hyperperiod equals the LCM of a and b
     * 
     * @param a the first period.
     * @param b the second period.
     * @return the hyperperiod of a and b.
     */
    public static long computeHyperPeriod(long a, long b) {
        if (a == 0) return b;
        if (b == 0) return a;
        return lcm(a, b);
    }
    
    /**
     * Computes the maximum offset on the y-axis for a curve part of length
     * hyperPeriod(a,b) with periodic offsets of dya or dyb respectively.
     * I.e. if both periods a and b are 0, then the offset is 0. If either
     * of the period is 0, then the maximum offset equals the offset belonging
     * to the other period. Otherwise, the maximum offset is computed as
     * offset = max((HP/a)*dya, (HP/b)*dyb) .
     * 
     * @param a the first period.
     * @param dya the y-offset of the first period.
     * @param b the second period.
     * @param dyb the y-offset of the second period.
     * @return the hyperperiod of a and b.
     */
    public static double computeVHyperPeriod(long a, double dya, long b, double dyb) {
        if (a == 0) {
            if (b == 0) {
                return 0.0;
            } else {
                return dyb;
            }
        } else if (b == 0) {
            return dya;
        } else {
            return Math.max(((double)(lcm(a, b)/a)) * dya,
                    (double)(lcm(a, b)/b) * dyb);
        }
    }
    
	/**
	 * Computes the GCD of two numbers.
     * 
	 * @param a the first number.
	 * @param b the second number.
	 * @return the GCD of a and b.
	 */
	public static long gcd(long a, long b) {
		if (a<=0 || b<=0) return 1;
		else if (a>b) return gcdMain(a,b);
		else if (b>a) return gcdMain(b,a);
		else return a;
	}
	
	/**
	 * Computes the LCM of two numbers.
     * 
	 * @param a the first number.
	 * @param b the second number.
	 * @return the LCM of a and b.
	 */
	public static long lcm(long a, long b) {
		if (a<=0 || b<=0) return 1;
		return( a*b/gcd(a,b) );
	}
	
    /**
     * Rounds number to the precision that was
     * set using setRoundPrecision(). If this precision is not set, a default
     * precision is used.
     * 
     * @param number the number to round.
     * @return number rounded number.
     */
    public static double round(double number) {
	    return (double)Math.round(number * precision) / precision;
	}
    
    /**
     * Sets the precision for the round() method. Numbers passed to round()
     * will be round to d digits after the comma. If d is negative, numbers
     * passed to round() will be rounded to d powers of 10.
     * 
     * @param d the precision for round().
     */
    public static void setRoundPrecision(int d) {
        digits = d;
        precision = (double)Math.pow(10, digits);
    }
	
    /**
     * Returns the precision of the round() method.
     * 
     * @return the precision of the round() method.
     */
    public static int getRoundPrecision() {
        return digits;
    }
    
    /**
     * Returns true if number is a natural number.
     * 
     * @return true if number is a natural number.
     */
    public static boolean isFullNumber(double number) {
        return DoubleMath.eq(number, (double)Math.round(number));
    }
    
	private static long gcdMain(long a, long b) {
		if (b==0) return a;
		return gcdMain(b,a%b);
	}	
	
    private static int digits = 4;
    private static double precision = (double)Math.pow(10, digits);
}


