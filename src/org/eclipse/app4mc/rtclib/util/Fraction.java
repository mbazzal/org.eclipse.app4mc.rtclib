/* Fraction implementation.

Copyright (c) 2004 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit          Red (wandeler@tik.ee.ethz.ch)

Created: 15.11.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib.util;

/**
 * <code>Fraction</code> represents a rational number as a fraction of a 
 * numerator and a denominator.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: Fraction.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Red (wandeler@tik.ee.thz.ch)
 */
public class Fraction implements Cloneable {   
        
    /**
     * Constructs a Fraction numerator/denominator.
     *
     * @param  numerator the numerator of the fraction.
     * @param  denominator the denominator of the fraction.
     **/
    public Fraction(long numerator, long denominator) {
        sign = 1;
        if (numerator < 0) {
            sign *= -1;
            numerator *= -1;
        }
        if (denominator < 0) {
            sign *= -1;
            denominator *= -1;
        }
        num = numerator;
        den = denominator;
    }
    
    
    /**
     * Constructs a Fraction numerator/denominator.
     *
     * @param  numerator the numerator of the fraction.
     * @param  denominator the denominator of the fraction.
     **/
    public Fraction(int numerator, int denominator) {
        sign = 1;
        if (numerator < 0) {
            sign *= -1;
            numerator *= -1;
        }
        if (denominator < 0) {
            sign *= -1;
            denominator *= -1;
        }
        num = numerator;
        den = denominator;
    }
    
    
    /**
     * Constructs a Fraction from a rational number. The rantional number
     * is converted into a representation Nominator/Denominator. In the 
     * worst case, the Fraction approximates the rational number value to
     * within tolerance.
     *
     * @param   rational the rational number to convert.
     * @param   tolerance the tolerance to be met by the Fraction relative
     * to the rational.
     **/
    public Fraction(double rational, double tolerance) {
        if (rational < 0) {
            sign = -1;
            rational *= -1;
        } else
            sign = 1;           
        floatToFraction(rational, tolerance);
    }
    
    
    /**
     * Returns the value of this Fraction as a double.
     *
     * @return the value of this Fraction as a double.
     **/
    public double value() {
        double theNum = (double)this.num;
        double theDen = (double)this.den;            
        return (sign*(theNum/theDen));
    }
    
    /**
     * Returns the sign of this Fraction.
     *
     * @return the sign of this Fraction.
     **/
    public int sign() {           
        return sign;
    }
    
    /**
     * Returns the numerator of this Fraction.
     *
     * @return the numerator of this Fraction.
     **/
    public long numerator() {
            return sign*num;
    }
    
    /**
     * Returns the denominator of this Fraction.
     *
     * @return the denominator of this Fraction.
     **/
    public long denominator() {
        return den;
    }
    
    /** 
     * Returns a String representation of this Fraction.
     * 
     * @return a String representation of this Fraction.
     */
    public String toString() {
        return sign*num + "/" + den;
    }

    /**
     * Approximates an arbitrary floating point number with a
     * rational fraction in the form numerator/denominator.
     *
     * Uses the method of continued fractions.
     *
     * @param  rational the floating point number to be made into a
     * fraction.
     * @param  tol the tolerance that fraction should be created to.
     * The generated fraction will have at least this tolerance.
     **/
    private void floatToFraction(double rational, double tol ) {
        long    ai;                     
        double  denominator;                  
        double  numerator;                    
        double  error = Double.MAX_VALUE;
        double  startX = rational;     
        double  matrixElem01, matrixElem11;              
        
        if (DoubleMath.eq(rational, 0.)) {
            num = 0;
            den = 1;
            return;
        } else if (rational >= Long.MAX_VALUE ) {
            num = 0;
            den = 1;
            return;
        }

        numerator = matrixElem11 = 1.;
        denominator = matrixElem01 = 0.;
    
        while ( error > tol ) {
            double dx;
            double numOld, denomOld;

            ai = (long)rational;
    
            denomOld = denominator;
            denominator = denomOld*ai + matrixElem11;
            numOld = numerator;
            numerator = numOld*ai + matrixElem01;
        
            if (numerator > Long.MAX_VALUE || denominator > Long.MAX_VALUE) {
                num = (long)numerator;
                den = (long)denominator;
                return;
            }
        
            error = Math.abs( startX - numerator/denominator );
    
            if (error <= tol) {
                long aiRounded = Math.round(rational);
                double denomRounded = denomOld*aiRounded + matrixElem11;
                double numRounded = numOld*aiRounded + matrixElem01;
                if ( denomRounded < Long.MAX_VALUE && numRounded < Long.MAX_VALUE ) {
                    ai = aiRounded;
                    numerator = numRounded;
                    denominator = denomRounded;
                    error = Math.abs( startX - numerator/denominator );
                }
            }

            matrixElem01 = numOld;
            matrixElem11 = denomOld;

            dx = (rational - (double)ai);

            if (dx <= 1./Float.MAX_VALUE) break;
            rational = 1./dx;       
        }

        num = (long)numerator;
        den = (long)denominator;
        
        if (num == 0) {
            den = 1;
        }
    }

    private long num;      
    private long den;    
    private int sign=1;     
}



