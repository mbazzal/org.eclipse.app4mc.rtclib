/* SegmentList implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch) 

Created: 31.10.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.Messages;

/**
 * <code>SegmentList</code> represents an ordered list of curve segments.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: SegmentList.java 942 2010-07-26 11:39:35Z psimon $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class SegmentList extends ArrayList<Segment> implements Cloneable {

	
    /**
     * Constructs an empty SegmentList.
     */
    public SegmentList() {
        super();
    }
    
    /**
     * Constructs an empty SegmentList with an specified initial capacity.
     * 
     * @param initialCapacity the initial capacity.
     */
    public SegmentList(int initialCapacity) {
        super(initialCapacity);
    }
    
    /**
     * Constructs a SegmentList with the elements from the Collection
     * 
     * @param c a data structure implementing the Collection interface
     */
    public SegmentList(Collection<Segment> c) {
    	super(c);
    }

    /**
     * Constructs a SegmentList from a double[][] of the form
     * [[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]]
     * This constructor is intended for use with Matlab.
     * 
     * @param data the double[][]
     */
    public SegmentList(double[][] data) {
        super();
        if (data != null) {
            this.ensureCapacity(data.length);
            for (int i = 0; i < data.length; i++) {
                this.add(new Segment(data[i][0], data[i][1], data[i][2]));
            }  
        }
    }
    
    /**
     * Constructs a SegmentList from a String of the form 
     * "[[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]]".
     * 
     * @param str the String of form "[[x1 y1 s1];[x2 y2 s2];[x3 y3 s3]]"
     */
    public SegmentList(String str) {
        super();
        String[] elem;
        str = str.trim();
        str = str.substring(str.indexOf("[") + 1, str.lastIndexOf("]")).trim();
        if (!str.equals("")) {
            elem = str.split(";");
            for (int i = 0; i < elem.length; i++) {
                this.add(new Segment(elem[i]));
            }  
        }
    }
    
    /**
     * Return the Segment at the specified position in this SegmentList.
     * 
     * @param i index of Segment to return.
     * @return the Segment at the specified position in this SegmentList.
     */
    public Segment segment(int i) {
        return (Segment)this.get(i);
    }
    
    /**
     * Return the last Segment in this SegmentList.
     * 
     * @return the last Segment in this SegmentList.
     */
    public Segment lastSegment() {
        return (Segment)this.get(this.size() - 1);
    }
    
    /**
     * Retruns a SegmentListIterator for this SegmentList.
     * 
     * @return a SegmentListIterator for this SegmentList.
     */
    public SegmentListIterator segmentListIterator() {
        return new SegmentListIterator(this);
    }
    
    /**
     * Retruns a SegmentListIterator for this SegmentList.
     * 
     * @param xMax xMax the maximum x-value up to which the SegmentListIterator
     * iterates over this SegmentList.
     * @return a SegmentListIterator for this SegmentList.
     */
    public SegmentListIterator segmentListIterator(double xMax) {
        return new SegmentListIterator(this, xMax);
    }
    
    /**
     * Returns the minimum y-value of the Segments between 0 <= x <= xMax.
     * 
     * @param xMax the maximum x-value
     * @return the minimum y-value of the Segments between 0 <= x <= xMax.
     */
    public double min(double xMax) {
        if (this.size() == 0) {
            return Double.NaN;
        }
        double yMin = Double.POSITIVE_INFINITY;
        SegmentListIterator iter = this.segmentListIterator();
        while(iter.next() && DoubleMath.leq(iter.xStart(), xMax)) {
            yMin = Math.min(yMin, iter.yStart());
            if (DoubleMath.lt(iter.xEnd(), xMax)) {
                yMin = Math.min(yMin, iter.yEnd());
            } else {
                yMin = Math.min(yMin, iter.segment().yAt(xMax));
            }
        }
        return yMin;
    }

    /**
     * Returns the maximum y-value of the Segments between 0 <= x <= xMax.
     * 
     * @param xMax the maximum x-value
     * @return the maximum y-value of the Segments between 0 <= x <= xMax.
     */
    public double max(double xMax) {
        if (this.size() == 0) {
            return Double.NaN;
        }
        double yMax = Double.NEGATIVE_INFINITY;
        SegmentListIterator iter = this.segmentListIterator();
        while(iter.next() && DoubleMath.leq(iter.xStart(), xMax)) {
            yMax = Math.max(yMax, iter.yStart());
            if (DoubleMath.lt(iter.xEnd(), xMax)) {
                yMax = Math.max(yMax, iter.yEnd());
            } else {
                yMax = Math.max(yMax, iter.segment().yAt(xMax));
            }
        }
        return yMax;
    }

    /**
     * Returns true if the x-values of the start points of all Segments in 
     * this SegmentList are strictly increasing.
     * 
     * @return true if the x-values of the start points of all Segments in 
     * this SegmentList are strictly increasing.
     */
    public boolean checkOrder() {
        if (this.size() <= 1) {
            return true;
        }
        double currentX = 0.0;
        double lastX = Double.NEGATIVE_INFINITY;
        SegmentListIterator iter = this.segmentListIterator();
        while (iter.next()) {
            currentX = iter.xStart();
            if (DoubleMath.leq(currentX, lastX)) {
                return false;
            }
            lastX = currentX;
        }
        return true;
    }

    /**
     * Trims this SegmentList by removing all Segments with a start point 
     * xStart >= xMax. 
     * 
     * @param xMax the value from which on Segments are removed. 
     */
    public void trimLT(double xMax) {
        if (DoubleMath.leq(xMax, 0)) {
            this.removeRange(0, this.size());
            return;
        }
    	int numOfSeg = this.size();
    	while (DoubleMath.geq(((Segment)(this.get(numOfSeg - 1))).x(), xMax)) {
    		this.remove(numOfSeg - 1);
            numOfSeg = numOfSeg - 1;
    	}
    }

    /**
     * Trims this SegmentList by removing all Segments with a start point 
     * xStart > xMax. 
     * 
     * @param xMax the value after which Segments are removed. 
     */
    public void trimLEQ(double xMax) {
        if (DoubleMath.lt(xMax, 0)) {
            this.removeRange(0, this.size());
            return;
        }
    	int numOfSeg = this.size();
    	while (DoubleMath.gt(((Segment)(this.get(numOfSeg - 1))).x(), xMax)) {
    		this.remove(numOfSeg - 1);
            numOfSeg = numOfSeg - 1;
    	}
    }
    
    /**
     * Concatenates two SegmentLists. First this SegmentList is trimmed by
     * removing all Segments with xStart >= xConcat. Then the SegmentList 
     * addOn is first moved to the point (xConcat,yConcat), and finaly all 
     * Segments of the moved addOn are added to this SegmentList.
     * 
     * @param addOn the SegmentList to concatenate with this SegmentList.
     * @param xConcat the x-coordinate of the concatenation point.
     * @param yConcat the y-coordinate of the concatenation point.
     */
    public void concat(SegmentList addOn, double xConcat, double yConcat) {
        this.trimLT(xConcat);
        addOn.move(xConcat, yConcat);
        int numOfSeg = addOn.size();
        for (int i = 0; i < numOfSeg; i++) {
            this.add(addOn.get(i));
        }
    }

    /**
     * Scales this SegmentList along the x-axis. The factor must be greater 
     * than 0.
     * 
     * @param factor the factor to scale this SegmentList.
     */
    public void scaleX(double factor) {
        if (DoubleMath.leq(factor, 0)) {
            throw new IllegalArgumentException(
                    Messages.SEGMENT_SCALEX_FACTOR_LEQ_0);
        }  
        int numOfSeg = this.size();
        for (int i = 0; i < numOfSeg; i++) {
            ((Segment)this.get(i)).scaleX(factor);
        }
    }

    /**
     * Scales this SegmentList along the y-axis. 
     * 
     * @param factor the factor to scale this SegmentList.
     */
    public void scaleY(double factor) { 
        int numOfSeg = this.size();
        if (DoubleMath.eq(factor, 0) && numOfSeg > 0) {
            double xStart = ((Segment)this.get(0)).x();
            this.add(0, new Segment(xStart, 0, 0));
            this.removeRange(1, numOfSeg + 1);
        } else {
            for (int i = 0; i < numOfSeg; i++) {
                ((Segment)this.get(i)).scaleY(factor);
            }
        }
    }
    
    /**
     * Moves this SegmentList by moving all Segments by (dx, dy).
     * 
     * @param dx the value by which this SegmentList is moved on the x-axis.
     * @param dy the value by which this SegmentList is moved on the y-axis.
     */
    public void move(double dx, double dy) {
    	int numOfSeg = this.size();
    	for (int i = 0; i < numOfSeg; i++) {
    		((Segment)this.get(i)).move(dx, dy);
    	}
    }

    /**
     * Rounds this SegmentList by rounding all Segments.
     */
    public void round() {
    	int numOfSeg = this.size();
    	for (int i = 0; i < numOfSeg; i++) {
    		((Segment)this.get(i)).round();
    	}
    }
    
    /**
     * Inverts this SegmentList.
     */
    public void invert() {
        SegmentList copy = (SegmentList)this.clone();
        this.removeRange(0, this.size());
        SegmentListIterator iter = copy.segmentListIterator();
        double lastYEnd = 0.0;
        double lastXEnd = 0.0;
        while(iter.next()) {
            if (DoubleMath.lt(iter.yStart(), lastYEnd) || DoubleMath.lt(iter.s(), 0.0)) {
                throw new IllegalOperationException(Messages.SEGMENT_LIST_NOT_INVERTABLE);
            }
            if (DoubleMath.gt(iter.yStart(), lastYEnd)) {
                this.add(new Segment(lastYEnd, lastXEnd, 0.0));
            }
            if (DoubleMath.gt(iter.s(), 0.0)) {
                this.add(new Segment(iter.yStart(), iter.xStart(), 1 / iter.s()));
            }
            else if (Double.isInfinite(iter.xEnd()) && DoubleMath.eq(iter.s(), 0.0)) {
                this.add(new Segment(iter.yStart(), iter.xStart(), Double.POSITIVE_INFINITY));
            }
            lastYEnd = iter.yEnd();
            lastXEnd = iter.xEnd();
        }
    }

    /**
     * Minimizes the number of Segments in this SegmentList by 
     * removing Segments that have the same slope as their predecessor and 
     * that start at the same point as their predecessor ends.
     */
    public void simplify() {
        if (this.size() < 2) return;
        int i;
        double dx, dy;
        Segment current = null, next = null;
        current = this.segment(0);
        i = 1;
        while (i < this.size()) {
            next = this.segment(i);
            dx = next.x() - current.x();
            dy = dx * current.s();
            if (current.moveEquals(dx, dy, next)) {
                this.remove(i);
            } else {
                current = next;
                i = i + 1;
            }
        }
    }
    
    /** 
     * Indicates whether some other SegmentList is equal to this one.
     * 
     * @param object the SegmentList to compare this one with.
     * @return <code>true</code> if this SegmentList equals object, 
     * <code>false</code> otherwise.
     */
    public boolean equals(Object object){
        if (object == null) return false;
        if (this == object) return true;
        SegmentList list = (SegmentList) object;
        if (this.size() != list.size()) return false;
        for (int i = 0; i < this.size(); i++) {
            if (!(this.segment(i).equals(list.segment(i)))) return false;
        }
        return true;
    }
    
    /** 
     * Creates a deep copy of this SegmentList.
     */
    public SegmentList clone() {
    	int numOfSeg = this.size();
    	SegmentList clone = new SegmentList(numOfSeg);
    	for (int i = 0; i < numOfSeg; i++) {
    		clone.add(i, ((Segment)this.get(i)).clone());
    	}
    	return clone;
    }
    
    /** 
     * Returns a String representation of this SegmentList.
     * 
     * @return a String representation of this SegmentList.
     */
    public String toString() {
		String str = "{";
        int numOfSeg = this.size();
        for (int i = 0; i < numOfSeg; i++) {
				str += this.get(i);
		}
		str += "}";
		return str;
    }

    /**
     * Returns the data of this SegmentList in a textual form for export. 
     * 
     * @return the data of this SegmentList in a textual form for export.
     */
    public String toMatlabString() {
        boolean first = true;
        String str = "[";
        int numOfSeg = this.size();
        for (int i = 0; i < numOfSeg; i++) {
            if (!first) {
                str += ";";
            }
            str += ((Segment)this.get(i)).toExportString();
            first = false;
        }
        str += "]";
        return str;      
    }

    private static final long serialVersionUID = 8432168731657357318L;
}


