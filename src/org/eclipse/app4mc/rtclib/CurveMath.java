/* CurveMath implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating individual per method (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit          individual per method (wandeler@tik.ee.ethz.ch)

Created: 03.11.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import java.util.ArrayList;
import java.util.Arrays;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.Fraction;
import org.eclipse.app4mc.rtclib.util.MathUtil;
import org.eclipse.app4mc.rtclib.util.Messages;
import org.eclipse.app4mc.rtclib.util.Point;
import org.eclipse.app4mc.rtclib.util.Vector;

/**
 * <code>CurveMath</code> provides methods to do mathematics with curves.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: CurveMath.java 923 2009-01-30 13:00:42Z nikolays $
 * @since          
 * @ProposedRating individual per method (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    individual per method (wandeler@tik.ee.thz.ch)
 */
public class CurveMath {
    
    /**
     * Computes a + b.
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return a + b.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve add(Curve a, Curve b) {
        // Build the new name
        String name = "";
        if (!a.name().equals("") && !b.name().equals("")) {
            name = "(" + a.name() + "+" + b.name() + ")";
        }
    	// If a or b is a constant, then the other Curve must only be moved.
    	if (a.isConstant()) {
            Curve resultCurve = (Curve)b.clone();
    	    resultCurve.move(0.0, a.y0epsilon());
            resultCurve.setName(name);
    	    return resultCurve;
    	} else if (b.isConstant()) {
            Curve resultCurve = (Curve)a.clone();
    	    resultCurve.move(0.0, b.y0epsilon());	
            resultCurve.setName(name);
    	    return resultCurve;		    
    	}
    	// Compute the period of the resulting Curve, and the point up 
        // to which the Curves must be unfolded.
        long periodNew = MathUtil.computeHyperPeriod(a.period(), b.period());
        double pdyNew = periodNew * (a.lowerBound().s() + b.lowerBound().s());
    	double xUnfold = Math.max(a.lowerBound().x(), b.lowerBound().x()) + periodNew;
    	// Compute the addition of the unfolded Curves.
    	SegmentList result = new SegmentList();
    	CurveSubSegmentIterator iter = new CurveSubSegmentIterator(a, b, xUnfold);
    	while (iter.next()) {
    		result.add(new Segment(iter.xStart(), 
    		                       iter.yStartA() + iter.yStartB(), 
                                   iter.sA() + iter.sB()));
    	}
    	return new Curve(result, periodNew, pdyNew, xUnfold, name);
    }

    
    /**
     * Computes a - b.
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return a - b.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve sub(Curve a, Curve b) {
        // Build the new name
        String name = "";
        if (!a.name().equals("") && !b.name().equals("")) {
            name = "(" + a.name() + "-" + b.name() + ")";
        }
        Curve temp = (Curve)b.clone();
        temp.scaleY(-1);
        temp = add(a, temp);
        temp.setName(name);
        return temp;
    }

    /**
     * Computes min(a,b).
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return the minimum of a and b.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve min(Curve a, Curve b) {
        Curve resultCurve;

        // Build the new name
        String name = "";
        if (!a.name().equals("") && !b.name().equals("")) {
            name = "min(" + a.name() + "," + b.name() + ")";
        }

        // Rearrange a and b, such that a has the lower or equal lowerBound.
        if (!SegmentMath.leq(a.lowerBound(), b.lowerBound())) {
            Curve c = a;
            a = b;
            b = c;
        }

        // Compute the period of the resulting minimum Curve, and the point up 
        // to which the Curves must be unfolded.
        boolean crossPointExists = true;
        long periodNew = 0;
        double pdyNew = 0.0;
        double xUnfold = computeUpperCrossX(a, b);

        if (Double.isNaN(xUnfold)) {
            // Set xUnfold to the upper start of the bounds + 1 hyperperiod.
            crossPointExists = false;
            periodNew = MathUtil.computeHyperPeriod(a.period(), b.period());
            pdyNew = periodNew * a.lowerBound().s();
            xUnfold = Math.max(a.lowerBound().x(), b.upperBound().x()) + periodNew;
        } else if (a.hasPeriodicPart()) {
            // Set xUnfold to the start of a new period.
            xUnfold = (Math.ceil((xUnfold - a.px0()) / a.period()) * a.period()) + a.px0();  
        } 

        // If xUnfold equals 0.0, a is for sure always the minimum.
        // This can be the case, if a and b both consist only of either 
        // an aperiodic part with one single Segment, or of only a 
        // periodic part.
        if (DoubleMath.eq(xUnfold, 0.0)) {
            resultCurve = (Curve)a.clone();
            //resultCurve.simplify();
            return resultCurve;
        }
                
        // Compute the minimum of the unfolded Curves.
        SegmentList result = new SegmentList();     
        CurveSubSegmentIterator iter = new CurveSubSegmentIterator(a, b, xUnfold);
        boolean lastSubSegWasA = true;
        while (iter.next()) {   
            if (DoubleMath.lt(iter.yStartA(), iter.yStartB())) {
                if (DoubleMath.leq(iter.yEndA(), iter.yEndB())) {
                    // a is the minimum for this subsegment
                    if (iter.aIsNew() || !lastSubSegWasA) {
                        result.add(new Segment(iter.xStart(), iter.yStartA(), iter.sA()));
                    }
                    lastSubSegWasA = true;
                } else {
                    //a and b cross. First a is the minimum, then b.
                    if (iter.aIsNew() || !lastSubSegWasA) {
                        result.add(new Segment(iter.xStart(), iter.yStartA(), iter.sA()));
                    }
                    Point crossPoint = SegmentMath.crossPoint(iter.segmentA(), iter.segmentB());
                    result.add(new Segment(crossPoint.x(), crossPoint.y(), iter.sB()));
                    lastSubSegWasA = false;
                }
            } else if (DoubleMath.gt(iter.yStartA(), iter.yStartB())) { 
                if (DoubleMath.geq(iter.yEndA(), iter.yEndB())) {
                    // b is the minimum for this subsegment     
                    if (iter.bIsNew() || lastSubSegWasA) {
                        result.add(new Segment(iter.xStart(), iter.yStartB(), iter.sB()));
                    }
                    lastSubSegWasA = false;
                } else { //(subSegmentEndYA < subSegmentEndYB)
                    // a and b cross. First a is the minimum, then b.
                    if (iter.bIsNew() || lastSubSegWasA) {
                        result.add(new Segment(iter.xStart(), iter.yStartB(), iter.sB()));                    
                    }
                    Point crossPoint = SegmentMath.crossPoint(iter.segmentA(), iter.segmentB());
                    result.add(new Segment(crossPoint.x(), crossPoint.y(), iter.sA()));
                    lastSubSegWasA = true;
                }
            } else { //(subSegmentStartYA == subSegmentStartYB)
                if (DoubleMath.lt(iter.yEndA(), iter.yEndB())) {
                    // a is the minimum for this subsegment     
                    if (iter.aIsNew() || !lastSubSegWasA) {
                        result.add(new Segment(iter.xStart(), iter.yStartA(), iter.sA()));
                    }
                    lastSubSegWasA = true;
                } else if (DoubleMath.gt(iter.yEndA(), iter.yEndB())) {
                    // b is the minimum for this subsegment     
                    if (iter.bIsNew() || lastSubSegWasA) {
                        result.add(new Segment(iter.xStart(), iter.yStartB(), iter.sB()));    
                    }
                    lastSubSegWasA = false;
                } else { //(subSegmentEndYA == subSegmentEndYB)
                    // a and b are equal for this subsegment
                    if (lastSubSegWasA) {
                        if (iter.aIsNew()) {
                            result.add((Segment)iter.segmentA().clone());
                        }
                    } else {
                        if (iter.bIsNew()) {
                            result.add((Segment)iter.segmentB().clone());
                            lastSubSegWasA = true;
                        }                       
                    }
                }               
            }
        }
        // If xUpper was exactly the last cross point, we need to ensure that
        // the last Segment comes from a.
        if (DoubleMath.eq(iter.yEndA(), iter.yEndB())) {
            result.trimLT(iter.xEnd());
            result.add(new Segment(iter.xEnd(), iter.yEndA(), iter.sA())); 
        }
        
        // Build the minimum-curve, simplify and return it.
        if (!crossPointExists) {
            // If a and b do not cross within periodic part, we may use the 
            // period of a or b. 
            resultCurve = new Curve((SegmentList)result.clone(), periodNew, pdyNew, xUnfold);
            Curve resultCurveAltA = new Curve((SegmentList)result.clone(), 
                    a.period(), a.pdy(), xUnfold);
            Curve resultCurveAltB = new Curve((SegmentList)result.clone(), 
                    b.period(), b.pdy(), xUnfold);
            if (DoubleMath.leq(resultCurveAltA.px0(), resultCurve.px0()) 
                    && resultCurveAltA.period() < resultCurve.period()) {
                resultCurve = resultCurveAltA;
            }
            if (DoubleMath.leq(resultCurveAltB.px0(), resultCurve.px0())
                    && resultCurveAltB.period() < resultCurve.period()) {
                resultCurve = resultCurveAltB;
            }
        } else if (a.hasPeriodicPart()) {
            result.trimLT(xUnfold);
            double py0;
            if (DoubleMath.eq(xUnfold, a.px0())) {
                py0 = a.py0();
            } else {
                py0 = result.lastSegment().yAt(xUnfold);
                py0 -= a.periodicSegments().lastSegment().yAt(a.period());
                py0 += a.pdy();
            }
            resultCurve = new Curve(result, a.periodicSegments(), 
                    xUnfold, py0, a.period(), a.pdy());
            resultCurve.simplify();
        } else {
            resultCurve = new Curve(result);
            resultCurve.simplify();
        }     
        resultCurve.setName(name);
        return resultCurve;
    }
    
    /**
     * Computes max(a,b).
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return the maximum of a and b.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve max(Curve a, Curve b) {
        // Build the new name
        String name = "";
        if (!a.name().equals("") && !b.name().equals("")) {
            name = "max(" + a.name() + "," + b.name() + ")";
        }
        Curve tempA = (Curve)a.clone();
        Curve tempB = (Curve)b.clone();
        tempA.scaleY(-1);
        tempB.scaleY(-1);
        Curve tempC = min(tempA, tempB);
        tempC.scaleY(-1);
        tempC.setName(name);
        return tempC;
    }
    
    /**
     * Computes the maximum vertical distance between a and b. The result equals
     * the maximum y-value of the Curve sub(a, b). If the long-term slope of 
     * a is bigger than the one of b, the result is Double.POSITIVE_INFINITY
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return the maximum vertical distance between a and b.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static double maxVDist(Curve a, Curve b) {
        if (DoubleMath.gt(a.lowerBound().s(), b.upperBound().s())) {
            return Double.POSITIVE_INFINITY;
        }
        // At x=0, all Curves have y=0, therefore maxVDist >=0.
        double result = 0.0;
        
        // Compute the point up to which the Curves must be unfolded.
        double xUnfold = computeUpperCrossX(a, b);
        if (Double.isNaN(xUnfold)) {
            xUnfold = Math.max(a.lowerBound().x(), b.upperBound().x()) + 
            MathUtil.computeHyperPeriod(a.period(), b.period());
        } else {
            xUnfold = Math.min(Math.max(a.lowerBound().x(), b.upperBound().x()) + 
                    MathUtil.computeHyperPeriod(a.period(), b.period()), xUnfold);
        }
        
        // Compute the maxVDist of all subsegments.
        CurveSubSegmentIterator iter = new CurveSubSegmentIterator(a, b, xUnfold);            
        while (iter.next()) {
            result = Math.max(result, iter.yStartA() - iter.yStartB());
            result = Math.max(result, iter.yEndA() - iter.yEndB());
        }
        return result;
    }


    /**
     * Computes the maximum vertical distance between a and b. The result equals
     * the maximum y-value of the Curve sub(a, b). If the long-term slope of 
     * a is bigger than the one of b, the result is Double.POSITIVE_INFINITY
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return a Vector indicating the maximum vertical distance between a and b.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Vector maxVDistLine(Curve a, Curve b) {
        if (DoubleMath.gt(a.lowerBound().s(), b.upperBound().s())) {
            return new Vector(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY,
                    Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        }
        // At x=0, all Curves have y=0, therefore maxVDist >=0.
        double result = 0.0;
        double resultX = 0.0, resultY = 0.0;

        // Compute the point up to which the Curves must be unfolded.
        double xUnfold = computeUpperCrossX(a, b);
        if (Double.isNaN(xUnfold)) {
            xUnfold = Math.max(a.lowerBound().x(), b.upperBound().x()) + 
                    MathUtil.computeHyperPeriod(a.period(), b.period());
        } else {
            xUnfold = Math.min(Math.max(a.lowerBound().x(), b.upperBound().x()) + 
                    MathUtil.computeHyperPeriod(a.period(), b.period()), xUnfold);
        }
        
        // Compute the maxVDist of all subsegments.
        CurveSubSegmentIterator iter = new CurveSubSegmentIterator(a, b, xUnfold);            
        while (iter.next()) {
            if (iter.yStartA() - iter.yStartB() > result) {
                result = iter.yStartA() - iter.yStartB();
                resultX = iter.xStart();
                resultY = iter.yStartB();
            }
            if (iter.yEndA() - iter.yEndB() > result) {
                result = iter.yEndA() - iter.yEndB();
                resultX = iter.xEnd();
                resultY = iter.yEndB();
            }
            
        }
        return new Vector(resultX, resultY, resultX, resultY + result);
    }
    
    /**
     * Computes the maximum horizontal distance between a and b. The result 
     * equals the maximum y-value of the Curve sub(inv(b), inv(a)). I.e. the 
     * results equals the maximum distance when starting at Curve a, and going
     * horizontally to the right until hitting Curve b. If the long-term slope 
     * of a is bigger than the one of b, the result is Double.POSITIVE_INFINITY.
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>a and b must be wide-sense increasing</li>
     * </ul>
     * 
     * @param a - the first curve.
     * @param b - the second curve.
     * @return the maximum horizontal distance between a and b.
     * @exception IllegalArgumentException - if the parameters do not fulfill
     * the specified conditions.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */    
    public static double maxHDist(Curve a, Curve b) {
        if (!CurveChecker.isWideSenseIncreasing(a) ||
                !CurveChecker.isWideSenseIncreasing(b)) {
            throw new IllegalArgumentException(Messages.NON_WSI_CURVE);
        }
        if (DoubleMath.gt(a.lowerBound().s(), b.upperBound().s())) {
            return Double.POSITIVE_INFINITY;
        }
        if (DoubleMath.eq(a.lowerBound().s(), b.upperBound().s()) &&
                DoubleMath.eq(a.lowerBound().s(), 0.0) &&
                DoubleMath.gt(a.upperBound().y(), b.upperBound().y())) {
            return Double.POSITIVE_INFINITY;
        }
        
        // At y=0, all Curves have x=0, therefore maxHDist >=0.
        double result = 0.0;
        
        // Compute the point up to which the Curves must be unfolded.
        double yUnfold = computeUpperCrossY(a, b);
        double vHyperPeriod = MathUtil.computeVHyperPeriod(a.period(), a.pdy(), 
                b.period(), b.pdy());
        if (Double.isNaN(yUnfold)) {
            yUnfold = Math.max(a.upperBound().y(), b.upperBound().y()) + 
                    vHyperPeriod;
        } else {
            yUnfold = Math.min(Math.max(a.upperBound().y(), b.upperBound().y()) + 
                    vHyperPeriod, yUnfold);
        }
        yUnfold = yUnfold + Parameters.SAFETY_MARGIN;

        // Compute the maxHDist of all subsegments.
        try {
            CurveSubSegmentIteratorInv iter = new CurveSubSegmentIteratorInv(a, b, yUnfold);
            double xEndBLast = 0.0;
            while (iter.next()) {
                if (DoubleMath.lt(iter.yStart(), iter.yEnd())) {
                    result = Math.max(result, iter.xStartB() - iter.xStartA());
                    result = Math.max(result, iter.xEndB() - iter.xEndA());
                } else { // iter.yStart() == iter.yEnd()
                    result = Math.max(result, xEndBLast - iter.xStartA());
                }
                xEndBLast = iter.xEndB();
            }
            return result;
        } catch (IllegalOperationException e) {
            // this should never happen because the input curves are tested to 
            // be wide-sense increasing.
            System.out.println(Messages.MAXHDIST_UNDEFINED);
            System.out.println(a);
            System.out.println(b);
            return 0.0;
        }
    }

    /**
     * Computes the maximum horizontal distance between a and b. The result 
     * equals the maximum y-value of the Curve sub(inv(b), inv(a)). I.e. the 
     * results equals the maximum distance when starting at Curve a, and going
     * horizontally to the right until hitting Curve b. If the long-term slope 
     * of a is bigger than the one of b, the result is Double.POSITIVE_INFINITY.
     * <p>
     * The following conditions must be true for the passed parameters:
     * <ul>
     * <li>a and b must be wide-sense increasing</li>
     * </ul>
     * 
     * @param a - the first curve.
     * @param b - the second curve.
     * @return the maximum horizontal distance between a and b as a vector.
     * @exception IllegalArgumentException - if the parameters do not fulfill
     * the specified conditions.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */       
    public static Vector maxHDistLine(Curve a, Curve b) {
        if (!CurveChecker.isWideSenseIncreasing(a) ||
                !CurveChecker.isWideSenseIncreasing(b)) {
            throw new IllegalArgumentException(Messages.NON_WSI_CURVE);
        }
        if (DoubleMath.gt(a.lowerBound().s(), b.upperBound().s())) {
            return new Vector(0, 0, Double.POSITIVE_INFINITY, 0);
        }
        if (DoubleMath.eq(a.lowerBound().s(), b.upperBound().s()) &&
                DoubleMath.eq(a.lowerBound().s(), 0.0) &&
                DoubleMath.gt(a.upperBound().y(), b.upperBound().y())) {
            return new Vector(0, 0, Double.POSITIVE_INFINITY, 0);
        }
        
        // At y=0, all Curves have x=0, therefore maxHDist >=0.
        double result = 0.0;
        double resultX = 0.0, resultY = 0.0;
        
        // Compute the point up to which the Curves must be unfolded.
        double yUnfold = computeUpperCrossY(a, b);
        double vHyperPeriod = MathUtil.computeVHyperPeriod(a.period(), a.pdy(), 
                b.period(), b.pdy());
        if (Double.isNaN(yUnfold)) {
            yUnfold = Math.max(a.upperBound().y(), b.upperBound().y()) + 
            vHyperPeriod;
        } else {
            yUnfold = Math.min(Math.max(a.upperBound().y(), b.upperBound().y()) + 
                    vHyperPeriod, yUnfold);
        }
        yUnfold = yUnfold + Parameters.SAFETY_MARGIN;
        
        // Compute the maxHDist of all subsegments.
        try {
            CurveSubSegmentIteratorInv iter = new CurveSubSegmentIteratorInv(a, b, yUnfold);   
            double xEndBLast = 0.0;
            while (iter.next()) {
                if (DoubleMath.lt(iter.yStart(), iter.yEnd())) {
                    if (iter.xStartB() - iter.xStartA() > result) {
                        result = iter.xStartB() - iter.xStartA();
                        resultX = iter.xStartA();
                        resultY = iter.yStart();
                    }
                    if (iter.xEndB() - iter.xEndA() > result) {
                        result = iter.xEndB() - iter.xEndA();
                        resultX = iter.xEndA();
                        resultY = iter.yEnd();
                    }
                } else {
                    if (xEndBLast - iter.xEndA() > result) {
                        result = xEndBLast - iter.xEndA();
                        resultX = iter.xEndA();
                        resultY = iter.yEnd();
                    }
                }
                xEndBLast = iter.xEndB();
            }
            return new Vector(resultX, resultY, resultX + result, resultY); 
        } catch (IllegalOperationException e) {
            // this should never happen because the input curves are tested to 
            // be wide-sense increasing.
            System.out.println(Messages.MAXHDIST_UNDEFINED);
            System.out.println(a);
            System.out.println(b);
            return new Vector(0, 0, 0, 0);
        }
    }   

    /**
     * Computes minPlusConv(a, b).
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return minPlusConv(a, b)
     * @ProposedRating Red (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve minPlusConv(Curve a, Curve b) {
        // Build the new name
        String name = "";
        if (!a.name().equals("") && !b.name().equals("")) {
            name = "minPlusConv(" + a.name() + "," + b.name() + ")";
        }
        /* NS:
         * If one of the curves has only one Aperiodic segment
         * with a y-coordinate at Infinity, return the other curve.
         * min(Inf, b) = b (valid assumption ?)
         */
        if(a.hasAperiodicPart()) {
        	if(a.aperiodicSegments().size() == 1) {
        	    if(a.aperiodicSegments().lastSegment().y() == Double.POSITIVE_INFINITY) {
        		    return (Curve)b.clone();
        	    }
        	}
        }
        if(b.hasAperiodicPart()) {
        	if(b.aperiodicSegments().size() == 1) {
        	    if(b.aperiodicSegments().lastSegment().y() == Double.POSITIVE_INFINITY) {
        		    return (Curve)a.clone();
        	    }
        	}
        }
        Curve aNeg = (Curve)a.clone();
        aNeg.scaleY(-1);
        Curve bNeg = (Curve)b.clone();
        bNeg.scaleY(-1);
        Curve result = maxPlusConv(aNeg, bNeg);
        result.scaleY(-1);
        result.setName(name);
        return result;
    }
    
    /**
     * Computes minPlusDeconv(a, b).
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return minPlusDeconv(a, b)
     * @ProposedRating Red (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */    
    public static Curve minPlusDeconv(Curve a, Curve b) {
        Curve resultCurve;
        
        // Build the new name
        String name = "";
        if (!a.name().equals("") && !b.name().equals("")) {
            name = "minPlusDeconv(" + a.name() + "," + b.name() + ")";
        }
        
        if (DoubleMath.gt(a.lowerBound().s(), b.lowerBound().s())) {
            SegmentList part = new SegmentList();
            part.add(new Segment(0.0, Double.POSITIVE_INFINITY, 0.0));
            resultCurve = new Curve(part);
            resultCurve.setName(name);
            return resultCurve;
        }
        
        // Compute the period of the resulting Curve, and the point up 
        // to which the Curves must be unfolded.
        // new period   : a
        // upper unfold : hyperperiod + periodicstartA + periodicstartB
        long periodNew = a.period();
        long hyperPeriod = MathUtil.computeHyperPeriod(a.period(), b.period());
        double pdyNew = a.pdy();
        double xUnfold = a.upperBound().x() + b.upperBound().x() + hyperPeriod;
        double xCross = computeUpperCrossXWithCommonY0(a, b);
        if (!Double.isNaN(xCross)) {
            xUnfold = Math.max(xUnfold, xCross + periodNew);
        }       
        
        // TODO: tighten this bound!!!!
        X_UPPER_INIT = xUnfold;
        xUnfold = xUnfold + Parameters.SAFETY_MARGIN;
        double upperTrimX = 2 * xUnfold;
        xUnfold *= 3;
        X_UPPER_FIN = xUnfold;
        // Do the math...
        SegmentList resultSegments = minPlusDeconvHelp(a, b, xUnfold);
        SegmentList result = maxOfLimitedCurveSegments(resultSegments);
        
        // Build the resulting Curve, simplify and return it.
        if (DoubleMath.gt(xUnfold, 0.0)) {
            result.trimLT(upperTrimX);
        }
        resultCurve = new Curve(result, periodNew, pdyNew, upperTrimX);
        resultCurve = CurveMath.max(a, resultCurve);
        resultCurve.setName(name);
        return resultCurve;
    }
    
    /**
     * Computes maxPlusConv(a, b).
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return maxPlusConv(a, b)
     * @ProposedRating Red (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
        public static Curve maxPlusConv(Curve a, Curve b) {
            Curve resultCurve;
            
            // Build the new name
            String name = "";
            if (!a.name().equals("") && !b.name().equals("")) {
                name = "maxPlusConv(" + a.name() + "," + b.name() + ")";
            }
            
            // Rearrange a and b, such that a has the lower or equal lowerBound.
            if (!SegmentMath.leq(a.lowerBound(), b.lowerBound())) {
                Curve c = a;
                a = b;
                b = c;
            }
            
            // Compute the period of the resulting Curve, and the point up 
            // to which the Curves must be unfolded.
            long periodNew = 0;
            long hyperPeriod = MathUtil.computeHyperPeriod(a.period(), b.period());
            if (DoubleMath.eq(a.upperBound().s(), b.upperBound().s())) {
                periodNew = hyperPeriod;
            } else {
                periodNew = b.period();
            }
            double pdyNew = periodNew * b.upperBound().s();
            double xUnfold = a.upperBound().x() + b.upperBound().x() + 
                    hyperPeriod ;
            double xCross = computeUpperCrossX(a, b);
    //      double xCross = computeUpperCrossXWithCommonStartY(a, b);
            if (!Double.isNaN(xCross)) {
                xUnfold = Math.max(xUnfold, xCross + periodNew);
            }
            xUnfold = xUnfold + periodNew + Parameters.SAFETY_MARGIN;
            
            X_UPPER_INIT = xUnfold;
            // PERFORMANCE: tighten this bound!!!!
            //xUnfold = xUnfold + Parameters.SAFETY_MARGIN;
            //NS: Uncommented following line
            xUnfold *= 4;
            X_UPPER_FIN = xUnfold;
            
            // Do the math...
            SegmentList resultSegments = maxPlusConvHelp(a, b, xUnfold);
            SegmentList result = maxOfLimitedCurveSegments(resultSegments);
            
            // Build the resulting Curve, simplify and return it.
            if (DoubleMath.gt(xUnfold, 0.0)) {
                result.trimLT(xUnfold);
            }
            resultCurve = new Curve(result, periodNew, pdyNew, 
                    xUnfold);    
            resultCurve.setName(name);
            return resultCurve;
        }


    /**
     * Computes maxPlusConv(c, 0), but more efficiently.
     * maxPlusConv0(c)(Delta) = sup_{0<=delta<=Delta} {c(delta)}
     * I.e. the result must be positive and strictly increasing.
     * 
     * @param c the Curve.
     * @return maxPlusConv(c, 0)
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve maxPlusConv0(Curve c) {
        Curve resultCurve;
        SegmentList result;
        double xUnfold;      
    
        // Build the new name
        String name = "";
        if (!c.name().equals("")) {
            name = "maxPlusConv0(" + c.name() + ")";
        }
    
        if (!c.hasPeriodicPart()) {
            // The resulting part has only an aperiodic part.
            resultCurve = new Curve(maxPlusConv0Help(c.aperiodicSegments(), 0.0));
            resultCurve.simplify();
        } else if (DoubleMath.leq(c.pdy(), 0.0)) {
            // The periodic part is decreasing, the result is aperiodic.
            xUnfold = c.px0() + c.pdx();
            result =  maxPlusConv0Help(c.segmentsLT(xUnfold), 0.0);
            result.trimLT(xUnfold);
            if (DoubleMath.neq(result.lastSegment().s(), 0.0)) {
                result.add(new Segment(xUnfold, result.lastSegment().yAt(xUnfold), 0.0));
            }
            resultCurve = new Curve(result);
            resultCurve.simplify();
        } else {
            // The result has possibly an aperiodic and always a periodic part.
            double yMaxAper;
            if (c.hasAperiodicPart()) {
                result = maxPlusConv0Help(c.aperiodicSegments(), 0.0);
                result.trimLT(c.px0());
                yMaxAper = result.lastSegment().yAt(c.px0());
            } else {
                result = new SegmentList();
                yMaxAper = 0.0;
            }
            int shadowedPeriods = (int)Math.max(0, Math.floor(
                    ((yMaxAper - c.py0() - c.pyMax()) / c.pdy()) - Parameters.PRECISION)
                    + 1);
            if (!c.hasAperiodicPart() && shadowedPeriods > 0) {
                result.add(new Segment(0.0, 0.0, 0.0));
            }
            SegmentList unfoldedPeriods = 
                    (SegmentList)c.periodicSegments().clone();
            unfoldedPeriods.concat((SegmentList)unfoldedPeriods.clone(), c.pdx(), c.pdy());
            SegmentList unfoldedPeriodicResult = maxPlusConv0Help(unfoldedPeriods, 
                    yMaxAper - c.py0() - (shadowedPeriods * c.pdy()));      
            unfoldedPeriodicResult.trimLT(2 * c.pdx());
            result.concat(unfoldedPeriodicResult, 
                    c.px0() + (shadowedPeriods * c.pdx()), 
                    c.py0() + (shadowedPeriods * c.pdy()));
            resultCurve = new Curve(result, c.period(), c.pdy(), 
                    c.px0() + (shadowedPeriods + 2) * c.pdx());
        }
        resultCurve.setName(name);
        return resultCurve;
    }


    /**
     * Computes maxPlusDeconv(a, b).
     * 
     * @param a the first Curve.
     * @param b the second Curve.
     * @return maxPlusDeconv(a, b)
     * @ProposedRating Red (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */  
    public static Curve maxPlusDeconv(Curve a, Curve b) {
        // Build the new name
        String name = "";
        if (!a.name().equals("") && !b.name().equals("")) {
            name = "maxPlusDeconv(" + a.name() + "," + b.name() + ")";
        }
        Curve aNeg = (Curve)a.clone();
        aNeg.scaleY(-1);
        Curve bNeg = (Curve)b.clone();
        bNeg.scaleY(-1);
        Curve result = minPlusDeconv(aNeg, bNeg);
        result.scaleY(-1);
        result.setName(name);
        return result;
    }
    
    /**
     * Computes maxPlusDeconv(c, 0), but more efficiently.
     * maxPlusDeconv0(c)(Delta) = inf_{0<=delta} {c(Delta + delta)}
     * maxPlusDeconv0(c)(Delta) = inf_{Delta<=delta} {c(delta)}
     * 
     * @param c the Curve.
     * @return maxPlusDeconv(c, 0)
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve maxPlusDeconv0(Curve c) {
        Curve resultCurve;
        SegmentList result;
        
        // Build the new name
        String name = "";
        if (!c.name().equals("")) {
            name = "maxPlusDeconv0(" + c.name() + ")";
        }
        
        if (DoubleMath.lt(c.upperBound().s(), 0.0)) {
            // The result is zero.
            result = new SegmentList(1);
            result.add(new Segment(0.0, 0.0, 0.0));
            resultCurve = new Curve(result);
        } else if (!c.hasPeriodicPart()) {
            // The result has only an aperiodic part.
            resultCurve = new Curve(maxPlusDeconv0Help(c.aperiodicSegments(), 0.0));
            resultCurve.simplify();  
        } else {
            // The result has possibly an aperiodic and a periodic part.
            double yMinPer = c.py0() + c.pyMin();
            if (c.hasAperiodicPart()) {
                if (DoubleMath.gt(yMinPer, 0.0)) {
                    result = maxPlusDeconv0Help(c.aperiodicSegments(), 0.0, 
                            c.px0(), yMinPer);
                } else {
                    result = new SegmentList();
                    result.add(new Segment(0.0, 0.0, 0.0));
                }
            } else {
                result = new SegmentList();
            }
            if (DoubleMath.gt(c.pdy(), 0.0)) {
                int shadowedPeriods = (int)Math.max(0, Math.floor(
                        (0.0 - c.py0() - c.pyMin()) / c.pdy()));
                if (!c.hasAperiodicPart() && shadowedPeriods > 0) {
                    result.add(new Segment(0.0, 0.0, 0.0));
                }
                SegmentList unfoldedPeriods = (SegmentList)c.periodicSegments().clone();
                unfoldedPeriods.concat((SegmentList)unfoldedPeriods.clone(),
                        c.pdx(), c.pdy());
                SegmentList unfoldedPeriodicResult = maxPlusDeconv0Help(unfoldedPeriods, 
                        0.0 - c.py0() - (shadowedPeriods * c.pdy()),
                        2 * c.pdx(), (2 * c.pdy()) + c.pyMin());           
                result.concat(unfoldedPeriodicResult, 
                        c.px0() + (shadowedPeriods * c.pdx()), 
                        c.py0() + (shadowedPeriods * c.pdy()));
                resultCurve = new Curve(result, c.period(), c.pdy(), 
                        c.px0() + (shadowedPeriods + 2) * c.pdx());
            } else {
                if (c.hasAperiodicPart() && DoubleMath.gt(yMinPer, 0.0)) {
                    result.add(new Segment(c.px0(), yMinPer, 0.0));
                } else {
                    if (DoubleMath.gt(yMinPer, 0.0)) {
                        result.add(new Segment(c.px0(), yMinPer, 0.0));
                    } else {
                        result.add(new Segment(c.px0(), 0.0, 0.0));
                    }
                }
                resultCurve = new Curve(result);
                resultCurve.simplify();  
            }
        }
        resultCurve.setName(name);
        return resultCurve;
    }

    /**
     * Computes the ceil of a curve. The ceil of a curve is a piecewise constant
     * curve that is built by taking the ceil of the curve at every point on the
     * positive x-axis.
     * 
     * @param c the curve to compute the ceil of.
     * @return a curve that correspond to the ceil of c.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve ceil(Curve c) {
        Curve resultCurve;
        SegmentList result;

        // Build the new name
        String name = "";
        if (!c.name().equals("")) {
            name = "ceil(" + c.name() + ")";
        }
        
        double xUnfold;
        double pdx;
        double pdy;
        if (c.hasPeriodicPart()) {
            pdx = c.pdx();
            pdy = c.pdy();
            xUnfold = c.px0();
        } else {
            if (DoubleMath.neq(c.upperBound().s(), 0.0)) {
                pdx = 1. / Math.abs(c.upperBound().s());
                if (DoubleMath.gt(c.upperBound().s(), 0.0)) {
                    pdy = 1.;
                } else {
                    pdy = -1.;
                }
            } else {
                pdx = 0.;
                pdy = 0.;
            }
            xUnfold = c.upperBound().x();
        }
        // FIXME catch too big periods...
        Fraction f;
        if (c.hasPeriodicPart()) {
            f = new Fraction(pdy, Parameters.PRECISION);
        } else {
            f = new Fraction(pdx, Parameters.PRECISION);
        }
        pdx = pdx * f.denominator();
        pdy = pdy * f.denominator();      
        xUnfold = xUnfold + pdx;

        result = ceilHelp(c, xUnfold);
        
        if (DoubleMath.neq(pdx, 0.0)) {
            resultCurve = new Curve(result, Math.round(pdx), 
                    (double)Math.round(pdy), xUnfold);
        } else {
            resultCurve = new Curve(result);
        }       
        resultCurve.setName(name);
        return resultCurve;
    }
    
    /**
     * Computes the floor of a curve. The floor of a curve is a piecewise 
     * constant curve that is built by taking the floor of the curve at every 
     * point on the positive x-axis.
     * 
     * @param c the curve to compute the floor of.
     * @return a curve that correspond to the floor of c.
     * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
     */
    public static Curve floor(Curve c) {
        Curve resultCurve;
        SegmentList result;

        // Build the new name
        String name = "";
        if (!c.name().equals("")) {
            name = "floor(" + c.name() + ")";
        }
        
        double xUnfold;
        double pdx;
        double pdy;
        if (c.hasPeriodicPart()) {
            pdx = c.pdx();
            pdy = c.pdy();
            xUnfold = c.px0();
        } else {
            if (DoubleMath.neq(c.upperBound().s(), 0.0)) {
                pdx = 1. / Math.abs(c.upperBound().s());
                if (DoubleMath.gt(c.upperBound().s(), 0.0)) {
                    pdy = 1.;
                } else {
                    pdy = -1.;
                }
            } else {
                pdx = 0.;
                pdy = 0.;
            }
            xUnfold = c.upperBound().x();
        }
        // FIXME catch too big periods...
        Fraction f;
        if (c.hasPeriodicPart()) {
            f = new Fraction(pdy, Parameters.PRECISION);
        } else {
            f = new Fraction(pdx, Parameters.PRECISION);
        }
        pdx = pdx * f.denominator();
        pdy = pdy * f.denominator();      
        xUnfold = xUnfold + pdx;

        result = floorHelp(c, xUnfold);
        
        if (DoubleMath.neq(pdx, 0.0)) {
            resultCurve = new Curve(result, Math.round(pdx), 
                    (double)Math.round(pdy), xUnfold);
        } else {
            resultCurve = new Curve(result);
        }
        resultCurve.setName(name);
        return resultCurve;
    }
    
    
    
    /**
     * Computes RT^{-\alpha}(\beta',\beta)
     * 
     * @param bA \beta^{'A}.
     * @param bG \beta^G.
     * @return RT^{-\alpha}(\beta',\beta).
     * @ProposedRating Red (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Red (wandeler@tik.ee.thz.ch)
     */
    public static Curve RTinvAlpha(Curve bA, Curve bG) {
        // Build the new name
        String name = "";
        if (!bA.name().equals("") && !bG.name().equals("")) {
            name = "RTinvAlpha(" + bA.name() + "," + bG.name() + ")";
        }
        
        // Compute the period of the resulting Curve, and the point up 
        // to which the Curves must be unfolded.
        long periodNew = MathUtil.computeHyperPeriod(bA.period(), bG.period());
        double pdyNew = periodNew * (bG.lowerBound().s() - bA.lowerBound().s());
        double xUnfold = Math.max(bA.lowerBound().x(), bG.lowerBound().x()) + periodNew;
    
        // Compute the result.
        SegmentList result = new SegmentList();
        CurveSubSegmentIterator iter = new CurveSubSegmentIterator(bA, bG, xUnfold);
        
        double xTemp = 0, yTemp = 0;
        boolean tempValid = false;
        while (iter.next()) {
            if (tempValid && (DoubleMath.neq(iter.yStartA(), yTemp) 
                    || DoubleMath.neq(iter.sA(), 0.0))) { 
                result.add(new Segment(xTemp, 
                        - iter.yStartA() + iter.yStartB(),
                        0));                
            }           
            if (DoubleMath.eq(iter.sA(), 0.0)) {;
                if (!tempValid || (DoubleMath.neq(iter.yStartA(), yTemp))) {
                    xTemp = iter.xStart();
                    yTemp = iter.yStartA();
                    tempValid = true;
                } 
            } else {
                result.add(new Segment(iter.xStart(), 
                        - iter.yStartA() + iter.yStartB(),
                        - iter.sA() + iter.sB()));
                tempValid = false;
            }
        }   
        return new Curve(result, periodNew, pdyNew, xUnfold, name);
    }    
    
    /**
     * Computes RT^{-\beta}(\beta',\alpha)
     * 
     * @param bA \beta^{'A}.
     * @param aG \alpha^G.
     * @return RT^{-\alpha}(\beta',\beta).
     * @ProposedRating Red (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Red (wandeler@tik.ee.thz.ch)
     */
    //@SuppressWarnings("unchecked")
	public static Curve RTinvBeta(Curve bA, Curve aG) {
        // Build the new name
        String name = "";
        if (!bA.name().equals("") && !aG.name().equals("")) {
            name = "RTinvBeta(" + bA.name() + "," + aG.name() + ")";
        }
        
        // Compute the period of the resulting Curve, and the point up 
        // to which the Curves must be unfolded.
        long periodNew = MathUtil.computeHyperPeriod(bA.period(), aG.period());
//        System.err.println("period of aG is: " + aG.period());
//        System.err.println("period of bA is: " + bA.period());
//        System.err.println("New period is: " + periodNew);
        
        double pdyNew = periodNew * (aG.lowerBound().s() + bA.lowerBound().s());
//        System.err.println("slope of lowerBound of aG is: " + aG.lowerBound().s());
//        System.err.println("slope of lowerBound of bA is: " + bA.lowerBound().s());
//        System.err.println("Y coord of unfolding point: " + pdyNew);
        
        double xUnfold = Math.max(bA.lowerBound().x(), aG.lowerBound().x()) + periodNew;
//        System.err.println("x coord of starting point of lowerBound of aG is: " + aG.lowerBound().x());
//        System.err.println("x coord of starting point of lowerBound of bA is: " + bA.lowerBound().x());
//        System.err.println("X coord of unfolding point: " + xUnfold);

        
        // Compute the addition of the unfolded Curves.
        SegmentList result = new SegmentList();
        
        CurveSubSegmentIterator iter = new CurveSubSegmentIterator(bA, aG, xUnfold);
        
        // Compute the result.
        double yTemp = 0;
        boolean tempValid = false;
        double lastaG = 0;
        
        while (iter.next()) {
//        	System.err.println();
//        	System.err.println("New sub segment");
//        	System.err.println("xStart(): " + iter.xStart());
//        	System.err.println("xEnd(): " + iter.xEnd());
//        	System.err.println("yStartA(): " + iter.yStartA());
//        	System.err.println("yEndA(): " + iter.yEndA());
//        	System.err.println("sA(): " + iter.sA());
//        	System.err.println("yStartB(): " + iter.yStartB());
//        	System.err.println("yEndB(): " + iter.yEndB());
//        	System.err.println("sB(): " + iter.sB());
//        	System.err.println("aIsNew(): " + iter.aIsNew());
//        	System.err.println("bIsNew(): " + iter.bIsNew());
//        	System.err.println();
        	
            if (DoubleMath.eq(iter.sA(), 0.0)) {
//            	System.err.println("Slope of subsegment in A is 0");
//            	System.err.println("tampValid = " + tempValid);
//            	System.err.println("yTemp = " + yTemp);
            	
                if (!tempValid || (DoubleMath.neq(iter.yStartA(), yTemp))) {
                		
                    if (DoubleMath.eq(iter.xStart(), 0.0) && DoubleMath.eq(iter.yStartA(), 0.0)) {
//                    	System.err.println("Added (0,0,0) segment");
                        result.add(new Segment(0.0, 0.0, 0.0));
                    } else {
//                    	System.err.println("Added segment = (" + iter.xStart() + ", " + iter.yStartA() + " + " + lastaG + ", 0)");
                        result.add(new Segment(iter.xStart(), iter.yStartA() + lastaG, 0)); 
                    }
                    
                    yTemp = iter.yStartA();
//                    System.err.println("yTemp = iter.yStartA() = " + yTemp);
//                    System.err.println("tempValid = true");
                    tempValid = true;
                } 
                
            } else {
//            	System.err.println("Slope of curve A is not 0");
//            	System.err.println("Added segment = (" + iter.xStart() + ", " + iter.yStartA() + " + " + lastaG + ", " + iter.sA() + " + " + iter.sB() + ")");
                // substitue iter.uStartA() + lastaG with
            	// iter.yStartA() + iter.yStartB()
                result.add(new Segment(iter.xStart(), iter.yStartA() + iter.yStartB(), iter.sA() + iter.sB()));
                
//                System.err.println("tempValid = false");
                tempValid = false;
            }
            
            lastaG = iter.yEndB();
//            System.err.println("lastaG = iter.yEndB() = " + lastaG);
        }
        
        return new Curve(result, periodNew, pdyNew, xUnfold, name);
    }

	/**
     * Computes the minimum slot length to schedule an application on a 
     * TDMA resource.
     * 
     * FIXME: This method should maybe be moved to another class.
     * 
     * @param betaA \beta^{'A}.
     * @param cycle cycle.
     * @param bandwidth bandwidth.
     * @return sA.
     * @ProposedRating Red (wandeler@tik.ee.ethz.ch)
     * @AcceptedRating none
     * @JUnitRating    Red (wandeler@tik.ee.thz.ch)
     */
    public static double TDMAsA(Curve betaA, long cycle, double bandwidth) {
        Segment betaBound = betaA.tightUpperBound();
        Segment tdmaBound = new Segment(cycle, 0, 0);
        if (DoubleMath.gt(betaBound.s(), bandwidth)) return Double.POSITIVE_INFINITY;
        double s = 0, sTemp;
        double delta = 0, beta, iFloor, iCeil, boundCross = 0;
        CurveSegmentIterator iter = new CurveSegmentIterator(betaA);

        while (iter.next() && !((DoubleMath.geq(tdmaBound.s(), betaBound.s()))
                    && (DoubleMath.geq(delta, boundCross))
                    && (DoubleMath.geq(delta, betaBound.x())))) {
            delta = iter.xStart();
            beta = iter.yStart();
            iFloor = Math.floor(delta/(double)cycle);
            iCeil = Math.ceil(delta/(double)cycle);
            if (iFloor == 0) {
                if (iCeil == 0) {
                    sTemp = 0;
                } else {
                    sTemp = ((beta - (bandwidth * delta) + (bandwidth * iCeil * (double)cycle))
                            / (bandwidth * iCeil));
                }
            } else {
                sTemp = Math.min((beta / (bandwidth * iFloor)), 
                        ((beta - (bandwidth * delta) + (bandwidth * iCeil * (double)cycle))
                                / (bandwidth * iCeil)));
            }
            if (DoubleMath.gt(sTemp, s)) {
                s = sTemp;
                tdmaBound = new Segment(cycle, 0, (s * bandwidth) / (double)cycle);
                boundCross = SegmentMath.crossPoint(betaBound, tdmaBound).x();
            }
        }
        if (!iter.next()) {
            sTemp = (betaBound.s() * (double)cycle) / bandwidth;
            s = Math.max(s, sTemp);
        }
        if (DoubleMath.gt(s, (double)cycle)) return Double.POSITIVE_INFINITY;
        return s;
    }
    
	 /**
	 * Compute the pseude-inverse of a.
	 * @param a the curve.
	 * @return the inverse of a
	 * @since 2008-09-17
	 * @author reint
	 */
	public static Curve invert(Curve a) {
		Curve inverse = null;
		SegmentList aperSeg = null, perSeg = null;
		double px0 = Double.NaN, py0 = Double.NaN, pdy = Double.NaN;
		long period = 0;
		boolean hasAperiodicPart = (a.hasAperiodicPart() && a.aperiodicSegments().size() > 0);
		boolean hasPeriodicPart = (a.hasPeriodicPart() && a.periodicSegments().size() > 0);
		
	    if (hasAperiodicPart) {
	    	aperSeg = (SegmentList)a.aperiodicSegments().clone();
	    	aperSeg.invert();
	    }
	    if (hasPeriodicPart) {
	    	px0 = a.py0();
	    	py0 = a.px0();
	    	pdy = a.period();	 
	    	period = Math.round(a.pdy()); 
	    	if (DoubleMath.neq(a.pdy(), period)){
	    		// pdy not integer
				throw new IllegalOperationException(Messages.PERIOD_NOT_INVERTABLE);
	    	}
	    	if (DoubleMath.gt(a.pyMax(), a.pdy())){
	    		// discontinuity at the edge of the period
	    		throw new IllegalOperationException(Messages.ILLEGAL_ARGUMENT_ORDER_PER);
	    	}
	    		
	    	perSeg = (SegmentList)a.periodicSegments().clone();
	    	perSeg.invert();
	    	
	    	if (hasAperiodicPart){
	    		// modify the last segmennt of the aperiodic part if its
		    	// slope is infinity.
		    	Segment aperlastSeg = aperSeg.lastSegment();
		    	if (aperlastSeg.s() == Double.POSITIVE_INFINITY) {
		    		aperlastSeg.setY(py0);
		    		aperlastSeg.setS(0);
		    	}
		    	else if(DoubleMath.gt(aperlastSeg.s(), 0)) {
		    		// insert a segment if there is a discontinuity between the aperiodic
		    		// and the periodic part
		    		double yMax = aperlastSeg.yAt(px0);
		    		if (DoubleMath.lt(py0, yMax)){
		    			double xAtPyO = aperlastSeg.xAt(py0);
		    			aperSeg.add(new Segment(xAtPyO,py0,0));
		    		}
		    	}
	    	}
	    	else {
    			// insert an aperiodic Segment if periodic part doesn't 
	    		// start at px0 = 0
	    		if (DoubleMath.gt(px0, 0)){
	    			aperSeg = new SegmentList(1);
		    		aperSeg.add(new Segment(0,0,0));
		    		hasAperiodicPart=true;
	    		}
	    	}
	    	
	    	// modify the last segmennt of the periodic part if its
	    	// slope is infinity.
	    	Segment perlastSeg = perSeg.lastSegment();
	    	if (perlastSeg.s() == Double.POSITIVE_INFINITY) {
	    		perlastSeg.setY(pdy);
	    		perlastSeg.setS(0);
	    	}
	    	else if(DoubleMath.gt(perlastSeg.s(), 0)) {
	    		// insert a segment if there is a discontinuity a the edge of the period
	    		if (DoubleMath.lt(a.pyMax(),a.pdy())){
	    			perSeg.add(new Segment(a.pyMax(),a.period(),0));
	    		}
	    	}
	    }
	    
	    if (hasPeriodicPart) {
	    	inverse = new Curve(aperSeg, perSeg, px0, py0, period, pdy);
	    }
	    else if (hasAperiodicPart) {
	    	inverse = new Curve(aperSeg);
	    }
	    else {
	    	return null;
	    }
	    if (!a.name().equals("")) {
	    	inverse.setName("Inverse[("+a.name()+")]");
        }
	    inverse.simplify();
	    return inverse;
	}
	
	 /**
	 * Compute the concatenation f(g) of two UPPER curves.
	 * @param f g - the two curves.
	 * @return the concatenation f(g)
	 * @since 2008-09-17
	 * @author reint
	 */
	public static Curve concatUpper(Curve f, Curve g){
		if (!CurveChecker.isWideSenseIncreasingAllowInfinity(f) ||
				!CurveChecker.isWideSenseIncreasingAllowInfinity(g)){
			throw new IllegalOperationException(Messages.SEGMENT_LIST_NOT_INVERTABLE);
		}
		
		double[] param = concatGetParameters(f,g);
		// param[0] = pdxNew, param[1] = pdyNew, param[2] = xMax
		long pdxNew = (long)param[0];
		double pdyNew = param[1], xMax = param[2];
		
		f = insertZeroSegment(f);
		
		String name = "CONCAT(u)[("+f.name()+"),("+g.name()+")]";
		
		SegmentList segList = new SegmentList();
		CurveSegmentIterator f_iter = new CurveSegmentIterator(f);
		CurveSegmentIterator g_iter = new CurveSegmentIterator(g, xMax);
		
		double xStart = 0, xEnd = 0, yStart = 0, yEnd = 0;
		f_iter.next();
		
		while(g_iter.next()) {
			// check if g_iter.yStart() == Double.POSITIVE_INFINITY;
			if (g_iter.yStart() == Double.POSITIVE_INFINITY ||
					g_iter.s() == Double.POSITIVE_INFINITY) {
				segList.add(new Segment(g_iter.xStart(), f.yAtInfinity() , 0));
				xMax = g_iter.xStart();
				break;
			}
			
			while (DoubleMath.gt(g_iter.yStart(),f_iter.xEnd())
					&& f_iter.next()) { }
			
			xStart = g_iter.xStart();
			yStart = f_iter.segment().yAt(g_iter.yStart());
			
			while (DoubleMath.gt(g_iter.yEnd(), f_iter.xEnd())){
				xEnd = g_iter.segment().xAt(f_iter.xEnd());
				yEnd = f_iter.yEnd();
				if (!DoubleMath.eq(xStart, xEnd)){
					Segment s;
					if (yEnd==Double.POSITIVE_INFINITY && yStart==Double.POSITIVE_INFINITY){
						s = new Segment(xStart, yStart, Double.POSITIVE_INFINITY);
					}else {
						s = new Segment(xStart, yStart, (yEnd-yStart)/(xEnd-xStart));
					}
					segList.add(s);
				}
				//next f-segment
				f_iter.next();
				xStart = xEnd;
				yStart = f_iter.yStart();
			}
			xEnd = g_iter.xEnd();
			yEnd = f_iter.segment().yAt(g_iter.yEnd());

			if (!DoubleMath.eq(xStart, xEnd)){
				Segment s;
				if (yEnd==Double.POSITIVE_INFINITY && yStart==Double.POSITIVE_INFINITY){
					s = new Segment(xStart, yStart, Double.POSITIVE_INFINITY);
				}else {
					s = new Segment(xStart, yStart, (yEnd-yStart)/(xEnd-xStart));
				}
				segList.add(s);
			}
			if (yEnd == Double.POSITIVE_INFINITY){
				xMax = xEnd;
				break;
			}
		}
			
		Curve result = new Curve(segList, pdxNew, pdyNew, xMax, name);
		return result;
	}
	
	/**
	 * Compute the concatenation f(g) of two LOWER curves.
	 * @param f g - the two curves.
	 * @return the concatenation f(g)
	 * @since 2008-09-17
	 * @author reint
	 */
	public static Curve concatLower(Curve f, Curve g){
		if (!CurveChecker.isWideSenseIncreasing(f) || !CurveChecker.isWideSenseIncreasing(g)){
			throw new IllegalOperationException(Messages.SEGMENT_LIST_NOT_INVERTABLE);
		}
		
		double[] param = concatGetParameters(f,g);
		// param[0] = pdxNew, param[1] = pdyNew, param[2] = xMax
		long pdxNew = (long)param[0];
		double pdyNew = param[1], xMax = param[2];
		
		String name = "CONCAT(l)[("+f.name()+"),("+g.name()+")]";
		
		SegmentList segList = new SegmentList();
		CurveSegmentIterator f_iter = new CurveSegmentIterator(f);
		CurveSegmentIterator g_iter = new CurveSegmentIterator(g, xMax);
		
		double xStart = 0, xEnd = 0, yStart = 0, yEnd = 0;
		f_iter.next();
		
		while (g_iter.next()) {
			// check if g_iter.yStart() == Double.POSITIVE_INFINITY;
			if (g_iter.yStart() == Double.POSITIVE_INFINITY ||
					g_iter.s() == Double.POSITIVE_INFINITY) {
				segList.add(new Segment(g_iter.xStart(), f.yAtInfinity() , 0));
				xMax = g_iter.xStart();
				break;
			}
			
			while (DoubleMath.geq(g_iter.yStart(),f_iter.xEnd()) &&
					f_iter.next()) { }
			
			xStart = g_iter.xStart();
			yStart = f_iter.segment().yAt(g_iter.yStart());

			while (DoubleMath.geq(g_iter.yEnd(), f_iter.xEnd())){
				xEnd = g_iter.segment().xAt(f_iter.xEnd());
				yEnd = f_iter.yEnd();
				if (!DoubleMath.eq(xStart, xEnd)){
					Segment s;
					if (yEnd==Double.POSITIVE_INFINITY && yStart==Double.POSITIVE_INFINITY){
						s = new Segment(xStart, yStart, Double.POSITIVE_INFINITY);
					}else {
						s = new Segment(xStart, yStart, (yEnd-yStart)/(xEnd-xStart));
					}
					segList.add(s);
				}
				//next f-segment
				f_iter.next();
				xStart = xEnd;
				yStart = f_iter.yStart();
			}
			xEnd = g_iter.xEnd();
			yEnd = f_iter.segment().yAt(g_iter.yEnd());
			if (!DoubleMath.eq(xStart, xEnd)){
				Segment s;
				if (yEnd==Double.POSITIVE_INFINITY && yStart==Double.POSITIVE_INFINITY){
					s = new Segment(xStart, yStart, Double.POSITIVE_INFINITY);
				}else {
					s = new Segment(xStart, yStart, (yEnd-yStart)/(xEnd-xStart));
				}
				segList.add(s);
			}
			if (yEnd == Double.POSITIVE_INFINITY){
				xMax = xEnd;
				break;
			}
		}
			
		Curve result = new Curve(segList, pdxNew, pdyNew, xMax, name);
		return result;
	}
	
	/**
	 * Evaluates some parameters needed for the concatenation. The challenge is the
	 * handling of periodic and aperiodc curves.
	 * @param the curve.
	 * @return double[]{pdxNew, pdyNew, xMax};
	 * @since 2008-10-17
	 * @author reint
	 */
	protected static double[] concatGetParameters(Curve f, Curve g){
		long pdxNew;
		double pdyNew, xMax;
		
		if (f.hasPeriodicPart() && g.hasPeriodicPart()){
			long g_pdy = Math.round(g.pdy());
			if (DoubleMath.neq(g.pdy(), g_pdy)){
				throw new IllegalOperationException(Messages.PERIOD_NOT_INVERTABLE);
			}
			double yHyperPeriod = MathUtil.computeHyperPeriod(f.period(), g_pdy);
			pdxNew = (long)(yHyperPeriod/g_pdy * g.pdx());
			pdyNew = yHyperPeriod/f.period()*f.pdy();
			xMax = g.px0() + pdxNew;
			// if aperiodic part of g contains less events then the Curve f
			// in aperiodic part **)
			if (DoubleMath.gt(f.px0(), g.py0())) {
				// you don't know what happens within a period. Therefore you have to
				// take the whole period into account -> use ceil();
				xMax += Math.ceil((f.px0()-g.py0())/g_pdy)*g.pdx();
			}
		}
		else if(!f.hasPeriodicPart() && g.hasPeriodicPart()){
			Segment f_last = f.aperiodicPart().segments().lastSegment();
			if (f_last.y == Double.POSITIVE_INFINITY || f_last.s == Double.POSITIVE_INFINITY){
				pdxNew = 0;
				pdyNew = 0;
				xMax = Double.POSITIVE_INFINITY;
			} else {
				long g_pdy = Math.round(g.pdy());
				if (DoubleMath.neq(g.pdy(),g_pdy)){
					throw new IllegalOperationException(Messages.PERIOD_NOT_INVERTABLE);
				}
				pdxNew = g.period();
				pdyNew = f_last.s * g_pdy;
				xMax = g.px0() + pdxNew;
				if (DoubleMath.gt(f_last.x, g.py0())) { // -> see comment above **)
					xMax += Math.ceil((f_last.x-g.py0())/g_pdy)*g.pdx();
				}
			}
		}
		else if(f.hasPeriodicPart() && !g.hasPeriodicPart()) {
			Segment g_last = g.aperiodicPart().segments().lastSegment();
			if (DoubleMath.eq(g_last.s, 0.0) ||
					(g_last.y == Double.POSITIVE_INFINITY || g_last.s == Double.POSITIVE_INFINITY)){
				pdxNew = 0;
				pdyNew = 0;
				xMax = g_last.x + 1;	
			} else{
				if (DoubleMath.neq(1.0/g_last.s, Math.round(1.0/g_last.s))){
					throw new IllegalOperationException(Messages.PERIOD_NOT_INVERTABLE);
				}
				pdxNew = Math.round(f.pdx()/g_last.s);
				pdyNew = f.pdy();
				xMax = g_last.x + pdxNew;
				if (DoubleMath.gt(f.px0(), g_last.y)) { // -> see comment above **)
					xMax += Math.ceil((f.px0()-g_last.y)/g_last.s);
				}
			}
		}
		else { // both aperiodic
			Segment g_last = g.aperiodicPart().segments().lastSegment();
			if (DoubleMath.eq(g_last.s, 0.0)){
				pdxNew = 0;
				pdyNew = 0;
				xMax = g_last.x + 1;
			} else {
				if (DoubleMath.neq(1.0/g_last.s, Math.round(1.0/g_last.s))){
					throw new IllegalOperationException(Messages.PERIOD_NOT_INVERTABLE);
				}
				Segment f_last = f.aperiodicPart().segments().lastSegment();
				pdxNew = 0;
				pdyNew = 0;
				xMax = g_last.x + Math.round(1/g_last.s);
				if (DoubleMath.gt(f_last.x, g_last.y)) { // -> see comment above **)
					xMax += Math.ceil((f_last.x-g_last.y)/g_last.s);
				}
			}
		}
		return new double[]{pdxNew, pdyNew, xMax};
	}
    
    /**
     * Compute the x-value of an upper bound of the point after which the
     * Curves a and b do not cross anymore. If a and b do not have the same
     * long-term slope, then a must be the Curve with the lower long-term slope.
     * If a and b have the same long-term slope, the value may be Double.NaN, 
     * if no such upper bound can be guaranteed.
     * 
     * @param a the Curve with the lower long-term slope.
     * @param b the Curve with the higher long-term slope.
     * @return the x-value of an upper bound of the point after which the
     * Curves a and b do not cross anymore
     */
    protected static double computeUpperCrossX(Curve a, Curve b) {
        // PERFORMANCE: tight bounds vs. loose bounds to compute upperCrossX
        // Segment boundA = a.upperBound();
        // Segment boundB = b.lowerBound();
        Segment boundA = a.tightUpperBound();
        Segment boundB = b.tightLowerBound();
        
        if (DoubleMath.eq(boundA.s(), boundB.s())) {
            double startX = Math.max(boundA.x(), boundB.x());
            if (DoubleMath.leq(boundA.yAt(startX), boundB.yAt(startX))) {
                return startX;          
            }else {
                return Double.NaN;
            }
        } else {
            double crossX = ((Point)SegmentMath.crossPoint(boundA, boundB)).x();
            crossX = Math.max(crossX, boundA.x());
            crossX = Math.max(crossX, boundB.x());
            return crossX;
        }
    }
    
    /**
     * Computes computeUpperCrossX, but before the curves are moved along the
     * y-axis, such that a and b start at the same y-value for x=0.0. 
     * 
     * @param a the Curve with the lower long-term slope.
     * @param b the Curve with the higher long-term slope.
     * @return the x-value of an upper bound of the point after which the
     * Curves a and b that start at the same y-value do not cross anymore.
     */
    protected static double computeUpperCrossXWithCommonY0 (Curve a, Curve b) {
        Segment boundA = a.tightUpperBound();
        Segment boundB = b.tightLowerBound();
                
        if (DoubleMath.eq(boundA.s(), boundB.s())) {
            double xStart = Math.max(boundA.x(), boundB.x());
            if (DoubleMath.leq(boundA.yAt(xStart), boundB.yAt(xStart))) {
                return xStart;          
            }else {
                return Double.NaN;
            }
        } else {
            double y0A = a.y0epsilon();   // difference to computeUpperCrossX
            double y0B = b.y0epsilon();   //                "
            boundA.move(0.0, y0B - y0A);  //                "
            double xCross = SegmentMath.crossPoint(boundA, boundB).x();
            xCross = Math.max(xCross, boundA.x());
            xCross = Math.max(xCross, boundB.x());
            return xCross;
        }
    }
    
    /**
     * Compute the y-value of an upper bound of the point after which the
     * Curves a and b do not cross anymore. If a and b do not have the same
     * long-term slope, then a must be the Curve with the lower long-term slope.
     * If a and b have the same long-term slope, the value may be Double.NaN, 
     * if no such upper bound can be guaranteed.
     * 
     * @param a the Curve with the lower long-term slope.
     * @param b the Curve with the higher long-term slope.
     * @return the y-value of an upper bound of the point after which the
     * Curves a and b do not cross anymore
     */
    protected static double computeUpperCrossY(Curve a, Curve b) {
        // PERFORMANCE: tight bounds vs. loose bounds to compute upperCrossX
        // Segment boundA = a.upperBound();
        // Segment boundB = b.lowerBound();
        Segment boundA = a.tightUpperBound();
        Segment boundB = b.tightLowerBound();
        
        if (DoubleMath.eq(boundA.s(), boundB.s())) {
            // FIXME is this correct?
            double startX = Math.max(boundA.x(), boundB.x());
            if (DoubleMath.leq(boundA.yAt(startX), boundB.yAt(startX))) {
                return boundB.yAt(startX);          
            }else {
                return Double.NaN;
            }
        } else {
            double crossY = ((Point)SegmentMath.crossPoint(boundA, boundB)).y();
            crossY = Math.max(crossY, boundA.y());
            crossY = Math.max(crossY, boundB.y());
            return crossY;
        }
    }
    
    /**
     * Computes maxPlusConv(c,0) on the SegmentList c under the assumption
     * that the greatest y-value before the start of the first Segment in c
     * was startY.
     * 
     * @param c the SegmentList to compute maxPlusConv(c,0)
     * @param startY the greates y-value that occurred before the start of the
     * first Segment in c.
     * @return maxPlusConv(c,0)
     */
    protected static SegmentList maxPlusConv0Help(SegmentList c, double startY) {
        double xMax = Double.POSITIVE_INFINITY;
        SegmentList result = new SegmentList(c.size());      
        SegmentListIterator iter = new SegmentListIterator(c, xMax);
        double currentYMax = startY;
        boolean zeroSegmentAdded = false;
        Point crossPoint = null;
        
        while(iter.next()) {
            if (DoubleMath.lt(iter.yStart(), currentYMax)) {
                if (!zeroSegmentAdded) {
                    result.add(new Segment(iter.xStart(), currentYMax, 0.0));
                    zeroSegmentAdded = true;
                }
                if (DoubleMath.gt(iter.yEnd(), currentYMax)) {
                    crossPoint = SegmentMath.crossPoint(iter.segment(), 
                            new Segment(iter.xStart(), currentYMax, 0.0));
                    result.add(new Segment(crossPoint.x(), crossPoint.y(), iter.s()));
                    zeroSegmentAdded = false;
                    currentYMax = iter.yEnd();
                }
            } else if (DoubleMath.gt(iter.yStart(), currentYMax)) {
                if (DoubleMath.leq(iter.s(), 0.0)) {
                    result.add(new Segment(iter.xStart(), iter.yStart(), 0.0));
                    zeroSegmentAdded = true;
                    currentYMax = iter.yStart();
                } else {
                    result.add(new Segment(iter.xStart(), iter.yStart(), iter.s()));
                    zeroSegmentAdded = false;
                    currentYMax = iter.yEnd();
                }
            } else {
                if (DoubleMath.leq(iter.s(), 0.0)) {
                    if (!zeroSegmentAdded) {
                        result.add(new Segment(iter.xStart(), iter.yStart(), 0.0));
                        zeroSegmentAdded = true;
                        currentYMax = iter.yStart();
                    }
                } else {
                    result.add(new Segment(iter.xStart(), iter.yStart(), iter.s()));
                    zeroSegmentAdded = false;
                    currentYMax = iter.yEnd();
                }
            }               
        }
        return result;
    }
    
    /**
     * Computes maxPlusDeconv(c,0) on the SegmentList c. relY0 defines the 
     * relative minimum y-value that the result should have, if c goes to 
     * y -> -Inf for x -> Inf.
     * 
     * @param c the SegmentList to compute maxPlusDeconv(c,0)
     * @param relY0 the relative minimum y-value.
     * @return maxPlusDeconv(c,0)
     */
    protected static SegmentList maxPlusDeconv0Help(SegmentList c, double relY0) {
        return maxPlusDeconv0Help(c, relY0, 
                Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
    }
        
    /**
     * Computes maxPlusDeconv(c,0) on the SegmentList c. relY0 defines the 
     * relative minimum y-value that the result should have, if c goes to 
     * y -> -Inf for x -> Inf.
     * 
     * @param c the SegmentList to compute maxPlusDeconv(c,0)
     * @param relY0 the relative minimum y-value.
     * @param xMax the maximum x-value.
     * @param yAtXMax the y-value at xMax.
     * @return maxPlusDeconv(c,0)
     */
    protected static SegmentList maxPlusDeconv0Help(SegmentList c, double relY0, 
            double xMax, double yAtXMax) {
        SegmentList result = new SegmentList(c.size());
        SegmentListIterator iter = new SegmentListIterator(c, xMax);
        double currentYMin = yAtXMax;
        boolean zeroSegmentAdded = false;
        Point crossPoint = null;
        iter.resetToEnd();
        while(iter.prev()) {
            if (Double.isInfinite(iter.xEnd())) {
                if (DoubleMath.gt(iter.s(), 0.0)) {
                    result.add(0, new Segment(iter.xStart(), iter.yStart(), iter.s()));
                    zeroSegmentAdded = false;
                    currentYMin = iter.yStart();
                } else if (DoubleMath.eq(iter.s(), 0.0)) {
                    result.add(0, new Segment(iter.xStart(), iter.yStart(), 0.0));
                    zeroSegmentAdded = true;
                    currentYMin = iter.yStart();
                } else {
                    result.add(0, new Segment(0.0, relY0, 0.0));
                    break;
                }
            } else if (DoubleMath.gt(iter.yEnd(), currentYMin)) {
                if (DoubleMath.lt(iter.yStart(), currentYMin)) {
                    crossPoint = SegmentMath.crossPoint(iter.segment(), 
                            new Segment(iter.xStart(), currentYMin, 0.0));
                    if (zeroSegmentAdded) result.remove(0);
                    result.add(0, new Segment(crossPoint.x(), crossPoint.y(), 0.0));
                    zeroSegmentAdded = true;                        
                    if (DoubleMath.gt(crossPoint.x(), iter.xStart())) {
                        result.add(0, new Segment(iter.xStart(), 
                                iter.yStart(), iter.s()));
                        zeroSegmentAdded = false;                       
                    }
                    currentYMin = iter.yStart();
                } else {
                    if (zeroSegmentAdded) result.remove(0);
                    result.add(0, new Segment(iter.xStart(), currentYMin, 0.0));
                    zeroSegmentAdded = true;
                }
            } else if (DoubleMath.lt(iter.yEnd(), currentYMin)) {
                if (DoubleMath.leq(iter.s(), 0.0)) {
                    result.add(0, new Segment(iter.xStart(), iter.yEnd(), 0.0));
                    zeroSegmentAdded = true;
                    currentYMin = iter.yEnd();
                } else {
                    result.add(0, new Segment(iter.xStart(), iter.yStart(), iter.s()));
                    zeroSegmentAdded = false;
                    currentYMin = iter.yStart();
                }
            } else {
                if (DoubleMath.leq(iter.s(), 0.0)) {
                    if (zeroSegmentAdded) result.remove(0);
                    result.add(0, new Segment(iter.xStart(), iter.yEnd(), 0.0));
                    zeroSegmentAdded = true;
                    currentYMin = iter.yEnd();
                } else {
                    result.add(0, new Segment(iter.xStart(), iter.yStart(), iter.s()));
                    zeroSegmentAdded = false;
                    currentYMin = iter.yStart();
                }
            }
            if (DoubleMath.eq(currentYMin, relY0)) {
                if (DoubleMath.gt(iter.xStart(), 0.0)) {
                    result.add(0, new Segment(0.0, relY0, 0.0));
                }
                break;
            } else if (DoubleMath.lt(currentYMin, relY0)) {
                result.remove(0);
                if (DoubleMath.gt(iter.s(), 0.0)) {
                    crossPoint = SegmentMath.crossPoint(iter.segment(), 
                            new Segment(0.0, relY0, 0.0));
                    if (result.size() == 0 || DoubleMath.lt(crossPoint.x(), 
                            ((Segment)result.get(0)).x())) {
                        result.add(0, new Segment(crossPoint.x(), relY0, iter.s()));
                    }
                }
                result.add(0, new Segment(0.0, 
                        relY0, 0.0));                  
                break;
            }
        }
        return result;
    }

    
    protected static SegmentList maxPlusConvHelp(Curve a, Curve b, double xUnfold) {
        // FIXME segmentsLEQ or segmentsLT??
        SegmentListIterator iter1 = new SegmentListIterator(a.segmentsLEQ(xUnfold), xUnfold);
        SegmentListIterator iter2 = new SegmentListIterator(b.segmentsLEQ(xUnfold), xUnfold);
        long preinitLong = Math.min((long)Math.ceil(
                (long)iter1.size() * (long)iter2.size() * Parameters.PRE_INIT_FACTOR),
                (long)Parameters.MAX_PRE_INIT);
        int preinit = (int)Math.min((double)Integer.MAX_VALUE, preinitLong);
//        System.out.print("s:" + iter1.size() + "/");
//        System.out.print("" + iter2.size() + " ");
//        System.out.print("Preinit: " + preinit + " ");
        SegmentList result = new SegmentList(preinit);
//System.out.print("Preinit: " + preinit + " ");
        int resultLenMax = Parameters.MAX_PRE_INIT;

        // Because all Curves have y=0 at x=0
        if (DoubleMath.neq(a.y0epsilon(), 0.0)) {
            while(iter2.next()) {
                result.add(new LimitedSegment(
                        iter2.xStart(),
                        iter2.yStart(),
                        iter2.s(),
                        iter2.xEnd()));
            }
            iter2.reset();
        }
        if (DoubleMath.neq(b.y0epsilon(), 0.0)) {
            while(iter1.next()) {
                result.add(new LimitedSegment(
                        iter1.xStart(),
                        iter1.yStart(),
                        iter1.s(),
                        iter1.xEnd()));
            }
            iter1.reset();
        }

        // Start of real computation
        if (iter1.size() < iter2.size()) {
            SegmentListIterator iterTemp = iter1;
            iter1 = iter2;
            iter2 = iterTemp;
        }
        int i, limit = 0;
        double xStart, xMiddle, xEnd;
        
        while(iter1.next()) {
            iter2.reset();
            limit++;
            i = 0;
            while (i < limit && iter2.next()) {
                if (result.size() >= resultLenMax) {
//System.out.print("Reduced result from " + result.size());
                    result = filterMaxLimitedCurveSegments(result);
//System.out.println(" to " + result.size() + " segments.");
                    if (result.size() > (resultLenMax * Parameters.MAX_POP_FACTOR)) { 
//System.out.print("Increase result length from " + resultLenMax);
                        resultLenMax = (int)Math.ceil(resultLenMax * 
                                Parameters.INCREASE_FACTOR);
//System.out.println(" to " + resultLenMax + " segments.");
                        result.ensureCapacity(resultLenMax);
                    }
                }
                xStart = iter1.xStart() + iter2.xStart();
                if (DoubleMath.leq(xStart, xUnfold)) {
                    xEnd = Math.min(iter1.xEnd() + iter2.xEnd(), xUnfold);
                    if (DoubleMath.gt(iter1.s(), iter2.s())) {
                        xMiddle = Math.min(iter1.xEnd() + iter2.xStart(), xUnfold);                        
                        result.add(new LimitedSegment(
                                xStart,
                                iter1.yStart() + iter2.yStart(),
                                iter1.s(), 
                                xMiddle));
                        if (DoubleMath.geq(iter1.xEnd() + iter2.xEnd(), xMiddle)) {
                            xEnd = Math.min(iter1.xEnd() + iter2.xEnd(), xUnfold);
                            result.add(new LimitedSegment(
                                    xMiddle,
                                    iter1.yEnd() + iter2.yStart(),
                                    iter2.s(), 
                                    xEnd));  
                        }
                    } else if (DoubleMath.lt(iter1.s(), iter2.s())) {
                        xMiddle = Math.min(iter1.xStart() + iter2.xEnd(), xUnfold);
                        result.add(new LimitedSegment(
                                xStart,
                                iter1.yStart() + iter2.yStart(),
                                iter2.s(), 
                                xMiddle));
                        if (DoubleMath.geq(iter1.xEnd() + iter2.xEnd(), xMiddle)) {
                            xEnd = Math.min(iter1.xEnd() + iter2.xEnd(), xUnfold);
                            result.add(new LimitedSegment(
                                    xMiddle,
                                    iter1.yStart() + iter2.yEnd(),
                                    iter1.s(), 
                                    xEnd));
                        }               
                    } else {
                        xEnd = Math.min(iter1.xEnd() + iter2.xEnd(), xUnfold);                     
                        result.add(new LimitedSegment(
                                xStart,
                                iter1.yStart() + iter2.yStart(),
                                iter1.s(), 
                                xEnd));
                    }
                }
            }
        }
//System.out.println("ResultLen: " + result.size() + "; ration = " + (double)result.size()/(double)preinit);
        return result;
    }

    protected static SegmentList minPlusDeconvHelp(Curve a, Curve b, double xUnfold) {
        // FIXME segmentsLEQ or segmentsLT??
        SegmentListIterator iter1 = new SegmentListIterator(a.segmentsLEQ(xUnfold), xUnfold);
        SegmentListIterator iter2 = new SegmentListIterator(b.segmentsLEQ(xUnfold), xUnfold);
        
        int preinit = Math.min((int)Math.ceil(
                iter1.size() * iter2.size() * Parameters.PRE_INIT_FACTOR),
                Parameters.MAX_PRE_INIT);        
        SegmentList result = new SegmentList(preinit);
        
        int resultLenMax = Parameters.MAX_PRE_INIT;

        //PERFORMANCE: we do not need to calculate every combination twice!!!
        int i;
        double xEnd;
        Segment seg1, seg2;
        while(iter1.next()) {
            iter2.reset();
            while (iter2.next()) {  
                if (result.size() >= resultLenMax) {
                    result = filterMaxLimitedCurveSegments(result);
                    if (result.size() > (resultLenMax * Parameters.MAX_POP_FACTOR)) { 
                        resultLenMax = (int)Math.ceil(resultLenMax * Parameters.INCREASE_FACTOR);
                        result.ensureCapacity(resultLenMax);
                    }
                }

                seg1 = null;
                seg2 = null;
                xEnd = iter1.xEnd() - iter2.xStart();
                
                if (DoubleMath.gt(iter1.s(), iter2.s())) {
                    if (DoubleMath.geq(iter1.xEnd(), iter2.xStart())) {
                        if (DoubleMath.geq(iter1.xStart(), iter2.xEnd())) {
                            seg1 = new Segment(
                                    iter1.xStart() - iter2.xEnd(),
                                    iter1.yStart() - iter2.yEnd(),
                                    iter1.s()
                            );
                        } else if (DoubleMath.gt(iter1.xEnd(), iter2.xEnd())) {
                            seg1 = new Segment(
                                    0.0,
                                    iter1.yStart() - iter2.yEnd() 
                                    + iter1.s() 
                                    * (iter2.xEnd() - iter1.xStart()),
                                    iter1.s()
                            );
                        }
                        if (DoubleMath.geq(iter1.xEnd(), iter2.xEnd())) {
                            seg2 = new Segment(
                                    iter1.xEnd() - iter2.xEnd(),
                                    iter1.yEnd() - iter2.yEnd(),
                                    iter2.s()
                            );
                        } else if (DoubleMath.gt(iter1.xEnd(), iter2.xStart())) {
                            seg2 = new Segment(
                                    0.0,
                                    iter1.yEnd() - iter2.yStart()
                                    - iter2.s()
                                    * (iter1.xEnd() - iter2.xStart()),
                                    iter2.s()
                            );
                        }
                    }
                } else {
                    if (DoubleMath.geq(iter1.xEnd(), iter2.xStart())) {
                        if (DoubleMath.geq(iter1.xStart(), iter2.xEnd())) {
                            seg1 = new Segment(
                                    iter1.xStart() - iter2.xEnd(),
                                    iter1.yStart() - iter2.yEnd(),
                                    iter2.s()
                            );
                        } else if (DoubleMath.gt(iter1.xStart(), iter2.xStart())) {
                            seg1 = new Segment(
                                    0.0,
                                    iter1.yStart() - iter2.yStart() 
                                    - iter2.s() 
                                    * (iter1.xStart() - iter2.xStart()),
                                    iter2.s()
                            );
                        }
                        if (DoubleMath.geq(iter1.xStart(), iter2.xStart())) {
                            seg2 = new Segment(
                                    iter1.xStart() - iter2.xStart(),
                                    iter1.yStart() - iter2.yStart(),
                                    iter1.s()
                            );
                        } else if (DoubleMath.gt(iter1.xEnd(), iter2.xStart())) {
                            seg2 = new Segment(
                                    0.0,
                                    iter1.yStart() - iter2.yStart()
                                    + iter1.s()
                                    * (iter2.xStart() - iter1.xStart()),
                                    iter1.s()
                            );
                        }
                    }
                }
                
                if (seg2 != null) {
                    if (seg1 != null) {
                        result.add(new LimitedSegment(
                                seg1.x(), seg1.y(), 
                                seg1.s(), seg2.x()));
                        result.add(new LimitedSegment(
                                seg2.x(), seg2.y(), 
                                seg2.s(), xEnd)); 
                    } else {
                        result.add(new LimitedSegment(
                                seg2.x(), seg2.y(), 
                                seg2.s(), xEnd));
                    }
                }
            }
        }
        
        //remove all with x > upperUnfoldX!!
        // PERFORMANCE this could be done above!!!
        i = 0;
        LimitedSegment seg;
        while (i < result.size()) {
            seg = (LimitedSegment)result.get(i);
            if (DoubleMath.gt(seg.x(), xUnfold)) {
                result.remove(i);
            } else if (DoubleMath.gt(seg.xEnd(), xUnfold)) {
                seg.setXEnd(xUnfold);
                i++;
            } else {
                i++;
            }
        }
        return result;
    }
    
    protected static SegmentList filterMaxLimitedCurveSegments(SegmentList input) {
        SegmentList result = new SegmentList(input.size());
        
        // Build an array of LimitedCurveSegments, that are sorted by
        // increasing x-values of their start-point.
        LimitedSegment[] array = new LimitedSegment[0];
        LimitedSegment[] segments = (LimitedSegment[])input.toArray(array);
        Arrays.sort(segments);
        
        // Points of interest are all start- and end-points of all segments. 
        double[] poi = new double[segments.length * 2];
        for (int i = 0; i < segments.length; i++) {
            poi[2 * i] = segments[i].x();
            poi[(2 * i) + 1] = segments[i].xEnd();
        }
        Arrays.sort(poi);
        
        int poiIndex = 0;
        int segmentsIndex = 0;
        int i;
        ArrayList<Segment> activeSegments = new ArrayList<Segment>();
        ArrayList<Segment> takeOverSegments = new ArrayList<Segment>();
        double y, maxY, endY, xCross, minXCross;
        double segStart, segEnd;
        LimitedSegment maxSegment = null;
        LimitedSegment lastMaxSegment = null;
        // Iterate over all points of interest.
        while(poiIndex < poi.length) {
            // Add all segments that start at the current point of interest
            // to the active segments.
            while((segmentsIndex < segments.length) &&
                    DoubleMath.eq(segments[segmentsIndex].x(), poi[poiIndex])) {
                activeSegments.add(segments[segmentsIndex]);
                segmentsIndex++;
            }
            maxSegment = null;
            maxY = Double.NEGATIVE_INFINITY;
            i = 0;
            // Compute the y-value of all active segments at the current point
            // of interest. maxSegment contains the segment with the maximum 
            // y-value that does not end at the current point of interest.
            // (the maximum slope is used as third criterion).
            while(i < activeSegments.size()) {
                y = ((LimitedSegment)activeSegments.get(i)).yAt(poi[poiIndex]);
                if (Double.isNaN(y)) {
                    activeSegments.remove(i);
                } else if (DoubleMath.gt(y, maxY)) {
                    if (DoubleMath.gt(((LimitedSegment)activeSegments
                            .get(i)).xEnd(), poi[poiIndex])
                            || DoubleMath.eq(poi[poiIndex], 
                                    poi[poi.length - 1])) {
                        maxY = y;
                        maxSegment = (LimitedSegment)activeSegments.get(i);
                    }
                    i++;
                } else if (DoubleMath.eq(y, maxY)) {
                    if (maxSegment == null || DoubleMath.lt(maxSegment.s(), 
                            ((LimitedSegment)activeSegments.get(i)).s())) {
                        if (DoubleMath.gt(((LimitedSegment)activeSegments
                                .get(i)).xEnd(), poi[poiIndex])
                                || DoubleMath.eq(poi[poiIndex], 
                                        poi[poi.length - 1])) {
                            maxSegment = (LimitedSegment)activeSegments.get(i);
                        }
                    }
                    i++;
                } else {
                    i++;
                }
            }
            
            segStart = poi[poiIndex];           
            // Move to the next point of interest.
            poiIndex++;
            while((poiIndex < poi.length) &&
                    DoubleMath.eq(poi[poiIndex], poi[poiIndex - 1])) {

                poiIndex++;
            }
            if (poiIndex < poi.length) {
                segEnd = poi[poiIndex];
            } else {
                segEnd = poi[poiIndex - 1];
            }

            // add the maxSegment
            if (maxSegment != null) {
                if (maxSegment != lastMaxSegment) {
                    result.add(new LimitedSegment(segStart, 
                            maxSegment.yAt(segStart), 
                            maxSegment.s(), segEnd));
                    lastMaxSegment = maxSegment;
                } else {
                    ((LimitedSegment)result.lastSegment()).setXEnd(segEnd);
                }
            }
            
            if (poiIndex < poi.length) {
                endY = lastMaxSegment.yAt(poi[poiIndex]);
                i = 0;
                // Compute the y-value of all active segments at the next point
                // of interest. takeOverSegments contains a list
                // of all segments that overtake the current maxSegment before
                // the next point of interest.
                while(i < activeSegments.size()) {
                    y = ((LimitedSegment)activeSegments.get(i)).yAt(poi[poiIndex]);
                    if (Double.isNaN(y)) {
                        activeSegments.remove(i);
                    } else if (DoubleMath.gt(y, endY)) {
                        takeOverSegments.add(activeSegments.get(i));
                        i++;
                    } else {
                        i++;
                    }
                }
                
                while(takeOverSegments.size() > 0) {
                    minXCross = poi[poiIndex];
                    for (i = 0; i < takeOverSegments.size(); i++) {
                        xCross = SegmentMath.crossPoint(lastMaxSegment, 
                                (LimitedSegment)takeOverSegments.get(i)).x();
                        if (DoubleMath.lt(xCross, minXCross)) {
                            minXCross = xCross;
                            maxSegment = (LimitedSegment)takeOverSegments.get(i);
                        } else if (DoubleMath.eq(xCross, minXCross) && DoubleMath.gt(
                                ((LimitedSegment)takeOverSegments.get(i)).s(),
                                maxSegment.s())) {
                            minXCross = xCross;
                            maxSegment = (LimitedSegment)takeOverSegments.get(i);
                        }
                    } 
                    
                    // add the maxSegment
                    if (maxSegment != null && maxSegment != lastMaxSegment) {
                        result.add(new LimitedSegment(minXCross, 
                                maxSegment.yAt(minXCross), 
                                maxSegment.s(),poi[poiIndex]));
                        lastMaxSegment = maxSegment;
                    }
                    
                    // remove the segments from takeOverSegments, that do not
                    // overtake the new maxSegment
                    endY = maxSegment.yAt(poi[poiIndex]);
                    i = 0;
                    while(i < takeOverSegments.size()) {
                        y = ((LimitedSegment)takeOverSegments.get(i))
                        .yAt(poi[poiIndex]);
                        if (Double.isNaN(y)) {
                            takeOverSegments.remove(i);
                        } else if (DoubleMath.leq(y, endY)) {
                            takeOverSegments.remove(i);
                        } else {
                            i++;
                        }
                    }
                }
            }
        }
        return result;        
    }

    protected static SegmentList maxOfLimitedCurveSegments(SegmentList input) {
        SegmentList result = new SegmentList();
        
        // Build an array of LimitedCurveSegments, that are sorted by
        // increasing x-values of their start-point.
        LimitedSegment[] array = new LimitedSegment[0];
        LimitedSegment[] segments = (LimitedSegment[])input.toArray(array);
        Arrays.sort(segments);
        
        // Points of interest are all start- and end-points of all segments. 
        double[] poi = new double[segments.length * 2];
        for (int i = 0; i < segments.length; i++) {
            poi[2 * i] = segments[i].x();
            poi[(2 * i) + 1] = segments[i].xEnd();
        }
        Arrays.sort(poi);
        
        int poiIndex = 0;
        int segmentsIndex = 0;
        int i;
        ArrayList<Segment> activeSegments = new ArrayList<Segment>();
        ArrayList<Segment> takeOverSegments = new ArrayList<Segment>();
        double y, maxY, endY, xCross, minXCross;
        LimitedSegment maxSegment = null;
        LimitedSegment lastMaxSegment = null;
        // Iterate over all points of interest.
        while(poiIndex < poi.length) {
            // Add all segments that start at the current point of interest
            // to the active segments.
            while((segmentsIndex < segments.length) &&
                    DoubleMath.eq(segments[segmentsIndex].x(), poi[poiIndex])) {
                activeSegments.add(segments[segmentsIndex]);
                segmentsIndex++;
            }
            maxSegment = null;
            maxY = Double.NEGATIVE_INFINITY;
            i = 0;
            // Compute the y-value of all active segments at the current point
            // of interest. maxSegment contains the segment with the maximum 
            // y-value that does not end at the current point of interest.
            // (the maximum slope is used as third criterion).
            while(i < activeSegments.size()) {
                y = ((LimitedSegment)activeSegments.get(i)).yAt(poi[poiIndex]);
                if (Double.isNaN(y)) {
                    activeSegments.remove(i);
                } else if (DoubleMath.gt(y, maxY)) {
                    if (DoubleMath.gt(((LimitedSegment)activeSegments
                            .get(i)).xEnd(), poi[poiIndex])
                            || DoubleMath.eq(poi[poiIndex], 
                                    poi[poi.length - 1])) {
                        maxY = y;
                        maxSegment = (LimitedSegment)activeSegments.get(i);
                    }
                    i++;
                } else if (DoubleMath.eq(y, maxY)) {
                    if (maxSegment == null || DoubleMath.lt(maxSegment.s(), 
                            ((LimitedSegment)activeSegments.get(i)).s())) {
                        if (DoubleMath.gt(((LimitedSegment)activeSegments
                                .get(i)).xEnd(), poi[poiIndex])
                                || DoubleMath.eq(poi[poiIndex], 
                                        poi[poi.length - 1])) {
                            maxSegment = (LimitedSegment)activeSegments.get(i);
                        }
                    }
                    i++;
                } else {
                    i++;
                }
            }
            
            // add the maxSegment
            if (maxSegment != null && maxSegment != lastMaxSegment) {
                result.add(new Segment(poi[poiIndex], 
                        maxSegment.yAt(poi[poiIndex]), 
                        maxSegment.s()));
                lastMaxSegment = maxSegment;
            }
                
            // Move to the next point of interest.
            poiIndex++;
            while((poiIndex < poi.length) &&
                    DoubleMath.eq(poi[poiIndex], poi[poiIndex - 1])) {
                poiIndex++;
            }

            if (poiIndex < poi.length) {
                endY = lastMaxSegment.yAt(poi[poiIndex]);
                i = 0;
                // Compute the y-value of all active segments at the next point
                // of interest. takeOverSegments contains a list
                // of all segments that overtake the current maxSegment before
                // the next point of interest.
                while(i < activeSegments.size()) {
                    y = ((LimitedSegment)activeSegments.get(i)).yAt(poi[poiIndex]);
                    if (Double.isNaN(y)) {
                        activeSegments.remove(i);
                    } else if (DoubleMath.gt(y, endY)) {
                        takeOverSegments.add(activeSegments.get(i));
                        i++;
                    } else {
                        i++;
                    }
                }

                while(takeOverSegments.size() > 0) {
                    minXCross = poi[poiIndex];
                    for (i = 0; i < takeOverSegments.size(); i++) {
                        xCross = SegmentMath.crossPoint(lastMaxSegment, 
                                (LimitedSegment)takeOverSegments.get(i)).x();
                        if (DoubleMath.lt(xCross, minXCross)) {
                            minXCross = xCross;
                            maxSegment = (LimitedSegment)takeOverSegments.get(i);
                        } else if (DoubleMath.eq(xCross, minXCross) && DoubleMath.gt(
                                        ((LimitedSegment)takeOverSegments.get(i)).s(),
                                        maxSegment.s())) {
                            minXCross = xCross;
                            maxSegment = (LimitedSegment)takeOverSegments.get(i);
                        }
                    } 
                    
                    // add the maxSegment
                    if (maxSegment != null && maxSegment != lastMaxSegment) {
                        result.add(new Segment(minXCross, 
                                maxSegment.yAt(minXCross), 
                                maxSegment.s()));
                        lastMaxSegment = maxSegment;
                    }
                    
                    // remove the segments from takeOverSegments, that do not
                    // overtake the new maxSegment
                    endY = maxSegment.yAt(poi[poiIndex]);
                    i = 0;
                    while(i < takeOverSegments.size()) {
                        y = ((LimitedSegment)takeOverSegments.get(i))
                                .yAt(poi[poiIndex]);
                        if (Double.isNaN(y)) {
                            takeOverSegments.remove(i);
                        } else if (DoubleMath.leq(y, endY)) {
                            takeOverSegments.remove(i);
                        } else {
                            i++;
                        }
                    }
                }
            }
        }
        return result;
    }

    protected static SegmentList ceilHelp(Curve c, double xUpper) {
        //xUpper = xUpper + Parameters.SAFETY_MARGIN;
        CurveSegmentIterator iter = c.segmentIterator(xUpper);
        SegmentList result = new SegmentList();
        double yStart, yEnd, xCurrent, dx, dy, y;

        while(iter.next()) {
            yStart = Math.ceil(iter.yStart() - Parameters.PRECISION);
            yEnd = Math.ceil(iter.yEnd() - Parameters.PRECISION);
            if (DoubleMath.eq(yStart, yEnd)) {
                result.add(new Segment(iter.xStart(), yEnd, 0));
            } else if (DoubleMath.lt(yStart, yEnd)){
                dx = iter.xEnd() - iter.xStart();
                dy = iter.yEnd() - iter.yStart();
                if (DoubleMath.eq(yStart, iter.yStart())) {
                    y = yStart + 1;
                } else {
                    y = yStart;
                }
                result.add(new Segment(iter.xStart(), y, 0));
                xCurrent = iter.xStart() + ((y - iter.yStart()) * (dx / dy));
                while (DoubleMath.lt(xCurrent, iter.xEnd())) {
                    y = y + 1;
                    result.add(new Segment(xCurrent, y, 0));
                    xCurrent += dx / dy;
                }
            } else {
                dx = iter.xEnd() - iter.xStart();
                dy = iter.yEnd() - iter.yStart();
                y = yStart;
                result.add(new Segment(iter.xStart(), y, 0));
                xCurrent = iter.xStart() + Math.abs(((iter.yStart() - y - 1) * (dx / dy)));
                while (DoubleMath.lt(xCurrent, iter.xEnd())) {
                    y = y - 1;
                    result.add(new Segment(xCurrent, y, 0));
                    xCurrent += Math.abs(dx / dy);
                }
            }
        }
        return result;
    }

    protected static SegmentList floorHelp(Curve c, double xUpper) {
        //xUpper = xUpper + Parameters.SAFETY_MARGIN;
        CurveSegmentIterator iter = c.segmentIterator(xUpper);
        SegmentList result = new SegmentList();
        double yStart, yEnd, xCurrent, dx, dy, y;

        while(iter.next()) {
            yStart = Math.floor(iter.yStart() + Parameters.PRECISION);
            yEnd = Math.floor(iter.yEnd() + Parameters.PRECISION);
            if (DoubleMath.eq(yStart, yEnd)) {
                result.add(new Segment(iter.xStart(), yStart, 0));
            } else if (DoubleMath.lt(yStart, yEnd)){
                dx = iter.xEnd() - iter.xStart();
                dy = iter.yEnd() - iter.yStart();
                if (DoubleMath.eq(yStart, iter.yStart())) {
                    y = yStart;
                } else {
                    y = yStart;
                }
                result.add(new Segment(iter.xStart(), y, 0));
                xCurrent = iter.xStart() + ((y + 1 - iter.yStart()) * (dx / dy));
                while (DoubleMath.lt(xCurrent, iter.xEnd())) {
                    y = y + 1;
                    result.add(new Segment(xCurrent, y, 0));
                    xCurrent += dx / dy;
                }
            } else {
                dx = iter.xEnd() - iter.xStart();
                dy = iter.yEnd() - iter.yStart();
                if (DoubleMath.eq(yStart, iter.yStart())) {
                    y = yStart - 1;
                } else {
                    y = yStart;
                }
                result.add(new Segment(iter.xStart(), y, 0));
                xCurrent = iter.xStart() + Math.abs(((iter.yStart() - y) * (dx / dy)));
                while (DoubleMath.lt(xCurrent, iter.xEnd())) {
                    y = y - 1;
                    result.add(new Segment(xCurrent, y, 0));
                    xCurrent += Math.abs(dx / dy);
                }
            }
        }
        return result;
    }
       
	 /**
	 * Inserts a Segment(0,0,0) as first Segment of the Curve. This is
	 * to evaluate an upper Curve at x=0.
	 * This function is needed in the function concatUpper
	 * 
	 * @param the curve.
	 * @return the curve with periodic part
	 * @since 2008-11-18
	 * @author reint
	 */
    protected static Curve insertZeroSegment(Curve c){
    	Curve z = c.clone();
    	if (z.hasAperiodicPart()){
    		if (DoubleMath.gt(z.aperiodicSegments().segment(0).y, 0)){
    			z.aperiodicSegments().add(0, new Segment(0,0,0));
    		}
    	}
    	else if (z.hasPeriodicPart()){
    		if (DoubleMath.gt(z.py0(), 0) || DoubleMath.gt(z.periodicSegments().segment(0).y, 0)){
	    		SegmentList segList = new SegmentList(1);
	    		segList.add(new Segment(0,0,0));
	    		z.setAperiodicPart(new AperiodicPart(segList));
        	}
       	}
    	return z;
    }
    
    public static double X_UPPER_INIT;
    public static double X_UPPER_FIN;
}
