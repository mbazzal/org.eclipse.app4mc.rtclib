/* Parameters implementation

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit			none (wandeler@tik.ee.ethz.ch)

Created: 06.01.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

/**
 * <code>Parameters</code> provides globel parameters.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: Parameters.java 429 2007-07-05 14:02:30Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    none (wandeler@tik.ee.thz.ch)
 */
public class Parameters {

	
    /**
     * The precistion that is used for comparisons of doubles.
     * That is two doubles <code>a</code> and <code>b</code>
     * are considered to be equal, if <code>Math.abs(a - b) < PRECISION</code>.
     */
    static public double PRECISION = 0.00001;

    /**
     * The factor that is used to compute SAFETY_MARGIN.
     */
    static public double SAFETY_MARGIN_FACTOR = 100;

    /**
     * A safety margin that is used in some operations when unfolding a curve 
     * up and including to a value <code>x</code>. To ensure that the a curve
     * segment that starts at <code>x</code> is really included, some operations
     * unfold the curve up to a value <code>x + SAFETY_MARGIN</code>. 
     * <p>
     * <code>SAFETY_MARGIN</code> is computed as the product of 
     * <code>SAFETY_MARGIN_FACTOR</code> and 
     * <code>PRECISION</code>.
     */
    static public double SAFETY_MARGIN = SAFETY_MARGIN_FACTOR * PRECISION;
    
    /**
     * The file path separator of the operating system.
     */
    static public String PATH_SEPARATOR = System.getProperty("file.separator");

    
    /**
     * Set <code>PRECISION</code>. Note that <code>SAFETY_MARGIN</code> is also 
     * updated.
     * 
     * @param precision the new value for <code>PRECISION</code>.
     */
    static public void setPrecision(double precision) {
        PRECISION = precision;
        SAFETY_MARGIN = SAFETY_MARGIN_FACTOR * PRECISION;
    }
    
    /**
     * Set <code>SAFETY_MARGIN_FACTOR</code>. Note that 
     * <code>SAFETY_MARGIN</code> is also updated.
     * 
     * @param factor the new value for <code>SAFETY_MARGIN_FACTOR</code>.
     */
    static public void setSafetyMarginFactor(double factor) {
        SAFETY_MARGIN_FACTOR = factor;
        SAFETY_MARGIN = SAFETY_MARGIN_FACTOR * PRECISION;
    }
     
    static protected double PRE_INIT_FACTOR = 1.0;
    static protected int    MAX_PRE_INIT    = 10000;
    static protected double MAX_POP_FACTOR  = 0.5;
    static protected double INCREASE_FACTOR = 2;
}
