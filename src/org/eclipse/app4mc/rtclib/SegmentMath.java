/* SegmentMath implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit			Yellow (wandeler@tik.ee.ethz.ch)

Created: 28.10.05 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.Point;

/**
 * <code>SegmentMath</code> provides methods to do mathematics with segments.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: SegmentMath.java 796 2008-02-06 12:01:50Z haidw $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class SegmentMath {
	
	///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

	/**
	 * Returns the point where the segments a and b cross. If the 
	 * segments do not cross, i.e. if their slope is equal, the x- and y-
	 * coordinates of the returned point have both the value 
	 * {@link java.lang.Double#NaN java.lang.Double.NaN}.
	 * 
	 * @param a the first Segment.
	 * @param b the second Segment.
	 * @return the Point where the segments a and b cross.
	 */
	public static Point crossPoint(Segment a, Segment b) {
		double x,y;
		if (DoubleMath.eq(a.s(), b.s())) {
			x = Double.NaN;
			y = Double.NaN;
		} else {
			x = (a.y() - b.y() 
					- (a.s() * a.x()) + (b.s() * b.x())) 
					/ (b.s() - a.s());
			y = a.yAt(x);
		}
		if (x == Double.POSITIVE_INFINITY ||
			y == Double.POSITIVE_INFINITY) {
			x = Double.NaN;
			y = Double.NaN;
		}
		return new Point(x, y);
	}
	
	/**
	 * Returns the point where the limited segments a and b cross. If the 
	 * segments do not cross, i.e. if their slope is equal, the x- and y-
	 * coordinates of the returned point have both the value 
	 * {@link java.lang.Double#NaN java.lang.Double.NaN}. The cross point is
     * also computed if it lies outside the x-interval over which the 
     * LimitedSegmet a and b are defined.
	 * 
	 * @param a the first LimitedSegment.
	 * @param b the second LimitedSegment.
	 * @return the Point where the LimitedSegment a and b cross.
	 */
	public static Point crossPoint(LimitedSegment a, LimitedSegment b) {
		double x,y;
		if (DoubleMath.eq(a.s(), b.s())) {
			x = Double.NaN;
			y = Double.NaN;
		} else {
			x = (a.y() - b.y() 
					- (a.s() * a.x()) + (b.s() * b.x())) 
					/ (b.s() - a.s());
			y = a.yAt(x);
		}
		if (x == Double.POSITIVE_INFINITY ||
				y == Double.POSITIVE_INFINITY) {
				x = Double.NaN;
				y = Double.NaN;
		}
		return new Point(x, y);
	}
    
    /**
     * Returns true, if Segment a has a smaller slope than Segment b, or 
     * if both Segments have the same slope and a is lower than b or equals b. 
     * For this operation, the Segments are interpreted as straight lines that
     * are valid from 0 <= x < infinity, i.e. the start point is not considered.
     * 
     * @param a the first Segment.
     * @param b the second Segment.
     * @return true if a has a smaller slope than b, or if a is lower or equals 
     * than b.
     */
    public static boolean leq(Segment a, Segment b) {
        if (a.y() == Double.POSITIVE_INFINITY) {
            return false;
        } else if (b.y() == Double.POSITIVE_INFINITY) {
            return true;
        } else if (DoubleMath.eq(a.s(), b.s())) {
            double upperStartX = Math.max(a.x(), b.x());
            if (DoubleMath.gt(a.yAt(upperStartX), b.yAt(upperStartX))) {
                return false;
            }
        } else if (DoubleMath.gt(a.s(), b.s())) {
            return false;
        }
        return true;
    }
}
