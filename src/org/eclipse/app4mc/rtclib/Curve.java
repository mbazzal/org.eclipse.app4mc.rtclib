/* Curve implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnit			Yellow (wandeler@tik.ee.ethz.ch) 

Created: 02.11.2005 by Ernesto Wandeler

*/

package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;
import org.eclipse.app4mc.rtclib.util.Messages;
import java.util.LinkedList;
import java.io.Serializable;

/**
 * <code>Curve</code> represents a piecewise linear curve.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: Curve.java 968 2015-09-11 11:37:36Z thiele $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class Curve implements Cloneable, Serializable  {

	// is set in the build.xml (replaces it while copying files)
	// private static final long serialVersionUID = Integer.parseInt("@serialVersionUID@");	
	private static final long serialVersionUID = 1598352218976515684L;	
	
	///////////////////////////////////////////////////////////////////
    ////                         public methods                    ////

    protected Curve(SegmentList aper, SegmentList per, 
            double px0, double py0, long period, 
            double pdx, double pdy, double pds, double pyMin, double pyMax, 
            String name) {
        if (aper != null) {
            this.aper = new AperiodicPart(aper, true); 
        } else {
            this.aper = null;
        }
        if (per != null) {
            this.per = new PeriodicPart(per, px0, py0, period, pdx, pdy, pds, pyMin, pyMax);
        } else {
            this.per = null;
        }
        this.name = name;       
    }
    
    /**
     * Constructs a Curve with an aperiodic and a periodic part.
     * 
     * @param aperSegments the segments of the aperiodic part.
     * @param perSegments the segments of the periodic part.
     * @param px0 the x-value of the start point of the periodic part.
     * @param py0 the y-value of the start point of the periodic part.
     * @param period the period of the periodic part.
     * @param pdy the y-offset between periods of the periodic part.
     * @param name the name of the curve.
     */	
    public Curve(SegmentList aperSegments, SegmentList perSegments, 
			double px0, double py0, 
			long period, double pdy, String name) {
        
        if ((aperSegments == null || aperSegments.size() == 0) 
                && (perSegments == null || perSegments.size() == 0)) 
            throw new IllegalArgumentException(
                    Messages.ILLEGAL_ARGUMENT_BOTH_NULL);        
        this.name = name;  
        // Check and process the aperiodic part data.
        if (aperSegments != null && aperSegments.size() > 0) {
            this.aper = new AperiodicPart(aperSegments);
            if (perSegments != null && perSegments.size() > 0)
                this.aper.segments().trimLT(px0);               
        } else {
            this.aper = null;
            px0 = 0.0;
        }
        
        // Check and process the periodic part data
        if (perSegments != null && perSegments.size() > 0) {
            if (Double.isNaN(py0)) {
                if (this.aper != null) {
                    py0 = this.aper.segments().lastSegment().yAt(px0);
                } else {
                    py0 = 0.0;
                }
            }
            this.per = new PeriodicPart(perSegments, px0, py0, period, pdy);
        } else {
            this.per = null;
        }
	}

     /**
      * Constructs a Curve with an aperiodic and a periodic part.
      * 
      * @param aperSegments the segments of the aperiodic part.
      * @param perSegments the segments of the periodic part.
      * @param px0 the x-value of the start point of the periodic part.
      * @param py0 the y-value of the start point of the periodic part.
      * @param period the period of the periodic part.
      * @param pdy the y-offset between periods of the periodic part.
      */
    public Curve(SegmentList aperSegments, SegmentList perSegments, 
                double px0, double py0, long period, double pdy) {
        this(aperSegments, perSegments, px0, py0, period, pdy, "");
    }
    
    /**
     * Constructs a Curve with only a periodic part.
     * 
     * @param perSegments the segments of the periodic part.
     * @param py0 the y-value of the start point of the periodic part.
     * @param period the period of the periodic part.
     * @param pdy the y-offset between periods of the periodic part.
     * @param name the name of the curve.
     */
    public Curve(SegmentList perSegments, double py0, 
                long period, double pdy, String name) {
        this(null, perSegments, 0.0, py0, period, pdy, name);
    }
    
    /**
     * Constructs a Curve with only a periodic part.
     * 
     * @param perSegments the segments of the periodic part.
     * @param py0 the y-value of the start point of the periodic part.
     * @param period the period of the periodic part.
     * @param pdy the y-offset between periods of the periodic part.
     */
    public Curve(SegmentList perSegments, double py0, 
            long period, double pdy) {
        this(null, perSegments, 0.0, py0, period, pdy, "");
    }
 
    /**
     * Constructs a Curve with only an aperiodic part.
     * 
     * @param aperSegments the segments of the aperiodic part.
     * @param name the name of the curve.
     */ 
    public Curve(SegmentList aperSegments, String name) {
        this(aperSegments, null, Double.NaN, Double.NaN, 0, Double.NaN, name);
    }
    
    /**
     * Constructs a Curve with only an aperiodic part.
     * 
     * @param aperSegments the segments of the aperiodic part.
     */ 
    public Curve(SegmentList aperSegments) {
        this(aperSegments, null, Double.NaN, Double.NaN, 0, Double.NaN, "");
    }
    
	/**
	 * This constructor is intended for use with Matlab and behaves exactly
	 * as its counterpart that takes ArrayLists instead of Double arrays.
	 */
	public Curve(double[][] aper, double[][] per, double px0, double py0, 
            long period, double pdy, String name) {         
        this(new SegmentList(aper), new SegmentList(per), px0, py0, period, pdy, name);
    }
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] aper, double[][] per, double px0, double py0, 
            long period, double pdy) {         
        this(new SegmentList(aper), new SegmentList(per), px0, py0, period, pdy, "");
    }
      
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] aper, double[][] per, double px0, 
            double py0, long period, String name) {         
        this(new SegmentList(aper), new SegmentList(per), px0, py0, period, Double.NaN, name);
    }
      
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] aper, double[][] per, double px0, 
            double py0, long period) {         
        this(new SegmentList(aper), new SegmentList(per), px0, py0, period, Double.NaN, "");
    }  
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] aper, double[][] per, double px0, long period, String name) {         
        this(new SegmentList(aper), new SegmentList(per), px0, Double.NaN, period, Double.NaN, name);
    }
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] aper, double[][] per, double px0, long period) {         
        this(new SegmentList(aper), new SegmentList(per), px0, Double.NaN, period, Double.NaN, "");
    }    
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] per, double py0, long period, double pdy, String name) {         
        this(null, new SegmentList(per), 0.0, py0, period, pdy, name);
    }
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] per, double py0, long period, double pdy) {         
        this(null, new SegmentList(per), 0.0, py0, period, pdy, "");
    }
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] per, double py0, long period, String name) {         
        this(null, new SegmentList(per), 0.0, py0, period, Double.NaN, name);
    }
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] per, double py0, long period) {         
        this(null, new SegmentList(per), 0.0, py0, period, Double.NaN, "");
    }

    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] per, long period, String name) {         
        this(null, new SegmentList(per), 0.0, 0.0, period, Double.NaN, name);
    }
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] per, long period) {         
        this(null, new SegmentList(per), 0.0, 0.0, period, Double.NaN, "");
    }
    
    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] aper, String name) {         
        this(new SegmentList(aper), name);
    }

    /**
     * This constructor is intended for use with Matlab and behaves exactly
     * as its counterpart that takes ArrayLists instead of Double arrays.
     */
    public Curve(double[][] aper) {         
        this(new SegmentList(aper));
    }

    /**
     * Constructs a Curve from a textual representation. The textual representation
     * equals the syntax that is used in Matlab to create a Curve. Any of the
     * following forms works:<br><br>
     * <pre>
     * name = Curve(aper, per, px0, py0, period, pdy, 'name') 
     * name = Curve(aper, per, px0, py0, period, pdy) 
     * name = Curve(aper, per, px0, py0, period, 'name') 
     * name = Curve(aper, per, px0, py0, period) 
     * name = Curve(per, py0, period, pdy, 'name') 
     * name = Curve(per, py0, period, pdy) 
     * name = Curve(per, py0, period, 'name') 
     * name = Curve(per, py0, period) 
     * name = Curve(per, period, 'name') 
     * name = Curve(per, period)
     * name = Curve(aper, 'name') 
     * name = Curve(aper)
     * </pre><br><br>
     * Additionally, any of the above string without "name = " works, e.g.:<br><br>
     * <pre>
     * Curve(aper, per, px0, py0, period, pdy, 'name') 
     * </pre><br><br>
     * If name is defined at the left side of the equal sign, as well as in 
     * the method call, then the value in the method call is taken.
     * 
     * @param str the textural representation of the Curve.
     */
    public Curve(String str) {
        Curve temp = CurveFactory.createFromString(str);
        this.aper = temp.aperiodicPart();
        this.per = temp.periodicPart();
        this.name = temp.name();
    }
    
    /**
     * Creates a Curve that represents the unfolded SegmentList segments. The 
     * AperiodicPart equals segments from 0 <= x < (xMax - period), the 
     * Periodic equals segments from (xMax - period) <= x < xMax.
     * 
     * @param segments the unfolded Curve.
     * @param period the period of the new Curve.
     * @param pdy the pdy of the new Curve.
     * @param xMax the x-value until which the Curve is unfolded.
     * @param name the name of the Curve.
     * @return a Curve that represents the SegmentList segments.
     */
    protected Curve(SegmentList segments, long period, double pdy, double xMax, String name) {
        Curve result;
   
        // The new curve has only an aperiodic part.
        if (period == 0) {
            result = new Curve(segments);
            this.aper = result.aper;
            this.per = result.per;
            this.name = name;
            return;
        }
        // NS fix: use first LinkedList which is converted to a SegmentList
        // Gives much better performance on add/remove operations
        SegmentList per = new SegmentList();
        LinkedList<Segment> perTemp = new LinkedList<Segment>();
        LinkedList<Segment> aperTemp = new LinkedList<Segment>();
        
        double px0 = xMax - (double)period;
        double px0Temp = px0;
        // segments.trimLEQ(xMax);
        segments.trimLT(xMax);
        if (DoubleMath.eq(px0, 0.0)) {
            // The new curve has only a periodic part.
            double py0 = segments.lastSegment().yAt(xMax) - pdy; 
            segments.move(0.0, -py0);
            result = new Curve(segments, py0, period, pdy);
        } else {     
            // The new curve has an aperiodic and a periodic part.
            // segments.trimLT(xMax);
            int index = segments.size() - 1;
            Segment seg = segments.segment(index);
            double py0 = seg.yAt(xMax) - pdy;
            //NS
            while (DoubleMath.geq(seg.x(), px0)) {
            	perTemp.addFirst(seg);
                //per.add(0, seg);
                px0Temp = seg.x();
                segments.set(index, null);
                //segments.remove(index);
                index = index - 1;
                if(index < 0) {
                	break;
                }
                seg = segments.segment(index);
            }
            per = new SegmentList(perTemp);
            
            for(int i = 0; i < segments.size(); i++) {
            	if(segments.segment(i) != null) {
            		aperTemp.add(segments.segment(i));
            	} else {
            		break;
            	}
            }
            segments = new SegmentList(aperTemp);
            //NS
            
            if (DoubleMath.neq(px0Temp, px0)) {
//                per.add(0, new Segment(px0, py0, seg.s()));               
                per.add(0, new Segment(px0, seg.yAt(px0), seg.s()));               
            }
            per.move(-px0, -py0);
            result = new Curve(segments, per, px0, py0, period, pdy);
        }
        this.aper = result.aper;
        this.per = result.per;
        this.name = name;
        this.simplify();
    }
    
    /**
     * Creates a Curve that represents the unfolded SegmentList segments. The 
     * AperiodicPart equals segments from 0 <= x < (xMax - period), the 
     * Periodic equals segments from (xMax - period) <= x < xMax.
     * 
     * @param segments the unfolded Curve.
     * @param period the period of the new Curve.
     * @param pdy the pdy of the new Curve.
     * @param xMax the x-value until which the Curve is unfolded.
     * @return a Curve that represents the SegmentList segments.
     */
    protected Curve(SegmentList segments, long period, double pdy, double xMax) {
        this(segments, period, pdy, xMax, "");
    }

    
	/**
     * Returns the name of this curve.
     * 
	 * @return the name of this curve.
	 */
	public String name() {
        return this.name;
    }
    
    /**
     * Sets the name of this Curve.
     * 
     * @param name the new name of this curve.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Returns the AperiodicPart of this Curve.
     * 
     * @return the AperiodicPart of this Curve.
     */
    public AperiodicPart aperiodicPart() {
        return this.aper;
    }

    /**
     * Sets the AperiodicPart of this Curve to the passed AperiodicPart.
     * 
     * @param aper the new AperiodicPart of this Curve.
     */
    public void setAperiodicPart(AperiodicPart aper) {
        this.aper = aper;
    }
    
    /**
     * Returns the PeriodicPart of this Curve.
     * 
     * @return the PeriodicPart of this Curve.
     */
    public PeriodicPart periodicPart() {
        return this.per;
    }

    /**
     * Sets the PeriodicPart of this Curve to the passed PeriodicPart.
     * 
     * @param per the new PeriodicPart of this Curve.
     */
    public void setPeriodicPart(PeriodicPart per) {
        this.per = per;
    }
    
    /**
     * Returns the segments of the aperiodic part of this curve as an 
     * SegmentList. Returns null if this
     * curve does not contain an aperiodic part.
     * 
     * @return the segments of the aperiodic part of this curve.
     */
    public SegmentList aperiodicSegments() {
        if (this.hasAperiodicPart()) {
            return this.aper.segments();
        } else {
            return null;
        }
    }
    /**
     * Returns the segments of the periodic part of this curve as an 
     * SegmentList. Returns null if this
     * curve does not contain an periodic part.
     * 
     * @return the segments of the periodic part of this curve.
	 */
	public SegmentList periodicSegments() {
        if (this.hasPeriodicPart()) {
            return this.per.segments();
        } else {
            return null;
        }
	}
	
	/**
     * Returns the segments that define this Curve from 0 <= x <= xMax as a 
     * SegmentList.
     * 
     * @param xMax the upper x-value up to which the segments of this Curve 
     * are returned.
     * @return the segments of this Curve from 0 <= x <= xMax 
     */
    public SegmentList segmentsLEQ(double xMax) {
        int elements = 0;
        if (this.hasAperiodicPart()) {
            elements += this.aper.segments().size();
        }
        if (this.hasPeriodicPart()) {
            elements += Math.max(0, 
                    Math.ceil((xMax - this.per.px0()) / this.per.pdx())) 
                    * this.per.segments().size();
        }
        SegmentList result = new SegmentList(elements);
        CurveSegmentIterator iter = this.segmentIterator();
        while(iter.next() && DoubleMath.leq(iter.xStart(), xMax)) {
            result.add(iter.segment());
        }
        return result;
    }
    
    /**
     * Returns the segments that define this Curve from 0 <= x < xMax as a 
     * SegmentList.
     * 
     * @param xMax the upper x-value up to which the segments of this Curve 
     * are returned.
     * @return the segments of this Curve from 0 <= x < xMax 
     */
    public SegmentList segmentsLT(double xMax) {
        int elements = 0;
        if (this.hasAperiodicPart()) {
            elements += this.aper.segments().size();
        }
        if (this.hasPeriodicPart()) {
            elements += Math.max(0, 
                    Math.ceil((xMax - this.per.px0()) / this.per.pdx())) 
                    * this.per.segments().size();
        }
        SegmentList result = new SegmentList(elements);
        CurveSegmentIterator iter = this.segmentIterator();
        while(iter.next() && DoubleMath.lt(iter.xStart(), xMax)) {
            result.add(iter.segment());
        }
        return result;
    }

    /**
     * Returns the period of the periodic part of this curve. Returns 0 if this
     * curve does not contain a periodic part.
     * 
     * @return the period of the periodic part.
     */
    public long period() {
        if (this.hasPeriodicPart()) {
            return this.per.period();
        } else {
            return 0;
        }
    }

    /**
	 * Returns the x-coordinate where the periodic
	 * part of this curve starts. Returns Double.NaN if this
     * curve does not contain a periodic part.
     * 
	 * @return the x-coordinate where the periodic
	 * part of this curve starts.
	 */
	public double px0() {        
	    if (this.hasPeriodicPart()) {
	        return this.per.px0();
        } else {
            return Double.NaN;
        }
	}

	/**
     * Returns the y-coordinate where the periodic
     * part of this curve starts. Returns Double.NaN if this
     * curve does not contain a periodic part.
     * 
     * @return the y-coordinate where the periodic
     * part of this curve starts.
	 */
	public double py0() {
        if (this.hasPeriodicPart()) {
            return this.per.py0();
        } else {
            return Double.NaN;
        }
	}

	/**
	 * Returns the delta on the x-axis between two consecutive repetitions
     * of the periodic part, i.e. the period itself. Returns Double.NaN if this
     * curve does not contain a periodic part.
     * 
	 * @return the delta on the x-axis between two consecutive repetitions
     * of the periodic part.
	 */
	public double pdx() {
        if (this.hasPeriodicPart()) {
            return this.per.pdx();
        } else {
            return Double.NaN;
        }
	}

	/**
     * Returns the delta on the y-axis between two consecutive repetitions
     * of the periodic part, i.e. the offset. Returns Double.NaN if this
     * curve does not contain a periodic part.
     * 
     * @return the delta on the y-axis between two consecutive repetitions
     * of the periodic part.
	 */
	public double pdy() {
        if (this.hasPeriodicPart()) {
            return this.per.pdy();
        } else {
            return Double.NaN;
        }
	}

	/**
	 * Returns the overall slope of the periodic part: pds = pdy / pdx. 
	 * Returns Double.NaN if this curve does not contain a periodic part.
     * 
	 * @return the overall slope of the periodic part.
	 */
	public double pds() {
        if (this.hasPeriodicPart()) {
            return this.per.pds();
        } else {
            return Double.NaN;
        }
	}
	
	/**
	 * Returns the minimum y-value of the periodic part within x = [0,pdx] 
     * in the relative coordinate system with origin (px0/py0). Returns 
     * Double.NaN if this curve does not contain a periodic part.
     * 
	 * @return the minimum y-value of the periodic part.
	 */
	public double pyMin() {
        if (this.hasPeriodicPart()) {
            return this.per.pyMin();
        } else {
            return Double.NaN;
        }
	}
	
    /**
     * Returns the maximum y-value of the periodic part within x = [0,pdx] 
     * in the relative coordinate system with origin (px0/py0). Returns 
     * Double.NaN if this curve does not contain a periodic part.
     * 
     * @return the maximum y-value of the periodic part.
     */
	public double pyMax() {
        if (this.hasPeriodicPart()) {
            return this.per.pyMax();
        } else {
            return Double.NaN;
        }
	}
	
	/**
     * Returns the y-value of this curve at x = 0 + epsilon. I.e. if this curve 
     * is not continuous at x = 0, then the value at the right side of the 
     * discontinuity is returned.
     * 
     * @return the y-value of this curve at x = 0 + epsilon.
     */
    public double y0epsilon() {
        if (this.hasAperiodicPart()) {
            return (this.aper.segments().segment(0)).y();
        } else {
            return (this.per.segments().segment(0)).y() + this.per.py0();
        }
    }
    
    /**
     * Returns the y-value of this curve at x = +infinity.
     * 
     * @return the y-value of this curve at x = +infinitiy.
     */
    public double yAtInfinity() {
    	double s, y;
    	if (this.hasPeriodicPart()){
    		s = this.pds();
    		y = this.py0();
    	} else {
    		s = this.aperiodicSegments().lastSegment().s;
    		y = this.aperiodicSegments().lastSegment().y;
    	}
		if (DoubleMath.gt(s, 0.0)) {
			return Double.POSITIVE_INFINITY;
		} else if (DoubleMath.lt(s, 0.0)){
			return Double.NEGATIVE_INFINITY;
		} else {
			return y;
		}
    }
    
    /**
     * Returns the y-value of this curve at x = x1 + epsilon. I.e. if this curve 
     * is not continuous at x = x1, then the value at the right side of the 
     * discontinuity is returned. It will return 0 if x1 < 0.
     * 
     * @param x1 - the x-value at which we want to evaluate the function
     * @return the y-value of this curve at x = x1 + epsilon.
     */
    public double yXPlusEpsilon(double x1) {
    	Curve t;
    	
    	if (x1 < 0) {
    		return 0;
    	}
    	
    	if (x1 == 0) {
    		return this.y0epsilon();
    	}
    	
    	t = this.clone();
    	t.move(-x1,0);
    	return t.y0epsilon();
    }
    
    /**
     * Returns the y-value of this curve at x = x1 - epsilon. I.e. if this curve 
     * is not continuous at x = x1, then the value at the left side of the 
     * discontinuity is returned. It will return 0 if x1 <= 0.
     * 
     * @param x1 - the x-value at which we want to evaluate the function
     * @return the y-value of this curve at x = x1 - epsilon.
     */
    public double yXMinusEpsilon(double x1) {
    	SegmentList s;
    	Curve t;
    	
    	if (x1 <= 0) {
    		return 0;
    	}
    	
    	s = this.segmentsLT(x1);
    	t = new Curve(s);
    	t.move(-x1,0);
    	
    	return t.y0epsilon();
    }
    
    
    /**
     * Returns the length of the definition range of this curve. For a purely 
     * aperiodic curve, the definition range is defined from 0 up to the x-value
     * of the start point of the last curve segment. For a curve with a periodic 
     * part, the definition range is defined from 0 up to (px0 + pdx).
     * 
     * @return the length of the definition range of this curve.
     */
    public double definitionRange() {
        double definitionRange = 0.0;

        if (this.hasPeriodicPart()) {
            definitionRange = this.px0() + this.pdx();
        } else {
            definitionRange = ((Segment)this.aperiodicSegments()
                    .lastSegment()).x();     
        }
        return definitionRange;
    }

    /**
     * Returns a CurveSegmentIterator for this Curve.
     * 
     * @return a CurveSegmentIterator for this Curve.
     */
    public CurveSegmentIterator segmentIterator() {
        return new CurveSegmentIterator(this);
    }

    /**
     * Returns a CurveSegmentIterator for this Curve up to x < xMax.
     * 
     * @param xMax the maximum value up to which to iterate.
     * @return a CurveSegmentIterator for this Curve.
     */
    public CurveSegmentIterator segmentIterator(double xMax) {
        return new CurveSegmentIterator(this, xMax);
    }

    /**
     * Returns a SubSegmentIterator for this Curve and c.
     * 
     * @param c the second curve for the SubSegmentIterator.
     * @return a SubSegmentIterator for this curve and c.
     */
    public CurveSubSegmentIterator subSegmentIterator(Curve c) {
        return new CurveSubSegmentIterator(this, c);
    }

    /**
     * Returns a SubSegmentIterator for this Curve up to x < xMax.
     * 
     * @param c the second curve for the SubSegmentIterator.
     * @return a SubSegmentIterator for this curve and c.
     */
    public CurveSubSegmentIterator subSegmentIterator(Curve c, double dMax) {
        return new CurveSubSegmentIterator(this, c, dMax);
    }
    
    /**
	 * Returns true if this curve has an aperiodic part.
     * 
	 * @return <code>true</code> if this curve has an aperiodic curve.
	 */
	public boolean hasAperiodicPart() {
		return (this.aper != null);
	} 
	
	/**
	 * Returns true if this curve has a periodic part.
     * 
	 * @return <code>true</code> if this curve has a periodic part.
	 */
	public boolean hasPeriodicPart() {
		return (this.per != null);
	} 
		
	/**
	 * Returns true, if this Curve represents a constant. I.e. the Curve 
	 * consists of a single Segment with slope=0.
     * 
	 * @return <code>true</code>, if this Curve represents a constant. 
	 */
	public boolean isConstant() {
        if (this.hasPeriodicPart()) return false;
        if (this.aper.segments().size() != 1) return false;
        if (DoubleMath.neq(this.aper.segments().segment(0).s(), 0.0)) return false;
        return true;
	}
	
//    public boolean isLeftContinuous() {}
//    public boolean isRightContinuous() {}
//    public boolean isUpperCurve() {}
//    public boolean isLowerCurve() {}
    
    /**
     * Returns a lower bound to this curve as a Segment.
     * The returned bound is valid from x = [start-point of returned segment] 
     * up to x = infinity.
     *  
     * @return a lower bound to this curve.
     */
    public Segment lowerBound() {
        if(this.hasPeriodicPart()) {
            if (DoubleMath.geq(this.pds(), 0.0)) {
                return new Segment(this.px0(), 
                        this.py0() + this.pyMin() - this.pdy(), 
                        this.pds());             
            } else {
                return new Segment(this.px0(), 
                        this.py0() + this.pyMin(),
                        this.pds());
            }
        } else {    
            return (Segment)this.aperiodicSegments().lastSegment().clone();
        }
    }

    /**
     * Returns an upper bound to this curve as a Segment.
     * The returned bound is valid from x = [start-point of returned segment] 
     * up to x = infinity.
     *  
     * @return an upper bound to this curve.
     */
    public Segment upperBound() {
        if(this.hasPeriodicPart()) {
            if (DoubleMath.geq(this.pds(), 0.0)) {
                return new Segment(this.px0(), 
                        this.py0() + this.pyMax(),
                        this.pds());
            } else {
                return new Segment(this.px0(), 
                        this.py0() + this.pyMax() - this.pdy(), 
                        this.pds());             
            }
        } else {    
            return (Segment)this.aperiodicSegments().lastSegment().clone();
        }  
    }

    /**
     * Returns a tight lower bound to this curve as a Segment.
     * The returned bound is valid from x = [start-point of returned segment] 
     * up to x = infinity.
     *  
     * @return a tight lower bound to this curve.
     */
    public Segment tightLowerBound() {
        if (this.hasPeriodicPart()) {
            double dy = Double.POSITIVE_INFINITY;
            SegmentListIterator iter = this.periodicSegments()
                    .segmentListIterator(this.pdx());
            while(iter.next()) {
                dy = Math.min(dy, iter.yStart() - (iter.xStart() * this.pds()));
                dy = Math.min(dy, iter.yEnd() - (iter.xEnd() * this.pds()));
            }
            return new Segment(this.px0(), this.py0() + dy, this.pds());                
            
        } else {
            return (Segment)this.aperiodicSegments().lastSegment().clone();        
        }    
    }

    /**
     * Returns a tight upper bound to this curve as a Segment.
     * The returned bound is valid from x = [start-point of returned segment] 
     * up to x = infinity.
     *  
     * @return a tight upper bound to this curve.
     */
    public Segment tightUpperBound() {
        if (this.hasPeriodicPart()) {
            double dy = Double.NEGATIVE_INFINITY;
            SegmentListIterator iter = this.periodicSegments()
                    .segmentListIterator(this.pdx());
            while(iter.next()) {
                dy = Math.max(dy, iter.yStart() - (iter.xStart() * this.pds()));
                dy = Math.max(dy, iter.yEnd() - (iter.xEnd() * this.pds()));
            }
            return new Segment(this.px0(), this.py0() + dy, this.pds());                
            
        } else {
            return (Segment)this.aperiodicSegments().lastSegment().clone();         
        }  
    }

    /**
     * Scale this curve along the x-axis.
     * 
     * @param factor the factor to scale this curve.
     */
    public void scaleX(double factor) {
        if (DoubleMath.leq(factor, 0)) {
            throw new IllegalArgumentException(
                    Messages.SEGMENT_SCALEX_FACTOR_LEQ_0);
        }  
        if (this.hasPeriodicPart()) {
            this.per.scaleX(factor);
        }
        if (this.hasAperiodicPart()) {
            this.aper.scaleX(factor);
        }
    }

    /**
     * Scale this curve along the y-axis.
     * 
     * @param factor the factor to scale this curve.
     */
    public void scaleY(double factor) {
        if (DoubleMath.eq(factor, 0)) {
            this.aper = new AperiodicPart("[[0 0 0]]");
            this.per = null;
        } else {
            if (this.hasAperiodicPart()) {
                this.aper.scaleY(factor);
            }
            if (this.hasPeriodicPart()) {
                this.per.scaleY(factor);
            }   
        }
    }

    /**
     * Move this Curve by (dx,dy). If dx is positive, a segment (0,0,0) is
     * added at the beginning of the curve. If dx is negative, the resulting
     * curve part defined over x < 0 is lost.
     * 
     * @param dx the x-value by which this curve is moved.
     * @param dy the y-value by which this curve is moved.
     */
    public void move(double dx, double dy) {
        AperiodicPart aperTemp = null;
        if (this.hasPeriodicPart()) {
            aperTemp = this.per.move(dx, dy);
        }  
        if (aperTemp != null) {
            this.aper = aperTemp;
        } else if (this.hasPeriodicPart() && DoubleMath.eq(this.per.px0(), 0.0)) {
            this.aper = null;
        } else if (this.hasPeriodicPart() && DoubleMath.eq(this.per.px0(), dx)) {
            SegmentList aperList = new SegmentList(1);
            aperList.add(new Segment(0,0,0));
            this.aper = new AperiodicPart(aperList);
        } else {
            this.aper.move(dx, dy);
        }
    }

    /**
	 * Simplify this curve.
	 */
	public void simplify() {
        if (this.hasAperiodicPart()) {
            this.aper.segments().simplify();
        }
        if (this.hasPeriodicPart()) {
            this.per.segments().simplify();
        }      
        this.simplifyPeriodicPart();
        this.simplifyAperiodicPart();

//        //FIXME:
//        //This is a hack, the simplifiers should handle this correctly!
//        this.periodicMinY = CurvePartUtil.min(periodicPart, periodDeltaX);
//        this.periodicMaxY = CurvePartUtil.max(periodicPart, periodDeltaX);  
	}
	
	/**
     * Round all points that define this Curve. 
     * This includes all start points of all segments, all slopes, 
     * as well as px0, py0, pdy.
     * All related values such as pds, pyMin and pyMax are newly calculated.
	 */
	public void round() {
        if (this.hasAperiodicPart()) {
            this.aper.round();
        }
        if (this.hasPeriodicPart()) {
            this.per.round();
        }
	}
	
    /** 
     * Creates a deep copy of this Curve.
     * 
     * @return a deep copy of this Curve.
     */
	public Curve clone() {
        if (this.hasAperiodicPart()) {
            if (this.hasPeriodicPart()) {
                return new Curve((SegmentList)this.aper.segments().clone(), 
                        (SegmentList)this.per.segments().clone(),
                        this.per.px0(), this.per.py0(), 
                        this.per.period(), this.per.pdx(), this.per.pdy(), this.per.pds(),
                        this.per.pyMin(), this.per.pyMax(), this.name);
            } else {
                return new Curve((SegmentList)this.aper.segments().clone(), this.name);
            }
        } else {
            return new Curve(null, 
                    (SegmentList)this.per.segments().clone(),
                    this.per.px0(), this.per.py0(), 
                    this.per.period(), this.per.pdx(), this.per.pdy(), this.per.pds(),
                    this.per.pyMin(), this.per.pyMax(), this.name);
        }
	    
	}
	
    /** 
     * Returns true, if object quantitatively equals this curve. I.e. the name
     * is not checked for equality.
     *  
     * @param object the Curve to compare this one with.
     * @return <code>true</code> if this Curve equals object.
     */
    public boolean equals(Object object){
        if (object == null) return false;
        Curve c = (Curve) object;
        if (this.hasAperiodicPart()) {
            if (!this.aper.equals(c.aperiodicPart())) return false;
        }
        if (this.hasPeriodicPart()) {
            if (!this.per.equals(c.periodicPart())) return false;
        }
        return true;
    }
	
    /** 
     * Returns a String representation of this Curve.
     * 
     * @return a String representation of this Curve.
     */
	public String toString() {
        String str = "Curve: " + this.name + "\n";
        if (this.hasAperiodicPart()) {
            str += aper.toString();
        }
        if (this.hasPeriodicPart()) {
            str += per.toString();
        }
        return str;
	}

	/**
	 * Returns the data of this Curve in a textual form for export. 
	 * The syntax is such that this String can be copied directly into Matlab
	 * to create a copy of this Curve.
     * 
	 * @return the data of this Curve in a textual form for export.
	 */
	public String toMatlabString() {
        String str = "curve(";
        if (this.hasAperiodicPart()) {
            str += aper.toMatlabString();
        }
        if (this.hasAperiodicPart() && this.hasPeriodicPart()) {
            str += ", ";
        }
        if (this.hasPeriodicPart()) {
            str += per.segments().toMatlabString() + ", ";
            if (this.hasAperiodicPart()) {          
                str += this.px0() + ", ";
            } 
            str += this.py0() + ", " + this.period() + ", " + this.pdy();
        }
        str += ")";
        return str;      
	}
		
    /**
     * Simplifies the PeriodicPart of this Curve. First, if the PeriodicPart
     * is not needed, it is removed. This is the case, if the PeriodicPart 
     * unfolds to a straight line. Then, the PeriodicPart is removed and a 
     * Segement that represents the straight line is added at the end of the 
     * AperiodicPart. <br><br>
     * Second, if the last Segment of 
     * the PeriodicPart and the first Segment of its immediate repetition build
     * a straight, continuous line, then the first segment can be removed, and 
     * instead the last Segment of the previous repetition is used. The 
     * start of the PeriodicPart is then shifted to the right, and if
     * needed, a new Segment is added at the end of the AperiodicPart of this
     * Curve.<br>
     * E.g. if the PeriodicPart of this curve is {(0,0,0)(1,0,1)(2,1,0)}, then 
     * the first Segment gets removed and the remainder gets adapted, leading to 
     * {(0,0,1)(1,1,0)} (note, this describes the same periodic part, but with 
     * only two segments). The start of the periodic part is shifted to the 
     * right by 1, and if needed a new segment (x,y,0) is added at the end of 
     * the AperiodicPart of the curve. <br>
     * <b>Pre:</b> <br> 
     * curve.periodicSegments().minimize();
     */
    protected void simplifyPeriodicPart() {
        if (!this.hasPeriodicPart()) return;
        if (this.periodicSegments().size() < 1) return;
        if (this.periodicSegments().size() == 1) {
            // Remove unnecessary PeriodicPart
            Segment seg = this.periodicSegments().segment(0);
            if (DoubleMath.eq(seg.y() + this.pdy(), seg.yAt(this.pdx())) ||
            		seg.y == Double.POSITIVE_INFINITY || seg.s == Double.POSITIVE_INFINITY) {
                if (!this.hasAperiodicPart()) {
                    SegmentList aper = new SegmentList(1);
                    aper.add(new Segment(this.px0(), this.py0() + seg.y(), seg.s()));
                    this.setAperiodicPart(new AperiodicPart(aper));
                } else {
                    Segment lastAperSeg = this.aperiodicSegments().lastSegment();
                    double dx = - lastAperSeg.x();
                    double dy = ((this.px0() - lastAperSeg.x()) * lastAperSeg.s()) - this.py0();
                    if (!lastAperSeg.moveEquals(dx, dy, seg)) {
                        this.aperiodicSegments().add(
                                new Segment(this.px0(), this.py0() + seg.y(), seg.s()));
                    } 
                }           
                this.setPeriodicPart(null);
            }
        } else {
            // Remove the first Segment of the PeriodicPart if possible.
            Segment firstSeg, lastSeg;
            firstSeg = this.periodicSegments().segment(0);
            lastSeg = this.periodicSegments().lastSegment();
            
            if (DoubleMath.eq(firstSeg.s(), lastSeg.s()) 
                    && DoubleMath.eq(firstSeg.y() + this.pdy(), lastSeg.yAt(this.pdx()))) {
                // Add the first Segment of the PeriodicPart to the AperiodicPart.
                if (!this.hasAperiodicPart()) {
                    SegmentList aper = new SegmentList(1);
                    aper.add(firstSeg);
                    aper.move(0.0, this.py0());
                    this.setAperiodicPart(new AperiodicPart(aper));
                } else {
                    Segment lastAperSeg = this.aperiodicSegments().lastSegment();
                    if (DoubleMath.neq(firstSeg.s(), lastAperSeg.s()) || 
                            DoubleMath.neq(this.py0() + firstSeg.y() , lastAperSeg.yAt(this.px0())))
                        this.aperiodicSegments().add(
                                new Segment(this.px0(), this.py0() + firstSeg.y(), firstSeg.s()));
                }
                // Remove the first Segment of the PeriodicPart.
                this.periodicSegments().remove(0);
                firstSeg = this.periodicSegments().segment(0);
                double dx = firstSeg.x();
                double dy = dx * lastSeg.s();
                this.periodicSegments().move(-dx, -dy);
                this.periodicPart().move(dx, dy);
                this.periodicPart().normalizeCoordinates();
                this.periodicPart().update(); // PERFORMANCE maybe do this only once in simplify???
            }
        }
    }
    
    /**
     * Minimizes the length of the AperiodicPart, by trying to fit 
     * the PeriodicPart at the end of the AperiodicPart, as often as possible.
     * Also fractions of the PeriodicPart are tried to fit. This then however
     * results in a reordering of the Segments in the PeriodicPart 
     * (revolving).
     * <b>Pre:</b> <br> 
     * this.aperiodicSegments().minimize();<br>
     * this.periodicSegments().minimize();<br>
     * this.simplifyPeriodicPart();
     */
    protected void simplifyAperiodicPart() {
        if (!this.hasAperiodicPart()) return;
        if (!this.hasPeriodicPart()) return;
        
        int perSize, aperSize, i, j, p, q;
        double dx, dy;
        boolean lastSegmentMatched = true;
        Segment periodicSegment = null, aperiodicSegment = null;

        aperSize = this.aperiodicSegments().size();
        perSize = this.periodicSegments().size();
        i = aperSize - 1;
        j = perSize - 1;
        dx = this.px0() - this.pdx();
        dy = this.py0() - this.pdy();
        while (lastSegmentMatched && (i >= 0)) {
            // Match Segments of the PeriodicPart
            aperiodicSegment = this.aperiodicSegments().segment(i);
            periodicSegment = this.periodicSegments().segment(j);              
            if (periodicSegment.moveEquals(dx, dy, aperiodicSegment)) {
                this.aperiodicSegments().remove(i);
                i = i - 1;
                j = j - 1;
                if (j < 0) {
                    j = perSize - 1;
                    this.periodicPart().move(-this.pdx(), -this.pdy());
                    dx = dx - this.pdx();
                    dy = dy - this.pdy();
                }
                lastSegmentMatched = true;
            } else {
                lastSegmentMatched = false;
            }
        }
        if (j < (perSize - 1)) {
            // Rotate the Segments in the PeriodicPart.
            double shiftX = this.periodicSegments().segment(j + 1).x();
            double shiftY = this.periodicSegments().segment(j + 1).y();
            SegmentList temp = new SegmentList(perSize);
            Segment seg = null;
            for (q = 0, p = j + 1; q < perSize; q++, p++) {
                seg = this.periodicSegments().segment(p % perSize);
                temp.add(q, new Segment(
                        (seg.x() - shiftX + this.pdx()) % this.pdx(), 
                        seg.y() - shiftY + 
                        ((p < perSize) ? 0 : this.pdy()),
                        seg.s()));
            }
            this.setPeriodicPart(new PeriodicPart(temp, dx + shiftX, dy + shiftY, 
                    this.period(), this.pdy()));
        }
        if (this.aperiodicSegments().size() == 0) {
            this.setAperiodicPart(null);
        } 
    }

    
    /**
     * Evaluates the curve and returns the biggest valid y value.
     * @param x
     * @return y = a(x)
     * @author reint (Tobias Rein)
     * @since 2008-10-06
     */
    public double getYmin(double x){
    	CurveSegmentIterator iter =  new CurveSegmentIterator(this);
    	while(iter.next() && DoubleMath.gt(x, iter.xStart())) {
    		if (DoubleMath.leq(x, iter.xEnd())) {
//        		System.err.println("DEBUG: seg from (" +
//        				iter.xStart()+","+iter.yStart()+") to ("+
//        				iter.xEnd()+","+iter.yEnd()+"), s="+iter.s());
    			return iter.segment().yAt(x);
    		}
    	}
    	if (DoubleMath.eq(x, 0.0)){
    		return 0.0;
    	}
    	throw new IllegalArgumentException(Messages.VALUE_UNDEFINED);
    }
    
    /**
     * Evaluates the curve and returns the smallest valid y value.
     * @param x
     * @return y = a(x)
     * @author reint (Tobias Rein)
     * @since 2008-10-06
     */
    public double getYmax(double x){
    	CurveSegmentIterator iter =  new CurveSegmentIterator(this);
    	while(iter.next() && DoubleMath.geq(x, iter.xStart())) {
    		if (DoubleMath.lt(x, iter.xEnd())) {
//        		System.err.println("DEBUG: seg from (" +
//        				iter.xStart()+","+iter.yStart()+") to ("+
//        				iter.xEnd()+","+iter.yEnd()+"), s="+iter.s());
    			return iter.segment().yAt(x);
    		}
    	}
    	throw new IllegalArgumentException(Messages.VALUE_UNDEFINED);
    }
    
    /**
     * Find the maximum x value that holds a(x)=y.
     * @param y
     * @return x
     * @author reint (Tobias Rein)
     * @since 2008-10-06
     */
    public double getXmax(double y){
    	CurveSegmentIterator iter =  new CurveSegmentIterator(this);
    	double lastXend = 0.0;
    	while(iter.next() && DoubleMath.geq(y, iter.yStart())) {
    		if (DoubleMath.lt(y, iter.yEnd())) {
//        		System.err.println("DEBUG: seg from (" +
//        				iter.xStart()+","+iter.yStart()+") to ("+
//        				iter.xEnd()+","+iter.yEnd()+"), s="+iter.s());
    			return iter.segment().xAt(y);
    		}
    		lastXend = iter.xEnd();
    	}
    	return lastXend;
    }
    
    /**
     * Find the minimum x value that holds a(x)=y.
     * @param y
     * @return x
     * @author reint (Tobias Rein)
     * @since 2008-10-06
     */
    public double getXmin(double y){
    	CurveSegmentIterator iter =  new CurveSegmentIterator(this);
    	double nextXstart = 0.0;
    	while(iter.next() && DoubleMath.gt(y, iter.yStart())) {
    		if (DoubleMath.leq(y, iter.yEnd())) {
//        		System.err.println("DEBUG: seg from (" +
//        				iter.xStart()+","+iter.yStart()+") to ("+
//        				iter.xEnd()+","+iter.yEnd()+"), s="+iter.s());
    			return iter.segment().xAt(y);
    		}
    		nextXstart = iter.xEnd();
    	}
    	return nextXstart;
    }
    
            
    private AperiodicPart aper;
    private PeriodicPart per;
    private String name;

}
