/* CurveChecker implementation.

Copyright (c) 2004-2006 Computer Engineering and Networks Laboratory (TIK), 
Swiss Federal Institute of Technology (ETH) Zurich.
All rights reserved.
Permission is hereby granted, without written agreement and without
license or royalty fees, to use, copy, modify, and distribute this
software and its documentation for any purpose, provided that the above
copyright notice and the following two paragraphs appear in all copies
of this software.

IN NO EVENT SHALL THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH 
BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR 
CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS 
DOCUMENTATION, EVEN IF THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) 
ZURICH HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE SWISS FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH SPECIFICALLY 
DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. 
THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE SWISS 
FEDERAL INSTITUTE OF TECHNOLOGY (ETH) ZURICH  HAS NO OBLIGATION TO 
PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

                                       RTC_COPYRIGHT_VERSION_1
                                       COPYRIGHTENDKEY

@ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
@AcceptedRating 
@JUnitTesting   Yellow (wandeler@tik.ee.ethz.ch)

Created: 02.11.2005 by Ernesto Wandeler
*/
package org.eclipse.app4mc.rtclib;

import org.eclipse.app4mc.rtclib.util.DoubleMath;

/**
 * <code>CurveChecker</code> provides methods to check properties of a Curve.
 * 
 * @author         Ernesto Wandeler
 * @version        $Id: CurveChecker.java 918 2008-12-23 11:38:46Z nikolays $
 * @since          
 * @ProposedRating Yellow (wandeler@tik.ee.ethz.ch)
 * @AcceptedRating none
 * @JUnitRating    Yellow (wandeler@tik.ee.thz.ch)
 */
public class CurveChecker {
    
    /**
     * Tests if the passed Curve is wide-sense increasing.
     * <p> 
     * A Curve f(x) is wide-sense increasing iff f(x) <= f(x+dx) for all dx >= 0.
     * <p>
     * 
     * @param c a Curve. 
     * @return <code>true</code> if the Curve is wide-sense increasing; 
     * <code>false</code> otherwise.
     */
    public static boolean isWideSenseIncreasing(Curve c) {
        CurveSegmentIterator iter = c.segmentIterator(c.definitionRange() 
                + Parameters.SAFETY_MARGIN);
        double currentY = 0.0;

        while(iter.next()) {
			if (DoubleMath.lt(iter.yStart(), currentY)) {
				return false;
			}
			if (DoubleMath.lt(iter.yEnd(), iter.yStart())) {
				return false;
			}
			currentY = iter.yEnd();
        }
        if (DoubleMath.lt(iter.s(), 0.0)) {
            return false;
        }
        return true;
    }

    /**
     * Tests if the passed Curve is wide-sense increasing. This method accepts also
     * Curves that go to infinity.
     * <p> 
     * A Curve f(x) is wide-sense increasing iff f(x) <= f(x+dx) for all dx >= 0.
     * <p>
     * 
     * @param c a Curve. 
     * @return <code>true</code> if the Curve is wide-sense increasing; 
     * <code>false</code> otherwise.
     */
    public static boolean isWideSenseIncreasingAllowInfinity(Curve c) {
        CurveSegmentIterator iter = c.segmentIterator(c.definitionRange() 
                + Parameters.SAFETY_MARGIN);
        double currentY = 0.0;

        while(iter.next()) {
			if (DoubleMath.lt(iter.yStart(), currentY)
					&& iter.yStart()!=Double.POSITIVE_INFINITY
					&& currentY!=Double.POSITIVE_INFINITY) {
				return false;
			}
			if (DoubleMath.lt(iter.yEnd(), iter.yStart())
					&& iter.yEnd()!=Double.POSITIVE_INFINITY
					&& iter.yStart()!=Double.POSITIVE_INFINITY) {
				return false;
			}
			currentY = iter.yEnd();
        }
        if (DoubleMath.lt(iter.s(), 0.0)) {
            return false;
        }
        return true;
    }
    
    /**
     * Tests if the passed Curve is non-negative
     * <p> 
     * A Curve f(x) is non-negative iff f(x) >= 0 for all x >= 0.
     * <p>
     * 
     * @param c a Curve. 
     * @return <code>true</code> if the Curve is non-negative; 
     * <code>false</code> otherwise.
     */
    public static boolean isNonNegative(Curve c) {
        if (DoubleMath.lt(c.lowerBound().s(), 0.0)) {
            return false;
        }
        CurveSegmentIterator iter = c.segmentIterator(c.definitionRange() 
                + Parameters.SAFETY_MARGIN);

        while(iter.next()) {
            if (DoubleMath.lt(iter.yStart(), 0.0)) {
                return false;
            }
            if (DoubleMath.lt(iter.yEnd(), 0.0)) {
                return false;
            }
        }
        if (DoubleMath.lt(iter.s(), 0.0)) {
            return false;
        }
        return true;
    }

}





